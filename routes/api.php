<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('brands', 'API\BrandController@index');
    Route::post('brands', 'API\BrandController@store');
    Route::put('brands/{id}', 'API\BrandController@update');
    Route::delete('brands/{id}', 'API\BrandController@destroy');

    Route::get('divisions', 'API\DivisionController@index');
    Route::post('divisions', 'API\DivisionController@store');
    Route::put('divisions/{id}', 'API\DivisionController@update');
    Route::delete('divisions/{id}', 'API\DivisionController@destroy');

    Route::get('blogs', 'API\BlogController@index');
    Route::get('blogs/tags', 'API\BlogController@tag');
    Route::get('blogs/{id}', 'API\BlogController@indivisualblog');
    Route::post('blogs', 'API\BlogController@store');
    Route::post('blogs/registration/{id}', 'API\BlogController@changeregistration');
    Route::post('blogs/remove', 'API\BlogController@remove');
    Route::put('blogs/{id}', 'API\BlogController@update');
    Route::delete('blogs/{id}', 'API\BlogController@destroy');

    Route::get('profile', 'API\UserController@profile');
    Route::put('profile', 'API\UserController@updateProfile');

    Route::get('about', 'API\AboutController@index');
    Route::put('about/{id}', 'API\AboutController@update');

    Route::get('users', 'API\UserController@index');
    Route::post('users', 'API\UserController@store');
    Route::put('users/{id}', 'API\UserController@update');
    Route::delete('users/{id}', 'API\UserController@destroy');

    Route::get('orders', 'API\OrderController@index');
    Route::delete('orders/{id}', 'API\OrderController@destroy');
    Route::post('orders/{id}', 'API\OrderController@changeStatus');
    Route::post('orders/archive/{id}', 'API\OrderController@archiveOrder');

    Route::get('teams', 'API\TeamController@index');
    Route::post('teams', 'API\TeamController@store');
    Route::post('teams/remove', 'API\TeamController@remove');
    Route::put('teams/{id}', 'API\TeamController@update');
    Route::delete('teams/{id}', 'API\TeamController@destroy');

    Route::get('products', 'API\ProductController@index');
    Route::get('products/{id}', 'API\ProductController@indivisualproduct');
    Route::post('products/published/{id}', 'API\ProductController@changePublished');
    Route::post('products/featured/{id}', 'API\ProductController@changeFeatured');
    Route::post('products/parallex/{id}', 'API\ProductController@changeParallex');
    Route::post('products/price/{id}', 'API\ProductController@changePrice');
    Route::post('products/salepercent/{id}', 'API\ProductController@changeSalePercentage');
    Route::post('products/sale/{id}', 'API\ProductController@changeSale');

    Route::post('products', 'API\ProductController@store');
    Route::post('products/colormap', 'API\ProductController@colormap');
    Route::post('products/upload', 'API\ProductController@upload');
    Route::post('products/remove', 'API\ProductController@remove');
    Route::post('products/removesizechart', 'API\ProductController@removeSizeChart');
    Route::put('products/{id}', 'API\ProductController@update');
    Route::delete('products/{id}', 'API\ProductController@destroy');

    Route::get('products/{id}/variants', 'API\VariantController@index');
    Route::post('products/{id}/variants', 'API\VariantController@store');
    Route::get('variants/{id}', 'API\VariantController@indivisualvariant');
    Route::post('variants/remove', 'API\VariantController@remove');
    Route::put('variants/{id}', 'API\VariantController@update');
    Route::delete('variants/{id}', 'API\VariantController@destroy');
    Route::post('variants/published/{id}', 'API\VariantController@changePublished');
    Route::post('variants/parallex/{id}', 'API\VariantController@changeParallex');
    Route::post('variants/price/{id}', 'API\VariantController@changePrice');
    Route::post('variants/salepercent/{id}', 'API\VariantController@changeSalePercentage');
    Route::post('variants/sale/{id}', 'API\VariantController@changeSale');

    Route::get('banners', 'API\BannerController@index');
    Route::post('banners', 'API\BannerController@store');
    Route::post('banners/video', 'API\BannerController@storevideo');
    Route::post('banners/status/{id}', 'API\BannerController@changestatus');
    Route::delete('banners/video/{id}', 'API\BannerController@destroyvideo');
    Route::put('banners/video/{id}', 'API\BannerController@updatevideo');
    Route::put('banners/{id}', 'API\BannerController@update');
    Route::delete('banners/{id}', 'API\BannerController@destroy');

    Route::get('settings', 'API\SettingController@index');
    Route::post('settings', 'API\SettingController@update');
  
    Route::get('coupons', 'API\CouponController@index');
    Route::post('coupons', 'API\CouponController@store');
    Route::put('coupons/{id}', 'API\CouponController@update');
    Route::delete('coupons/{id}', 'API\CouponController@destroy');

    Route::get('colors', 'API\ColorController@index');
    Route::post('colors', 'API\ColorController@store');
    Route::put('colors/{id}', 'API\ColorController@update');
    Route::delete('colors/{id}', 'API\ColorController@destroy');

    Route::get('segments', 'API\SegmentController@index');
    Route::post('segments', 'API\SegmentController@store');
    Route::put('segments/{id}', 'API\SegmentController@update');
    Route::delete('segments/{id}', 'API\SegmentController@destroy');

    Route::get('notices', 'API\NoticeController@index');
    Route::post('notices', 'API\NoticeController@store');
    Route::put('notices/{id}', 'API\NoticeController@update');
    Route::delete('notices/{id}', 'API\NoticeController@destroy');
    Route::post('notices/status/{id}', 'API\NoticeController@changeStatus');

    Route::get('dealers', 'API\DealerController@index');
    Route::post('dealers', 'API\DealerController@store');
    Route::put('dealers/{id}', 'API\DealerController@update');
    Route::delete('dealers/{id}', 'API\DealerController@destroy');

    Route::get('pages', 'API\PageController@index');
    Route::post('pages', 'API\PageController@store');
    Route::put('pages/{id}', 'API\PageController@update');
    Route::delete('pages/{id}', 'API\PageController@destroy');

    Route::get('sizes', 'API\SizeController@index');
    Route::post('sizes', 'API\SizeController@store');
    Route::put('sizes/{id}', 'API\SizeController@update');
    Route::delete('sizes/{id}', 'API\SizeController@destroy');
 
    Route::get('messages', 'API\MessageController@index');
    Route::put('messages/{id}', 'API\MessageController@update');
    Route::delete('messages/{id}', 'API\MessageController@destroy');
    Route::post('messages/send/{id}', 'FunctionController@replyMessage');

    Route::get('subscribers', 'API\SubscriberController@index');
    Route::post('subscribers', 'API\SubscriberController@store');
    Route::put('subscribers/{id}', 'API\SubscriberController@update');
    Route::delete('subscribers/{id}', 'API\SubscriberController@destroy');

    Route::get('productcategories', 'API\ProductCategoryController@index');
    Route::post('productcategories', 'API\ProductCategoryController@store');
    Route::put('productcategories/{id}', 'API\ProductCategoryController@update');
    Route::delete('productcategories/{id}', 'API\ProductCategoryController@destroy');

    Route::get('reviews', 'API\ProductController@getReviews');
    Route::delete('reviews/{id}', 'API\ProductController@destroyReview');

    Route::get('shippings', 'API\ShippingController@index');
    Route::post('shippings', 'API\ShippingController@store');
    Route::put('shippings/{id}', 'API\ShippingController@update');
    Route::delete('shippings/{id}', 'API\ShippingController@destroy');
    Route::post('shippings/status/{id}', 'API\ShippingController@changeStatus');

    Route::post("mail/sendmail/{id}",'MailController@sendOrderStatusMail');

    

    // Route::patch('settings/profile', 'Settings\ProfileController@update');
    // Route::patch('settings/password', 'Settings\PasswordController@update');
});
