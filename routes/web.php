<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

// Routes For Vuejs ------------------------------------------------------------------------------
Route::get('/admin',['middleware' => 'auth', 'uses' => 'HomeController@vuejs'])->where( 'path', '([A-z\d\-\/_.]+)');
Route::get('/admin/{path}',['middleware' => 'auth', 'uses' => 'HomeController@vuejs'])->where( 'path', '([A-z\d\-\/_.]+)' );

// Routes For Frontend Views ---------------------------------------------------------------------
Route::get('/', 'HomeController@index')->name('index');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('service', 'HomeController@service')->name('service');
Route::get('cart', 'HomeController@cart')->name('cart');
Route::get('checkout', 'HomeController@checkout')->name('checkout');
Route::get('payment', 'HomeController@payment')->name('payment');
Route::get('products/{slug}', 'HomeController@productsingle')->name('productsingle');
Route::get('blog/{slug}', 'HomeController@blogsingle');
Route::get('blog', 'HomeController@blogs');

// Paypal Payment Route ---------------------------------------------------------------------------
Route::get('handle-payment', 'CartController@handlePayPalPayment')->name('make.payment');
Route::get('cancel-payment', 'CartController@paymentPayPalCancel')->name('cancel.payment');
Route::get('payment-success', 'CartController@paymentPayPalSuccess')->name('success.payment');

// Esewa Payment Route -----------------------------------------------------------------------------
Route::any('esewa/success', 'CartController@esewaSuccess')->name('esewa.success');
Route::any('esewa/fail', 'CartController@esewaFail')->name('esewa.fail');

// Shipping Rate Route -----------------------------------------------------------------------------
Route::get('shippingrate/{name}', 'CartController@getShippingRate')->name('shippingrate');

// Search Product Route ----------------------------------------------------------------------------
Route::post('search/products','HomeController@searchProducts')->name('search-products');

// Get Instagram Route ----------------------------------------------------------------------------
Route::get('/instagram', 'FunctionController@getInstagramData');

// Functions for frontend --------------------------------------------------------------------------
Route::post('postcontact', 'FunctionController@postContact');
Route::post('bookride/{id}', 'FunctionController@registerRide');
Route::post('newsubscribe', 'FunctionController@subscribe');
Route::post('review/{id}', 'FunctionController@review');

// cart --------------------------------------------------------------------------------------------
Route::post('cart/add-to-cart-show', 'CartController@addToCartShow')->name('cart.add-to-cart-show');
Route::get('cart/empty-cart', 'CartController@emptyCart')->name('cart.empty-cart');
Route::get('cart/remove-from-cart/{key}', 'CartController@removefromCart')->name('cart.remove-from-cart');
Route::post('cart/update-quantity/{key}', 'CartController@updateQuantity')->name('cart.update-quantity');
Route::post('cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::post('cart/promocode', 'CartController@promocode')->name('cart.promocode');

//For Email ----------------------------------------------------------------------------------------
Route::get("mail/sendbasicemail",'MailController@basic_email');

// products filter ---------------------------------------------------------------------------------
Route::get('{slug?}/products-filter', 'HomeController@productsFilter')->name('products-filter');

// products reviews ---------------------------------------------------------------------------------
Route::get('{slug?}/products-review', 'HomeController@productReview')->name('products-review');

// blogs filter ---------------------------------------------------------------------------------
Route::get('about/blogs/{tag}', 'HomeController@blogFilter')->name('blog-filter');

// products categories -----------------------------------------------------------------------------
Route::get('brand/{slug}/{category}', 'HomeController@brandShop')->name('brand-shop');
Route::get('brand/{slug}', 'HomeController@brandShop')->name('brand-shop');
Route::get('{slug}', 'HomeController@shop')->name('shop');
Route::get('{slug}/{category}', 'HomeController@shop')->name('shopindivisual');


Route::get('category/{slug}','HomeController@getcategoryproduct');

//for ajax call on product image
Route::get('getproductimg/{id}','HomeController@getproductimage');






