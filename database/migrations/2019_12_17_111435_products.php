<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->longText('short_description')->nullable();
            $table->longText('features')->nullable();
            $table->float('price');
            $table->float('sale_price')->nullable();
            $table->boolean('sale')->default(false);
            $table->boolean('featured')->default(false);
            $table->boolean('published')->default(false);
            $table->boolean('is_highlighted')->default(false);
            $table->boolean('out_of_stock')->default(false);
            $table->bigInteger('view')->nullable();
            $table->bigInteger('color_id')->unsigned()->nullable()->default(null);
            $table->foreign('color_id')->references('id')->on('colors')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('brand_id')->unsigned()->nullable()->default(null);
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('set null');
            $table->integer('product_category_id')->unsigned()->nullable()->default(null);
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
