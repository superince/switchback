<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->bigInteger('quantity');
            $table->float('price');
            $table->string('size')->nullable();
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->string('color')->nullable();
            $table->integer('customer_order_id')->unsigned()->nullable()->default(null);
            $table->foreign('customer_order_id')->references('id')->on('customer_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
