<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Banners extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->text('location');
            $table->string('url')->nullable();
            $table->string('primary_tagline')->nullable();
            $table->text('secondary_tagline')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('type')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
