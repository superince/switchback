<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VariantSize extends Migration
{

    public function up()
    {
        Schema::create('variant_size', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('size_id')->unsigned()->nullable()->default(null);
            $table->foreign('size_id')->references('id')->on('sizes')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('variant_id')->unsigned()->nullable()->default(null);
            $table->foreign('variant_id')->references('id')->on('variants')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('stock')->default(0);
            $table->string('sku')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variant_size');
    }
}
