<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('country');
            $table->string('state');
            $table->string('address');
            $table->string('zip_code')->nullable();
            $table->string('email');
            $table->bigInteger('phone');
            $table->bigInteger('secondary_phone');
            $table->string('payment_method');
            $table->string('status')->nullable();
            $table->float('total');
            $table->bigInteger('coupon_id')->unsigned()->nullable()->default(null);
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_orders');
    }
}
