<?php

use Illuminate\Database\Seeder;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert([
            'name' => 'Moto',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Street',
        ]);
        DB::table('divisions')->insert([
            'name' => 'MTB',
        ]);
        DB::table('divisions')->insert([
            'name' => 'Casual',
        ]);
    }
}
