<?php

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            'name' => 'Helmet',

        ]);
        DB::table('product_categories')->insert([
            'name' => 'Shirt',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Pants',
        ]);
        DB::table('product_categories')->insert([
            'name' => 'Boots',

        ]);
    }
}
