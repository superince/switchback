<?php

use Illuminate\Database\Seeder;

class ShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shippings')->insert([
            'address' => 'Province No.1',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.2',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.3',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.4',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.5',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.6',
            'rate' => '0.0'
        ]);
        DB::table('shippings')->insert([
            'address' => 'Province No.7',
            'rate' => '0.0'
        ]);
    }
}
