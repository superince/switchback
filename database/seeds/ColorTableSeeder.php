<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            'name' => 'Red',
            'hex'=>'#ff0000',
            'group'=>"Red"
        ]);
        DB::table('colors')->insert([
            'name' => 'Blue',
            'hex'=>'#0000ff',
            'group'=>"Blue"
        ]);
        DB::table('colors')->insert([
            'name' => 'Green',
            'hex'=>'#00ff00',
            'group'=>"Green"
        ]);
        DB::table('colors')->insert([
            'name' => 'Yellow',
            'hex'=>'#ffff00',
            'group'=>"Yellow"
        ]);
        DB::table('colors')->insert([
            'name' => 'Grey',
            'hex'=>'#808080',
            'group'=>"Grey"
        ]);
        DB::table('colors')->insert([
            'name' => 'White',
            'hex'=>'#ffffff',
            'group'=>"White"
        ]);
    }
}
