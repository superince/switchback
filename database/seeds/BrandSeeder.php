<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'name' => 'Switchback',
        ]);
        DB::table('brands')->insert([
            'name' => 'Flyracing',
        ]);
    }
}
