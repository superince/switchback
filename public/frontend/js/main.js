///////////////// fixed menu on scroll for desktop
$(window).scroll(function(){  
 if ($(this).scrollTop() > 40) {
    $('.navbar').removeClass("transparent-bg");
  }else{
    $('.navbar').addClass("transparent-bg");
  }   
});


var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").classList.remove("invisible-nav");
  } else {
    document.getElementById("navbar").classList.add("invisible-nav");
  }
  prevScrollpos = currentScrollPos;
}


const $dropdown = $(".dropdown");
const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

$(window).on("load resize", function() {
  if (this.matchMedia("(min-width: 768px)").matches) {
    $dropdown.hover(
      function() {
        const $this = $(this);
        $this.addClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "true");
        $this.find($dropdownMenu).addClass(showClass);
      },
      function() {
        const $this = $(this);
        $this.removeClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "false");
        $this.find($dropdownMenu).removeClass(showClass);
      }
    );
  } else {
    $dropdown.off("mouseenter mouseleave");
  }
});

// search trigger
$("#search-trigger").click(function(){
	gsap.to("#search-overlay", {duration: 0, display: "block", zIndex: "99999", y: "-20%", opacity: 1});
	gsap.to("#search-overlay", {duration: 0.3, opacity: 1, y: "0%", ease: "power4.outout"});
	gsap.from("#search-overlay .search-input", {duration: 0.5, scaleX: 0, ease: "power4.outout", delay: 0.3});
	gsap.from("#search-overlay .search-input .form-control", {duration: 0.3, opacity: 0, y: 20, ease: "power4.outout", delay: 1});
	gsap.from("#search-overlay .search-input .btn", {duration: 0.2, opacity: 0, y: 3, ease: "power4.outout", delay: 1.1});
});
$(".search-overlay .close").click(function(){
	gsap.to("#search-overlay", {duration: 0.4, y: "-100%", ease: "power4.outout"});
	gsap.to("#search-overlay", {duration: 0, zIndex: "-99999", delay: 1});
});