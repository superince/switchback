<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    public $timestamps = false;
    protected $table = 'product_size';
    protected $fillable = [
        'name','product_id'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
