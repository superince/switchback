<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'color','photo','product_id','color_id'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
