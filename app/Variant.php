<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    public function color()
    {
        return $this->belongsTo('App\Color');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Size', 'variant_size')->withPivot(['stock','sku']);
    }

    public function photos()
    {
        return $this->hasMany('App\VariantImage');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
