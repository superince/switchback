<?php

namespace App\Http\Controllers;

use App\Color;
use App\Coupon;
use App\CustomerOrder;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductSize;
use App\Shipping;
use App\VariantSize;
use DB;
use Illuminate\Http\Request;
use Mail;
use Srmklive\PayPal\Services\ExpressCheckout;

class CartController extends Controller
{
    public function addToCartShow(Request $request)
    {
        $cart = $this->addToCart($request);

        $cart_products = $cart['cart_products'];
        $cart_subtotal = $cart['cart_subtotal'];
        return view('frontend.includes._partials.cart', compact('cart_products', 'cart_subtotal'))->render();
    }

    public function addToCart(Request $request)
    {
        $cart_products = session()->get('cart_products') ?: array();
        $cart_subtotal = session()->get('cart_subtotal') ?: 0;

        $product = Product::with('photos', 'brand')->find($request->product_id);

        $color = Color::find($request->color);
        $product->cart_color = $color;
        $product->cart_size = $request->size;
        $product->cart_quantity = $request->quantity;

        $products_data = $cart_products;

        $find_product_in_cart = collect($products_data)
            ->where('id', $request->product_id)
            ->where('cart_size', $request->size)
            ->where('cart_color', $color);

        if (count($find_product_in_cart)) {
            foreach ($find_product_in_cart as $key => $find_product) {
                if ($request->stock >= ($products_data[$key]['cart_quantity'] + $request->quantity)) {
                    $products_data[$key]['cart_quantity'] += $request->quantity;
                    $products_data[$key]['cart_stock'] = $request->stock;

                    //Calculate total price
                    $product_total_price = 0;
                    if ($product->sale == 1) {
                        $product_total_price = ($product->price - $product->sale_price / 100 * $product->price) * $request->quantity;
                    } else {
                        $product_total_price = $product->price * $request->quantity;
                    }

                    session()->put('cart_subtotal', $cart_subtotal += $product_total_price);
                    session()->save();
                }
            }
        } else {
            array_push($products_data, $product);
            $product['cart_stock'] = $request->stock;
            //Calculate total price
            $product_total_price = 0;
            if ($product->sale == 1) {
                $product_total_price = ($product->price - $product->sale_price / 100 * $product->price) * $request->quantity;
            } else {
                $product_total_price = $product->price * $request->quantity;
            }
            session()->put('cart_subtotal', $cart_subtotal += $product_total_price);
            session()->save();
        }

        session()->put('cart_products', $products_data);
        session()->save();

        return [
            'cart_products' => session()->get('cart_products'),
            'cart_subtotal' => session()->get('cart_subtotal'),
        ];
    }

    public function removefromCart($key)
    {
        $cart_products = session()->get('cart_products');
        $cart_subtotal = session()->get('cart_subtotal');

        //get cart product and total price
        $cart_product = $cart_products[$key];
        $cart_product_total_price = 0;
        if ($cart_product->sale == 1) {
            $cart_product_total_price = ($cart_product->price - $cart_product->sale_price / 100 * $cart_product->price) * $cart_product->cart_quantity;
        } else {
            $cart_product_total_price = $cart_product->price * $cart_product->cart_quantity;
        }

        //Update cart total
        unset($cart_products[$key]);
        session()->put('cart_products', $cart_products);
        $cart_subtotal = $this->calculateAndUpdateSubTotal($cart_products);
        session()->save();

        if (!request()->expectsJson()) {
            return redirect()->back();
        }

        return view('frontend.includes._partials.cart', compact('cart_products', 'cart_subtotal'))->render();
    }

    public function updateQuantity($key, Request $request)
    {
        $cart_products = session()->get('cart_products') ?: array();
        $cart_subtotal = session()->get('cart_subtotal');

        //Get product
        $product = $cart_products[$key];

        //update product quantity
        $product->cart_quantity = $request->quantity;
        if ($product->sale == 1) {
            $product_total_main = $product->cart_quantity * ($product->price - $product->sale_price / 100 * $product->price);
        } else {
            $product_total_main = $product->cart_quantity * $product->price;
        }

        $total_amount = $this->calculateAndUpdateSubTotal($cart_products);

        return redirect()->back();
    }

    public function emptyCart(Request $request)
    {
        $request->session()->forget('cart_products');
        $request->session()->forget('cart_subtotal');
        $request->session()->forget('cart_discount');
        $request->session()->forget('cart_shipping');
        $request->session()->forget('promocode');

        return redirect()->back();
    }

    public function promocode(Request $request)
    {
        $cart_subtotal = session()->get('cart_subtotal') ?: 0;
        $coupon = Coupon::where('code', $request->code)
            ->first();

        if ($coupon->used < $coupon->limit && $coupon->cart_limit <= $cart_subtotal && date('Y-m-d') <= $coupon->expiry) {
            $discount = $coupon->amount;
            if ($coupon->type == 1) {
                $discount = $cart_subtotal * $coupon->amount / 100;
            }

            session()->put('cart_discount', $discount);
            session()->put('promocode', $coupon);
            session()->save();

            return redirect()->back()->withNotify('Coupon applied');
        }

        return redirect()->back()->withNotify('Invalid coupon');
    }

    public function getShippingRate($name, Request $request)
    {
        $shipping = Shipping::where('address', '=', $name)->first();
        session()->put('cart_shipping', $shipping->amount);
        session()->save();
        return view('frontend.pages.shipping', compact('shipping'))->render();
    }

    public function calculateAndUpdateSubTotal($cart_products)
    {
        $total_amount = 0;
        foreach ($cart_products as $key => $cart_product) {
            if ($cart_product->sale == 1) {
                $product_total = $cart_product->cart_quantity * ($cart_product->price - $cart_product->sale_price / 100 * $cart_product->price);
            } else {
                $product_total = $cart_product->cart_quantity * $cart_product->price;
            }
            $total_amount += $product_total;
        }

        session()->put('cart_subtotal', $total_amount);

        return $total_amount;
    }

    public function checkout(Request $request)
    {
			$this->validate($request, [
					'firstname' => 'required',
					'lastname' => 'required',
					'email' => 'required',
					'phone' => 'required',
					'secondary_phone' => 'required',
					'address' => 'required',
					"payment_method" => "required",
					"country" => "required",
					"state" => "required",
			]);

			// if ($request->payment_method == 'paypal') {
			// 		$product=$this->handlePayPalPayment($request);
			// 		$paypalModule = new ExpressCheckout;
			// 		$res = $paypalModule->setExpressCheckout($product);
			// 		$res = $paypalModule->setExpressCheckout($product, true);
			// 		return redirect($res['paypal_link']);
			// } else if ($request->payment_method == 'cashondelivery') {
					$msg=$this->placeOrder($request);
					return redirect()->route('index')->withNotify($msg);
			// } else if ($request->payment_method == 'esewa') {
			// 		$this->handleEsewaPayment($request);
			// }
    }

    public function paymentPayPalCancel()
    {
        $notify_message = "Your payment has been declind.Failed to checkout, Try again";
        return redirect()->back()->withNotify($notify_message);
    }

    public function paymentPayPalSuccess(Request $request)
    {
        $paypalModule = new ExpressCheckout;
        $response = $paypalModule->getExpressCheckoutDetails($request->token);

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
						dd($request->all());
						$notify_message = "Checkout was successfull.";
						return redirect()->back()->withNotify($notify_message);
        }

        // dd('Error occured!');
        $notify_message = "Your payment has been declind.Failed to checkout, Try again";
        return redirect()->back()->withNotify($notify_message);
    }

    public function handlePayPalPayment(Request $request)
    {
			$product = [];
			$product['items'] = [];

			// get cart details
			$cart_products = session()->get('cart_products') ?: array();
			$cart_subtotal = session()->get('cart_subtotal');
			$cart_discount = session()->get('cart_discount') ?: 0;
			$cart_shipping = session()->get('cart_shipping');
			// foreach ($cart_products as $cart_product) {
			// 	$temp=[
			// 		"name"=>$cart_product->name,
			// 		"price"=>$cart_product->price,
			// 		"desc"=>$cart_product->cart_size,
			// 		"qty"=>$cart_product->cart_quantity
			// 	];
			// 	array_push($product['items'],$temp);
			// }
	
			$product['invoice_id'] = time();
			$product['invoice_description'] = "Order #{$product['invoice_id']} Bill";
			$product['return_url'] = route('success.payment');
			$product['cancel_url'] = route('cancel.payment');
			$product['total'] = $cart_subtotal - $cart_discount + $cart_shipping;
			return $product;
    }

    public function esewaSuccess(Request $request)
    {
        if(isset($request->pid) && isset($request->amt) && isset($request->refId)){
            $order=Order::where('invoice_no',$request->pid)->first();
            if($order){

            }

            $url = "https://uat.esewa.com.np/epay/main";
            $data =[
                'amt'=> $order->total,
                'pdc'=> 0,
                'psc'=> 0,
                'txAmt'=> 0,
                'tAmt'=> 100,
                'pid'=>$order->invoice_no,
                'scd'=> 'EPAYTEST',
                'su'=>'http://merchant.com.np/page/esewa_payment_success?q=su',
                'fu'=>'http://merchant.com.np/page/esewa_payment_failed?q=fu'
            ];

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
        }
    }

    public function esewaFail(Request $request)
    {
        
    }

    public function placeOrder(Request $request)
    {
        // get cart details
        $cart_products = session()->get('cart_products') ?: array();
        $cart_subtotal = session()->get('cart_subtotal');
        $cart_discount = session()->get('cart_discount') ?: 0;
        $cart_shipping = session()->get('cart_shipping');
        $promocode = session()->get('promocode');

        DB::beginTransaction();
        try {
            //save order
            $request['subtotal'] = $cart_subtotal;
            $request['discount'] = $cart_discount;
            $request['total'] = $cart_subtotal - $cart_discount + $cart_shipping;
            $request['shipping'] = $cart_shipping;
            $request['status'] = "Pending";
            $request['order_no'] = time();
            $order = CustomerOrder::create($request->all());
            // if($request->payment_method=='fonepay'){
            //     $fonepay=[];
            //     $fonepay['RU']=route('fonepay.return');
            //     $fonepay['PID']='4012001037141112';
            //     $fonepay['PRN']=$order['order_no'];
            //     $fonepay['AMT']=$request['total'];
            //     $fonepay['CRN']='NPR';
            //     $fonepay['DT']=date('m/d/Y');
            //     $fonepay['R1']='Test';
            //     $fonepay['R2']='Test';
            //     $fonepay['MD']='P';

            //     $data=$fonepay['PID'].','
            //     $fonepay['MD'].','
            //     $fonepay['PRN'].','
            //     $fonepay['AMT'].','
            //     $fonepay['CRN'].','
            //     $fonepay['DT'].','
            //     $fonepay['R1'].','
            //     $fonepay['R2'].','
            //     $fonepay['RU'];

            //     $fonepay['DV']=hash_hmac('sha512',$data,'MTIzNDU2Nzg5NDA1NjM1MDAwMDA2M');
            // }
            // return $request;

            //save order products
            foreach ($cart_products as $cart_product) {
                $order_product = new OrderProduct;
                $order_product->customer_order_id = $order->id;
                $order_product->product_id = $cart_product->id;
                $order_product->quantity = $cart_product->cart_quantity;
                if ($order_product->sale == 1) {
                    $order_product->price = $cart_product->price - $cart_product->sale_price / 100 * $cart_product->price;
                } else {
                    $order_product->price = $cart_product->price;
                }

                $order_product->size = $cart_product->cart_size;
                $order_product->name = $cart_product->name;
                $order_product->photo = $cart_product->photos[0]->photo;
                $order_product->color = $cart_product->cart_color->hex;
                $order_product->save();
                $product = ProductSize::where('product_id', '=', $cart_product->id)->first();
                if ($product !== null) {
                    $product->stock = $product->stock - $cart_product->cart_quantity;
                    $product->save();
                } else {
                    $variant = VariantSize::where('variant_id', '=', $cart_product->id)->first();
                    $variant->stock = $variant->stock - $cart_product->cart_quantity;
                    $variant->save();
                }
            }

            if ($promocode != null) {
                // use promocode
                $promocode->used += 1;
                $promocode->save();
            }

            // empty cart
            $request->session()->forget('cart_products');
            $request->session()->forget('cart_subtotal');
            $request->session()->forget('cart_discount');
            $request->session()->forget('cart_shipping');
            $request->session()->forget('promocode');

            DB::commit();
            $details = [
                'name' => $request['firstname'],
            ];
            Mail::to($request['email'])->send(new \App\Mail\PurchaseSender($details));
            $notify_message = "Checkout successfull.";

        } catch (Exception $e) {
            DB::rollback();
            $notify_message = "Failed to checkout, Try again";
        }

        return $notify_message;
    }

}
