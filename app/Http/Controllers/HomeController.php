<?php

namespace App\Http\Controllers;

use App\About;
use App\Banner;
use App\Blog;
use App\BlogTag;
use App\Brand;
use App\Color;
use App\Dealer;
use App\Division;
use App\GroupSize;
use App\Notice;
use App\Page;
use App\Product;
use App\ProductCategory;
use App\ProductReview;
use App\Setting;
use App\Shipping;
use App\Size;
use App\Team;
use App\Variant;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $_data = [
        'divisions' => null,
        'brands' => null,
        'static_content' => null,
        'pages' => null,
    ];

    public function __construct()
    {
        // $this->_data['divisions'] = Division::with(['products:name,product_category_id', 'products.productCategory'])
        //     ->get(['id', 'name', 'banner', 'highlight']);
        $divisions = Division::with(['products:name,product_category_id', 'products.productCategory'])
            ->get(['id', 'name', 'banner', 'highlight']);

        $category = [];
        foreach ($divisions as $division) {
            foreach ($division->products as $product) {
                array_push($category, $product->productCategory->name);
            }
            $uniqCat = array_unique($category);
            $division->setAttribute('categories', $uniqCat);
            $category = [];
        }
        $this->_data['divisions'] = $divisions;
        $this->_data['brands'] = Brand::get(['id', 'name', 'banner', 'highlight']);
        $this->_data['pages'] = Page::All();
        $this->_data['static_content'] = Setting::All();

        view()->share($this->_data);

        $this->middleware(function ($request, $next) {
            $cart_products = session()->get('cart_products') ?: [];
            $cart_subtotal = session()->get('cart_subtotal') ?: 0;
            $cart_discount = session()->get('cart_discount') ?: 0;

            view()->share(compact('cart_products', 'cart_subtotal', 'cart_discount'));

            return $next($request);
        });
    }

    public function vuejs()
    {
        return view('home');
    }

    public function payment()
    {
        return view('frontend.pages.payment');
    }

    public function index()
    {

        $parallaxproductdata = Product::with(['photos:id,photo,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])->where('published', 1)->where('is_highlighted', '=', '1')->take(4)->get();
        if ($parallaxproductdata->count() <= 4) {
            $parallaxproductdatanext = Variant::with(['photos', 'sizes', 'color', 'product', 'product.photos', 'product.color', 'product.brand', 'product.divisions', 'product.productCategory'])->where('is_highlighted', '=', 1)->where('published', '=', 1)->take(4 - $parallaxproductdata->count())->get();

            foreach ($parallaxproductdatanext as $px) {
                $px->product['photos'] = $px->photos;
                $px->product['price'] = $px->price;
                $px->product['sale_price'] = $px->sale_price;
                $parallaxproductdata->add($px['product']);
            }
        }
        $newproducts = Product::with(['photos:id,photo,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])->latest()->limit(7)->where('published', 1)->get();
        $popularproductdatas = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])->orderBy('view', 'DESC')->limit(7)->where('published', 1)->get();
        $featuredproductdatas = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])->where('featured', '=', '1')->where('published', 1)->limit(7)->get();
        $highlights = Division::take(3)->get();
        $videobanner = Banner::where('type', '=', 1)->where('status', 1)->get();
        $notice = Notice::where('status', 1)->get();
        $bannertype = 'video';
        $banner = null;
        if ($videobanner->isEmpty()) {
            $photobanner = Banner::where('type', '=', 0)->where('status', 1)->get();
            $banner = $photobanner;
            $bannertype = 'photo';
        } else {
            $banner = $videobanner;
        }
        $blogs = Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id'])->orderBy('id','DESC')->take(6)->get();
        return view('frontend.pages.index', compact('blogs', 'newproducts', 'parallaxproductdata', 'popularproductdatas', 'featuredproductdatas', 'highlights', 'banner', 'bannertype', 'notice'));
    }

    public function about()
    {
        $team = Team::with(['segment:id,name'])->get();
        $about = About::latest()->get();
        $dealers = Dealer::All();
        $tags = BlogTag::select('name')->distinct()->get();
        return view('frontend.pages.about', compact('team', 'about', 'dealers', 'tags'));
    }

    public function contact()
    {
        return view('frontend.pages.contact');
    }

    public function service()
    {
        $services = $this->_data['pages'];
        return view('frontend.pages.service', compact('services'));
    }

    public function cart()
    {
        return view('frontend.pages.cart');
    }

    public function checkout()
    {
        $shippings = Shipping::where('status', '=', 1)->get();
        return view('frontend.pages.checkout', compact('shippings'));
    }

    public function shop($name, $cat = '', Request $request)
    {

        $brand = Division::where('name', '=', $name)->firstOrFail();
        $raw = Division::with(['products:name,product_category_id', 'products.productCategory'])
            ->whereHas('products', function ($q) use ($brand) {
                $q->where('published', 1);
            })->where('name', '=', $name)->first();
        $categorydata = [];
        if ($raw != null) {
            foreach ($raw['products'] as $product) {
                $prod = json_decode($product);
                array_push($categorydata, ["name" => $prod->product_category->name]);
            }
        }

        $categorydata = array_unique($categorydata, SORT_REGULAR);

        $product_category = ProductCategory::all();
        $sizedata = GroupSize::with("sizes")->get();
        $colordata = Color::all();
        $branddata = $this->_data['brands'];

        $relatedproductdata = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])
            ->whereHas('divisions', function ($q) use ($name) {
                $q->where('name', '=', $name);
            })
            ->where('published', 1)
            ->orderBy('view', 'desc')
            ->limit(7)->get();

        return view('frontend.pages.productshop', compact(
            'brand', 'branddata', 'categorydata', 'sizedata', 'colordata', 'relatedproductdata', 'cat'
        ));
    }

    public function brandShop($name, $cat = null)
    {
        $brand = Brand::where('name', '=', $name)->firstOrFail();
        $pcat = ProductCategory::with(['products:product_category_id'])
            ->whereHas('products', function ($q) use ($brand) {
                $q->where('brand_id', $brand->id);
            })
            ->whereHas('products', function ($q) {
                $q->where('published', 1);
            })
            ->get();

        $categorydata = [];
        foreach ($pcat as $catz) {
            array_push($categorydata, ["name" => $catz['name']]);
        }
        $branddata = $this->_data['brands'];
        // $categorydata = ProductCategory::get();
        $sizedata = GroupSize::with("sizes")->get();
        $colordata = Color::get();
        $relatedproductdata = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])
            ->whereHas('brand', function ($q) use ($name) {
                $q->where('name', '=', $name);
            })
            ->where('published', 1)
            ->orderBy('view', 'desc')
            ->limit(7)->get();

        return view('frontend.pages.productshop', compact('brand', 'branddata', 'categorydata', 'sizedata', 'colordata', 'relatedproductdata', 'cat'));
    }

    public function productsFilter($name, Request $request)
    {
        $product_query = Product::with(
            [
                'photos:id,photo,product_id',
                'color:id,name,hex',
                'brand:id,name,banner',
                'divisions:name',
                'productCategory:id,name',
                'sizes:name,group_size_id',
                'productCategory',
                'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
                    $q->where('published', '=', 1);
                },
            ]
        );

        if ($request->type == 'division') {
            $product_query->whereHas('divisions', function ($q) use ($name) {
                $q->where('name', '=', $name);
            });
        }

        $filter_sizes = json_decode($request->size);
        if (count($filter_sizes)>0) {
            $product_query->whereHas('sizes', function ($q) use($filter_sizes) {
                foreach($filter_sizes as $key => $filter_size){
                    $filters = explode('-', $filter_size);
                    // dd($filters[1]);
                    if($key == 0){
                        $q->where(function($q) use ($filters) {
                            $q->where('name',$filters[1] )
                            ->where('group_size_id', $filters[0]);
                        });
                    }
                    $q->orWhere(function($q) use ($filters) {
                        $q->where('name',$filters[1] )
                        ->where('group_size_id', $filters[0]);
                    });
                }
            });
        }
        $filter_colors = json_decode($request->color);
        if (count($filter_colors)>0) {
            $product_query->where(function ($query) use ($filter_colors) {
                $query->whereHas('variants', function ($q) use ($filter_colors) {
                    foreach($filter_colors as $key => $filter_color){
                        if($key == 0){
                            $q->where('color_id', '=', $filter_color);
                        }
                        $q->orWhere('color_id', '=', $filter_color);
                    }
                });

                $query->orWhereHas('color', function ($q) use ($filter_colors) {
                    foreach($filter_colors as $key => $filter_color){
                        if($key == 0){
                            $q->where('id', '=', $filter_color);
                        }
                        $q->orWhere('id', '=', $filter_color);
                    }
                });
            });
        }
        $filter_brands = json_decode($request->brand);
        if (count($filter_brands)>0) {
            $product_query->whereHas('brand', function ($q) use($filter_brands) {
            foreach($filter_brands as $key => $filter_brand){
                    if($key == 0){
                        $q->where('name', '=', $filter_brand);
                    }
                    $q->orWhere('name', '=', $filter_brand);
                }
            });
        }
        if ($request->startPrice || $request->endPrice) {
            $product_query->whereBetween('price', [request()->startPrice, request()->endPrice]);
        }
        if ($request->category) {
            $product_query->whereHas('productCategory', function ($q) {
                $q->where('name', '=', request()->category);
            });
        }

        $productdata = $product_query->where('published', 1)->paginate(12);
        // dd($productdata);
        return view('frontend.pages.products_filter', compact('productdata'))->render();
    }

    public function blogFilter($tag, Request $request)
    {
        if ($tag === "rides") {
            $blogs = Blog::with(["photos", "tags"])->where("registration","=",1)->paginate(6);
            return view('frontend.pages.blogs_filter', compact('blogs'))->render();
        } else {
            $blogs = Blog::with(["photos", "tags"])->whereHas("tags", function ($q) use ($tag) {
                $q->where('name', $tag);
            })->paginate(6);
            return view('frontend.pages.blogs_filter', compact('blogs'))->render();
        }
    }

    public function getShippingRate($name, Request $request)
    {
        $shipping = Shipping::where('address', '=', $name)->first();
        session()->put('cart_shipping', $shipping->amount);
        session()->save();
        // return redirect()->back();
        return view('frontend.pages.shipping', compact('shipping'))->render();
    }

    public function productReview($name, Request $request)
    {
        $product = Product::where('slug', '=', $name)->first();
        $total = ProductReview::where('product_id', '=', $product->id)->count();
        $reviews = ProductReview::where('product_id', '=', $product->id)->paginate(3);
        return view('frontend.pages.reviews_filter', compact('reviews', 'total'))->render();
    }

    public function productsingle($slug, Request $request)
    {
        $productdata = Product::with([
            'photos:id,photo,product_id',
            'tags:id,name,product_id',
            'sizes:name,group_size_id',
            'sizes.group:id,group,sizechart',
            'genders:id,name,product_id',
            'color:id,name,hex',
            'brand:id,name,banner',
            'divisions:name',
            'productCategory:id,name',
            'variants:id,color_id,product_id',
            'variants.photos', 'variants.sizes', 'variants.color',
            "variants" => function ($q) {
                $q->where('published', '=', 1);
            },
        ])->where('slug', $slug)->first();

        $productdata->view++;
        $productdata->save();

        $relatedproduct = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])
            ->whereHas('divisions', function ($q) use ($slug) {
                $q->where('slug', '!=', $slug);
            })
            ->where('published', 1)->limit(7)->get();

        if (!empty($productdata)) {
            return view('frontend.pages.productsingle', compact('productdata', 'relatedproduct'));
        } else {
            return redirect()->back();
        }
    }

    public function searchProducts(Request $request)
    {
        $products = Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name', 'variants', 'variants.photos', 'variants.sizes', 'variants.color', "variants" => function ($q) {
            $q->where('published', '=', 1);
        }])
            ->where('name', 'like', '%' . $request->get('searchQuery') . '%')
            ->where('published', 1)
            ->get();
        return json_encode($products);
    }

    public function blogsingle($slug)
    {
        $blogdata = Blog::with(['photos', 'tags'])
            ->where('slug', $slug)
            ->firstOrFail();
        $relatedblogdata = Blog::latest()->limit(2)->get();

        return view('frontend.pages.blogsingle', compact('blogdata', 'relatedblogdata'));
    }

    public function getcategoryproduct($slug)
    {
        $getcat = ProductCategory::where('name', $slug)->firstOrFail();
        $getproduct = Product::where('product_category_id', $getcat->id)->paginate(16);
        // foreach($getproduct as $pro){

        // }
        // $tagdata=DB::table('product_tags')->where('product_id',$getproduct->id)->get();
        // dd($tagdata);
        $branddata = Brand::get();
        $categorydata = ProductCategory::get();
        $sizedata = Size::get();
        $colordata = Color::get();
        $relatedproductdata = Product::with(['photos:id,photo,product_id', 'color:id,name,hex', 'brand:id,name,banner'])->limit(7)->get();

        return view('frontend.pages.all', compact('getcat', 'getproduct', 'branddata', 'categorydata', 'sizedata', 'colordata', 'relatedproductdata'));
    }

    public function getdivisionproduct($slug)
    {
        $getcat = Division::where('name', $slug)->firstOrFail();
        $branddata = Brand::get();
        $categorydata = ProductCategory::get();
        $sizedata = Size::get();
        $colordata = Color::get();
        $relatedproductdata = Product::with(['photos:id,photo,product_id', 'color:id,name,hex', 'brand:id,name,banner'])->limit(7)->get();
        return view('frontend.pages.division', compact('getcat', 'branddata', 'categorydata', 'sizedata', 'colordata', 'relatedproductdata'));
    }

    public function getbrandproduct($slug)
    {
        // try {
        //     $division = Division::where('name', '=', $name)->firstOrFail();

        //     $raw = Division::with(['products:name,product_category_id', 'products.productCategory'])
        //         ->whereHas('products', function ($q) use ($division) {
        //             $q->where('published', 1);
        //         })->where('name', '=', $name)->first();

        //     $semiRaw = [];
        //     foreach ($raw['products'] as $product) {
        //         $prod = json_decode($product);
        //         array_push($semiRaw, ["name" => $prod->product_category->name]);
        //     }

        //     $product_category = ProductCategory::All();
        //     $products = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
        //         ->whereHas('divisions', function ($q) use ($name) {
        //             $q->where('name', '=', $name);
        //         })
        //         ->where('published', 1)
        //         ->paginate(6);

        //     // return ['brand' => $division, 'product_category' => $semiRaw, 'products' => $products];
        //     return view('frontend.pages.all')->with('brand',$division)->with('product_category',$semiRaw)->with('products',$products);
        // } catch (Exception $e) {
        //     $brand = Brand::where('name', '=', $name)->firstOrFail();
        //     $pcat = ProductCategory::with(['products:product_category_id'])
        //         ->whereHas('products', function ($q) use ($brand) {
        //             $q->where('brand_id', $brand->id);
        //         })
        //         ->whereHas('products', function ($q) {
        //             $q->where('published', 1);
        //         })
        //         ->get();

        //     $products = Product::with(['photos:photo,product_id,color_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'colors:name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
        //         ->where('brand_id', $brand->id)
        //         ->where('published', 1)
        //         ->paginate(6);

        //     $product_category = [];
        //     foreach ($pcat as $cat) {
        //         array_push($product_category, ["name" => $cat['name']]);
        //     }
        //     return view('frontend.pages.all', compact('brand', 'product_category', 'products'));
        // }
        $getcat = Brand::where('name', $slug)->firstOrFail();

        $getproduct = Product::where('brand_id', $getcat->id)->paginate(16);

        $branddata = Brand::get();
        $categorydata = ProductCategory::get();
        $sizedata = Size::get();
        $colordata = Color::get();

        $relatedproductdata = Product::with(['photos:id,photo,product_id', 'color:id,name,hex', 'brand:id,name,banner'])->limit(7)->get();
        return view('frontend.pages.all', compact('getcat', 'getproduct', 'branddata', 'categorydata', 'sizedata', 'colordata', 'relatedproductdata'));
    }

    public function getproductimage($id)
    {
        // $productdata=Product::where('id',$id)->firstOrFail();
        // $productid=$productdata->id;
        // $variantdata=Variant::where('product_id',$productid)->with('color')->get();

        return $id;
    }

    public function blogs()
    {
        $tags = BlogTag::select('name')->distinct()->get();
        return view('frontend.pages.blogs', compact('tags'));
    }

}
