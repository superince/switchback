<?php
namespace App\Http\Controllers;

use App\CustomerOrder;
use App\Order;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    public function basic_email()
    {

        $details = [
            'title' => 'Your Order has been placed.',
            'body' => 'This is for testing email using smtp'
        ];

        Mail::to('suprincezzz@gmail.com')->send(new \App\Mail\MailSender($details));
        dd("Email is Sent.");
    }

    public function sendOrderStatusMail(Request $request,$id){

        $rules = [
            'status' => 'required|string|max:20'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }
 
        if($request['status']==="Ready to ship"){
            $order=CustomerOrder::where('order_no','=',$id)->first();
           
            $details = [
                'order_no'=>$id
            ];
    
            Mail::to($order->email)->send(new \App\Mail\ReadyToShipSender($details));
            $order->email_sent=1;
            $order->save();
            return $order;
        }else if($request['status']==="Shipped"){
            $order=CustomerOrder::where('order_no','=',$id)->first();
           
            $details = [
                'order_no'=>$id
            ];
    
            Mail::to($order->email)->send(new \App\Mail\ShippedSender($details));
            $order->email_sent=1;
            $order->save();
            return $order;
        }else if($request['status']==="Returned"){
            $order=CustomerOrder::where('order_no','=',$id)->first();
           
            $details = [
                'order_no'=>$id
            ];
    
            Mail::to($order->email)->send(new \App\Mail\ReturnedSender($details));
            $order->email_sent=1;
            $order->save();
            return $order;
        }else if($request['status']==="Cancel"){
            $order=CustomerOrder::where('order_no','=',$id)->first();
           
            $details = [
                'order_no'=>$id
            ];
    
            Mail::to($order->email)->send(new \App\Mail\CancelSender($details));
            $order->email_sent=1;
            $order->save();
            return $order;
        }else if($request['status']==="Delivered"){
            $order=CustomerOrder::where('order_no','=',$id)->first();
           
            $details = [
                'order_no'=>$id
            ];
    
            Mail::to($order->email)->send(new \App\Mail\DeliveredSender($details));
            $order->email_sent=1;
            $order->save();
            return $order;
        }
       
    }
}
