<?php

namespace App\Http\Controllers;

use App\BlogRide;
use App\Message;
use App\Notifications\NewMessage;
use App\Notifications\NewSubscription;
use App\ProductReview;
use App\Subscriber;
use App\User;
use Illuminate\Http\Request;
use Session;
use Mail;
use GuzzleHttp\Client;

class FunctionController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required|string|email|max:80|unique:subscribers,email',
        ));
        $data = [
            'email' => $request->email,
        ];

        Session::flash('flash_success', 'Thank you for subscribing.');
        Session::flash('flash_type', 'alert-success');

        Subscriber::create($data);
        $user=User::where('type','Admin')->first();
        $user->notify(new NewSubscription);
        $details = [
            'body' => 'Thank you for subscribing to our newsletter. Exciting things are on your way.',
            'subject'=>'Thank you for subscribing'
        ];

        Mail::to($request['email'])->send(new \App\Mail\MailSender($details));
        return redirect()->back();
    }

    public function review(Request $request, $id)
    {
        $this->validate($request, array(
            'email' => 'required|string|email|max:80',
            'title' => 'required|string|max:20',
            'name' => 'required|string|max:20',
            'review' => 'required|string|max:255',
            'score' => 'sometimes|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ));
        $data = [
            'title' => $request->title,
            'name' => $request->name,
            'email' => $request->email,
            'review' => $request->review,
            'score' => $request->score,
            'product_id' => $id,
        ];

        ProductReview::create($data);
        return back();
    }

    public function registerRide(Request $request, $id)
    {
        $this->validate($request, array(
            'fname' => 'required|string|max:50',
            'lname' => 'required|string|max:50',
            'email' => 'required|string|email|max:80',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'message' => 'required|string|max:191',
        ));

        $data = [
            'name' => $request->fname . ' ' . $request->lname,
            'email' => $request->email,
            'contact_no' => $request->contact_no,
            'message' => $request->message,
            'blog_id' => $id,
        ];

        BlogRide::create($data);
        Session::flash('flash_success', 'You have been sucessfully registered.');
        Session::flash('flash_type', 'alert-success');
        $details = [
            'body' => 'Awesome. Hope to meet you at the next ride.  Details will be emailed to you. Have a great day.',
            'subject'=>'Hope to meet you at the next ride'
        ];

        Mail::to($request['email'])->send(new \App\Mail\MailSender($details));
        return redirect()->back();
    }

    public function postContact(Request $request)
    {
        $this->validate($request, array(
            'fname' => 'required|string|max:50',
            'lname' => 'required|string|max:50',
            'email' => 'required|string|email|max:80',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'message' => 'required|string|max:191',
        ));

        $data = [
            'name' => $request->fname . ' ' . $request->lname,
            'email' => $request->email,
            'contact_no' => $request->contact_no,
            'message' => $request->message,

        ];
        Message::create($data);
        $user=User::where('type','Admin')->first();
        $user->notify(new NewMessage);
        $details = [
            'body' => 'Thank you for reaching out. One of our representatives will get back to you soon.',
            'subject'=>'Thank you for reaching out.'
        ];

        Mail::to($request['email'])->send(new \App\Mail\MailSender($details));
        return back();
    }

    public function replyMessage(Request $request,$id)
    {
        $this->validate($request, array(
            'subject' => 'required|string|max:80',
            'message' => 'required|string|max:191',
        ));
        $message=Message::where('id','=',$id)->first();
        $details = [
            'name' => $message->name,
            'body' => $request['message'],
            'subject'=>$request['subject']
        ];

        Mail::to($message->email)->send(new \App\Mail\MailSender($details));
        $message->status=1;
        $message->save();
        return $message;
    }

    public function getInstagramData(Request $request){
        $client = new Client();
        $res = $client->get('https://www.instagram.com/switchback.nepal/?__a=1');
        return $res;
    }
}
