<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Setting;
use App\StaticContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index()
    {
        return Setting::all();
    }

    public function update(Request $request)
    {
        $rules = [
            'key' => 'required|string|max:50',
            'value' => 'sometimes|max:65535',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $setting = Setting::where('key', '=', $request['key'])->first();
        if (is_null($setting)) {
            $newSetting = new Setting();
            $newSetting->key = $request['key'];
            $newSetting->value = $request['value'];
            $newSetting->save();
            return $newSetting;
        }else{
            $setting->value = $request['value'];
            $setting->save();
            return $setting;
        }
    }
}
