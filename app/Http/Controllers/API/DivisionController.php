<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Division;
use Illuminate\Support\Facades\Validator;
use App\ProductCategory;

class DivisionController extends Controller
{
    public function index()
    {
        return Division::get(['id', 'name', 'banner', 'highlight','info']);
    }

    public function getDivisionWithSubCategories()
    {

        $divisions = Division::with(['products:product_category_id'])->get(['id', 'name']);
        $subCategories = ProductCategory::get(['id', 'name']);

        $headers = [];

        foreach ($divisions as $division) {
            if (!empty($division['products'])) {
                foreach ($subCategories as $subcategory) {
                    foreach ($division['products'] as $product) {
                        if ($product['product_category_id'] == $subcategory['id']) {
                            array_push($headers, (object) ['name' => $subcategory['name'],'division'=>$division['name']]);
                        }
                    }
                }
            }
        }
        return $headers;
    }

    public function brands()
    {
        return Division::get(['id', 'name', 'banner', 'highlight','info']);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:20|unique:divisions',
            'banner' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'highlight' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'info' => 'sometimes|string|max:256',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $division = new Division();
        $division->name = $request['name'];
        $division->info = $request['info'];

        if ($request->banner) {
            $name = time() . '.' . $request->banner->getClientOriginalExtension();
            $request->banner->move(public_path('img/banners/'), $name);
            $division->banner = $name;
        }

        if ($request->highlight) {
            $name = time() . '.' . $request->highlight->getClientOriginalExtension();
            $request->highlight->move(public_path('img/highlights/'), $name);
            $division->highlight = $name;
        }
        $division->save();
        return $division;
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $division = Division::find($id);
        if (is_null($division)) {
            return response()->json(["error" => 'Division Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:20|unique:divisions,name,' . $division->id,
            'banner' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'highlight' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'info' => 'sometimes|string|max:256',
        ];
     
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $division->name = $request['name'];
        $division->info = $request['info'];

        $currentBanner = $division->banner;
        if ($request->banner) {
            $name = time() . '.' . $request->banner->getClientOriginalExtension();
            
            $request->banner->move(public_path('img/banners/'), $name);
            $division->banner = $name;

            $divisionBanner = public_path('img/banners/') . $currentBanner;
            if (file_exists($divisionBanner)) {
                @unlink($divisionBanner);
            }
        }
        $currentHighlight = $division->highlight;
        if ($request->highlight) {
            $name = time() . '.' . $request->highlight->getClientOriginalExtension();
            $request->highlight->move(public_path('img/highlights/'), $name);
            $division->highlight = $name;
            $divisionHighlight = public_path('img/highlights/') . $currentHighlight;
            if (file_exists($divisionHighlight)) {
                @unlink($divisionHighlight);
            }
        }

        $division->save();
        return $division;
    }

    public function destroy($id)
    {
        $division = Division::findOrFail($id);
        $divisionBanner = public_path('img/banners/') . $division->banner;
        if (file_exists($divisionBanner)) {
            @unlink($divisionBanner);
        }
        $divisionHighlight = public_path('img/highlights/') . $division->highlight;
        if (file_exists($divisionHighlight)) {
            @unlink($divisionHighlight);
        }
        $division->delete();
        return $division;
    }
}
