<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Subscriber;

class SubscriberController extends Controller
{
    public function index()
    {
        return Subscriber::all();
    }

    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|string|max:20',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        return Subscriber::create([
            'email' => $request['email']
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $subscriber = Subscriber::find($id);
        if (is_null($subscriber)) {
            return response()->json(["error" => 'Subscriber Not Found'], 404);
        }

        $rules = [
            'email' => 'required|string|max:20'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $subscriber->update(['email' => $request->email]);
        return $subscriber;
    }

    public function destroy($id)
    {
        $subscriber = Subscriber::findOrFail($id);
        $subscriber->delete();
        return $subscriber;
    }
}
