<?php

namespace App\Http\Controllers\API;

use App\Blog;
use App\BlogImage;
use App\BlogTag;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function index()
    {
        return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id','riders'])->get();
    }

    public function withpaginate()
    {
        return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id'])->paginate(3);
    }

    public function limited()
    {
        return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id'])->take(9)->get();
    }

    public function indivisualblog($id)
    {
        return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id','riders:id,name,blog_id'])->where('id', '=', $id)
            ->first();
    }

    public function tag()
    {
        return BlogTag::select('name')->distinct()->get();
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:150',
            'short_description' => 'required|string|max:255',
            'description' => 'sometimes|string|max:65535',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'tags.*' => 'sometimes|string|distinct|max:20',
            'video' => 'sometimes|string|max:191',
            'registration' => 'sometimes|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        try {
            DB::beginTransaction();
            $blog = new Blog;
            $blog->title = $request['title'];
            $blog->slug = Str::slug($request['title'], "-");
            $blog->description = $request['description'];
            $blog->short_description = $request['short_description'];
            $blog->registration =  $request['registration'] == "true" ? 1 : 0;
            $blog->video = $request['video'];
            $blog->save();

            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/blog/'), $name);

                    BlogImage::create([
                        'photo' => $name,
                        'blog_id' => $blog->id,
                    ]);
                }
            }

            if ($request->tags) {
                foreach ($request->tags as $key => $tag) {
                    BlogTag::create([
                        'name' => $tag,
                        'blog_id' => $blog->id,
                    ]);
                }
            }

            DB::commit();
            return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id','riders:id,name,blog_id'])->where('id', $blog->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function remove(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'id' => 'sometimes',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        if ($request->id) {
            $blogImage = BlogImage::find($request->id);
            $blogImage->delete();
        }

        $product = public_path('img/blog/') . $request->name;
        if (file_exists($product)) {
            @unlink($product);
        }

        return response()->json([
            'status' => 'removed',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $blog = Blog::find($id);
        if (is_null($blog)) {
            return response()->json(["error" => 'Blog Not Found'], 200);
        }

        $rules = [
            'title' => 'required|string|max:150',
            'short_description' => 'required|string|max:255',
            'description' => 'sometimes|string|max:65535',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'tags.*' => 'sometimes|string|distinct|max:20',
            'video' => 'sometimes|string|max:191',
            'registration' => 'sometimes|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }
        try {
            DB::beginTransaction();
            $blog->title = $request['title'];
            $blog->slug = Str::slug($request['title'], "-");
            $blog->description = $request['description'];
            $blog->short_description = $request['short_description'];
            $blog->registration =  $request['registration'] == "true" ? 1 : 0;
            $blog->video = $request['video'];
            $blog->save();
            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/blog/'), $name);

                    BlogImage::create([
                        'photo' => $name,
                        'blog_id' => $blog->id,
                    ]);
                }
            }
            if ($request->tags) {
                BlogTag::where('blog_id', $blog->id)->delete();
                foreach ($request->tags as $key => $tag) {
                    BlogTag::create([
                        'name' => $tag,
                        'blog_id' => $blog->id,
                    ]);
                }
            }

            DB::commit();
            return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id','riders:id,name,blog_id'])->where('id', $blog->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function changeregistration(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $blog = Blog::where('id', $id)->first();
        $blog->registration = $request['value'] == "true" ? 1 : 0;
        $blog->save();
        return Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id','riders:id,name,blog_id'])->where('id', $blog->id)->first();
    }

    public function destroy($id)
    {
        $blog = Blog::with(['photos:id,photo,blog_id', 'tags:id,name,blog_id'])->where('id', $id)->first();
        if (is_null($blog)) {
            return response()->json(["error" => 'Blog Not Found'], 404);
        }
        foreach ($blog->photos as $key => $photo) {
            $blogPhoto = public_path('img/blog/') . $photo->photo;
            if (file_exists($blogPhoto)) {
                @unlink($blogPhoto);
            }
        }
        $blog->delete();
        return $blog;
    }
}
