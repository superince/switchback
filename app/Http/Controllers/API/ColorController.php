<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Color;


class ColorController extends Controller
{
    public function index()
    {
        return Color::all();
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:20',
            'hex' => 'required|string|max:20',
            'group' => 'required|string|max:20',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }
        return Color::create([
            'name' => $request['name'],
            'hex' => $request['hex'],
            'group' => $request['group'],
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $color = Color::find($id);
        if (is_null($color)) {
            return response()->json(["error" => 'Color Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:20',
            'hex' => 'required|string|max:20',
            'group' => 'required|string|max:20'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $color->update(['name' => $request->name, 'hex' => $request->hex,'group'=>$request->group]);
        return $color;
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();
        return $color;
    }
}
