<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    public function index()
    {
        return About::where('id', '=', 1)
            ->get();
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');

        $rules = [
            'photo' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'description' => 'sometimes|string|max:65535'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $about = About::find($id);
        if (is_null($about)) {
            $about = new About();
            $about->description = $request['description'];
            $about->id = 1;
            if ($request->photo) {
                $name = time() . '.' . $request->photo->getClientOriginalExtension();
                $request->photo->move(public_path('img/about/'), $name);
                $about->photo = $name;
            }
            $about->save();
            return $about;
        } else {
            $about->description = $request['description'];
            $currentPhoto = $about->photo;
            if ($request->photo) {
                $name = time() . '.' . $request->photo->getClientOriginalExtension();
                $request->photo->move(public_path('img/about/'), $name);

                $aboutPhoto = public_path('img/about/') . $currentPhoto;
                if (file_exists($aboutPhoto)) {
                    @unlink($aboutPhoto);
                }
                $about->photo = $name;
            }
            $about->save();
            return $about;
        }
    }
}
