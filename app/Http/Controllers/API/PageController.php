<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function index()
    {
        return Page::all();
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:191|unique:pages',
            'content' => 'sometimes|string|max:65535',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $page = new Page();
        $page->title = $request['title'];
        $page->content = $request['content'];
        $page->save();
        return $page;
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        if (is_null($page)) {
            return response()->json(["error" => 'Page Not Found'], 404);
        }

        $rules = [
            'content' => 'sometimes|string|max:65535',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $page->content = $request['content'];
        $page->save();
        return $page;
    }

    public function destroy($id)
    {
        $page = Page::where('id', $id)->first();
        if (is_null($page)) {
            return response()->json(["error" => 'Page Not Found'], 404);
        }

        $page->delete();
        return $page;
    }
}
