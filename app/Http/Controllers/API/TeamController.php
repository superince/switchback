<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Team;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    public function index()
    {
        return Team::with(['segment:id,name'])->get();
        
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:50',
            'bio' => 'sometimes|string|max:65535',
            'segment_id' => 'required|integer',
            'fb_link' => 'sometimes|string|max:191|',
            'insta_link' => 'sometimes|string|max:191',
            'photo' => 'required|image|mimes:jpg,jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $team = new Team();
        $team->name = $request['name'];
        $team->bio = $request['bio'];
        $team->segment_id = $request['segment_id'];
        $team->fb_link = $request['fb_link'];
        $team->insta_link = $request['insta_link'];

        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/teams/'), $name);
            $team->photo = $name;
        }
        $team->save();
        return $team;
    }

    public function remove(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'id' => 'sometimes'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        if ($request->id) {
            $team = Team::find($request->id);
            $team->photo = null;
            $team->save();
        }

        $team = public_path('img/teams/') . $request->name;
        if (file_exists($team)) {
            @unlink($team);
        }

        return response()->json([
            'status' => 'removed',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $team = Team::find($id);

        if (is_null($team)) {
            return response()->json(["error" => 'Team Not Found'], 200);
        }

        $rules = [
            'name' => 'required|string|max:50',
            'bio' => 'sometimes|string|max:65535',
            'segment_id' => 'required|integer',
            'fb_link' => 'sometimes|string|max:191|',
            'insta_link' => 'sometimes|string|max:191',
            'photo' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }

        $team->name = $request['name'];
        $team->bio = $request['bio'];
        $team->segment_id = $request['segment_id'];
        $team->fb_link = $request['fb_link'];
        $team->insta_link = $request['insta_link'];

        $currentPhoto = $team->photo;
        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/teams/'), $name);
            $team->photo = $name;
            $teamPhoto = public_path('img/teams/') . $currentPhoto;
            if (file_exists($teamPhoto)) {
                @unlink($teamPhoto);
            }
        }
        $team->save();
        return $team;
    }

    public function destroy($id)
    {
        $team = Team::find($id);
        if (is_null($team)) {
            return response()->json(["error" => 'Team Not Found'], 404);
        }
        $teamPhoto = public_path('img/teams/') . $team->photo;
        if (file_exists($teamPhoto)) {
            @unlink($teamPhoto);
        }
        $team->delete();
        return $team;
    }
}
