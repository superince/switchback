<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
    public function index()
    {
        return ProductCategory::get(['id', 'name']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:20',
        ]);
        return ProductCategory::create([
            'name' => $request['name']
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');

        $productcategory = ProductCategory::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:20',
        ]);
        $productcategory->update($request->all());
        return $productcategory;
    }

    public function destroy($id)
    {
        $productcategory = ProductCategory::findOrFail($id);
        $productcategory->delete();
        return $productcategory;
    }
}
