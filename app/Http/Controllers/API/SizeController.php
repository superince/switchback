<?php

namespace App\Http\Controllers\API;

use App\GroupSize;
use App\Http\Controllers\Controller;
use App\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SizeController extends Controller
{
    public function index()
    {
        return GroupSize::with(['sizes:id,name,group_size_id'])->get();
    }

    public function store(Request $request)
    {
        $rules = [
            'group' => 'required|string|max:50',
            'sizechart' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'name.*' => 'required|string|distinct|max:20',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $group = new GroupSize();
        $group->group = $request['group'];
        if ($request->sizechart) {
            $name = time() . '.' . $request->sizechart->getClientOriginalExtension();
            $request->sizechart->move(public_path('img/sizecharts/'), $name);
            $group->sizechart = $name;
        }

        $group->save();
        if ($request->name) {
            foreach ($request->name as $key => $name) {
                Size::create([
                    'name' => $name,
                    'group_size_id' => $group->id,
                ]);
            }
        }

        return GroupSize::where('id', '=', $group->id)
            ->with(['sizes:id,name,group_size_id'])
            ->first();
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $group = GroupSize::find($id);
        if (is_null($group)) {
            return response()->json(["error" => 'Group Not Found'], 404);
        }

        $rules = [
            'group' => 'required|string|max:50',
            'name.*' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
  
        $group->group = $request['group'];
        $currentSizechart = $group->sizechart;
        if ($request->size_chart) {
            $name = time() . '.' . $request->size_chart->getClientOriginalExtension();
            $request->size_chart->move(public_path('img/sizecharts/'), $name);
            $group->size_chart = $name;
            $oldSizechart = public_path('img/sizecharts/') . $currentSizechart;
            if (file_exists($oldSizechart)) {
                @unlink($oldSizechart);
            }
        }
   
        if ($request->name) {
            foreach ($request->name as $key => $name) {
                $rawsize = json_decode($name);
                if($rawsize->id!=null){
                    $size=Size::find($rawsize->id);
                    $size->update(['name' => $rawsize->size, 'group_size_id' =>$group->id]);
                }else{
                    Size::create([
                        'name' => $rawsize->size,
                        'group_size_id' => $group->id,
                    ]);
                }
            }
        }
        $group->save();
        return GroupSize::with(['sizes:id,name,group_size_id'])->where('id', $group->id)->first();
    }

    public function destroy($id)
    {
        $group = GroupSize::findOrFail($id);
        $oldSizeChart = public_path('img/sizecharts/') . $group->sizechart;
        if (file_exists($oldSizeChart)) {
            @unlink($oldSizeChart);
        }
        $group->delete();
        return $group;
    }
}
