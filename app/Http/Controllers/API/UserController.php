<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:20',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:8',
            'type' => 'required|string|max:50|',
            'bio' => 'sometimes|string|max:191'
        ]);

        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'bio' => $request['bio'],
            'password' => Hash::make($request['password']),
        ]);
    }

    public function profile()
    {
        return auth('api')->user();
    }

    public function updateProfile(Request $request)
    {
        $user=auth('api')->user();
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:50|unique:users,email,' . $user->id,
            'address' => 'string|max:50',
            'designation' => 'max:100',
            'bio' => 'max:191'
        ]);

        $currentPhoto = $user->photo;
        if ($request->photo != $currentPhoto) {
            if (!file_exists(public_path('img/profile'))) {
                mkdir(public_path('img/profile'), 666, true);
            }
            $name = time() . '.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            Image::make($request->photo)->save(public_path('img/profile/').$name);
            $request->merge(['photo' => $name]);

            $userPhoto = public_path('img/profile/') . $currentPhoto;
            if (file_exists($userPhoto)) {
                @unlink($userPhoto);
            }
        }

        $user->update($request->all());
        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:20',
            'email' => 'required|string|email|max:50|unique:users,email,' . $user->id,
            'address' => 'sometimes|string|max:50|',
            'bio' => 'sometimes|string|max:191',
            'password' => 'required|string|min:8',
            'type' => 'required|string'
        ]);
        $request->merge(['password' => Hash::make($request['password'])]);
        $user->update($request->all());
        return $user;
    }

    public function destroy($id)
    {
        // $this->authorize('isAdmin');
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }
}
