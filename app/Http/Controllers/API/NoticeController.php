<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Notice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NoticeController extends Controller
{
    public function index()
    {
        return Notice::all();
    }


    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:191',
            'description' => 'sometimes|string|max:191',
            'link' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
            'photo' => 'required|image|mimes:jpg,jpeg,png|max:3000'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $notice = new Notice();
        $notice->title = $request['title'];
        $notice->description = $request['description'];
        $notice->link = $request['link'];
        $notice->status = $request['status'];
        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/notices/'), $name);
            $notice->photo = $name;
        }
        $notice->save();
        return $notice;
    }

    public function update(Request $request, $id)
    {
        $notice = Notice::find($id);
        if (is_null($notice)) {
            return response()->json(["error" => 'Notice Not Found'], 404);
        }

        $rules = [
            'title' => 'required|string|max:191',
            'description' => 'sometimes|string|max:191',
            'link' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
            'photo' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
       
        $notice->title = $request['title'];
        $notice->description = $request['description'];
        $notice->link = $request['link'];
        $notice->status = $request['status'];
        $currentBanner = $notice->photo;
        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/notices/'), $name);

            $noticePhoto = public_path('img/notices/') . $currentBanner;
            if (file_exists($noticePhoto)) {
                @unlink($noticePhoto);
            }
            $notice->photo = $name;
        }
        $notice->save();
        return $notice;
    }

    public function destroy($id)
    {
        $notice = Notice::findOrFail($id);
        $noticePhoto = public_path('img/notices/') . $notice->photo;
        if (file_exists($noticePhoto)) {
            @unlink($noticePhoto);
        }
        $notice->delete();
        return $notice;
    }

    public function changeStatus(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $notice = Notice::where('id', $id)->first();
        $notice->status = $request['value'] == "true" ? 1 : 0;
        $notice->save();
        return $notice;
    }
}
