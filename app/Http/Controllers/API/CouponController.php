<?php

namespace App\Http\Controllers\API;

use App\Coupon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{
    public function index()
    {
        return Coupon::all();
    }

    public function check($coupon)
    {
        $coupon = Coupon::where('code', '=', $coupon)->first();
        if ($coupon) {
            return $coupon;
        } else {
            return response()->json(['error' => 'Coupon Not found'], 200);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'code' => 'required|string|max:191',
            'description' => 'sometimes|string|max:65535',
            'amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'type' => 'required|string',
            'cart_limit' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'expiry' => 'required|date_format:Y-m-d',
            'limit' => 'sometimes|integer',
            'used' => 'sometimes|integer',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $coupon = new Coupon();
        $coupon->code = $request['code'];
        $coupon->description = $request['description'];
        $coupon->amount = $request['amount'];
        $coupon->cart_limit = $request['cart_limit'];
        $coupon->type = $request['type'];
        $coupon->expiry = $request['expiry'];
        $coupon->limit = $request['limit'];
        $coupon->used = $request['used'];

        $coupon->save();
        return $coupon;
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $coupon = Coupon::find($id);
        if (is_null($coupon)) {
            return response()->json(["error" => 'Coupon Not Found'], 404);
        }

        $rules = [
            'code' => 'required|string|max:191',
            'description' => 'sometimes|string|max:65535',
            'amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'type' => 'required|string',
            'cart_limit' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'expiry' => 'required|date_format:Y-m-d|after:today',
            'limit' => 'sometimes|integer',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $coupon->code = $request['code'];
        $coupon->description = $request['description'];
        $coupon->amount = $request['amount'];
        $coupon->type = $request['type'];
        $coupon->cart_limit = $request['cart_limit'];
        $coupon->expiry = $request['expiry'];
        $coupon->limit = $request['limit'];
        $coupon->save();
        return $coupon;
    }

    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        return $coupon;
    }
}
