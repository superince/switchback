<?php

namespace App\Http\Controllers\API;

use App\Banner;
use App\Division;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index()
    {
        return Banner::all();
    }

    public function bannerAndHighlights()
    {
        $banner = Banner::all();
        $divisions = Division::whereNotNull('banner')->limit(3)->get()->toArray();
        $highlight = array_map(function ($cat) {
            return ['banner' => $cat['banner'], 'name' => $cat['name']];
        }, $divisions);

        return [
            'banners' => $banner,
            'highlights' => $highlight,
        ];
    }

    public function changestatus(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $banner = Banner::where('id', $id)->first();
        $banner->status = $request['value'] == "true" ? 1 : 0;
        $banner->save();
        return $banner;
    }

    public function store(Request $request)
    {
        $rules = [
            'url' => 'required|string|max:191',
            'primary_tagline' => 'sometimes|string|max:191',
            'secondary_tagline' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
            'photo' => 'required|image|mimes:jpg,jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $banner = new Banner();
        $banner->url = $request['url'];
        $banner->primary_tagline = $request['primary_tagline'];
        $banner->secondary_tagline = $request['secondary_tagline'];
        $banner->status = $request['status'];
        $banner->type = 0;
        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/banners/'), $name);
            $banner->location = $name;
        }
        $banner->save();
        return $banner;
    }

    public function storevideo(Request $request)
    {
        $file = $request->file('video');
        $mime = $file->getMimeType();
        $rules = [
            'url' => 'required|string|max:191',
            'primary_tagline' => 'sometimes|string|max:191',
            'secondary_tagline' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
            'video' => 'required|mimes:mp4| max:50000',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $banner = new Banner();
        $banner->url = $request['url'];
        $banner->primary_tagline = $request['primary_tagline'];
        $banner->secondary_tagline = $request['secondary_tagline'];
        $banner->status = $request['status'];
        $banner->type = 1;

        if ($request->video) {
            $name = time() . '.' . $request->video->getClientOriginalExtension();
            $request->video->move(public_path('vid/'), $name);
            $banner->location = $name;
        }
        $banner->save();
        return $banner;
    }

    public function updatevideo(Request $request, $id)
    {
        $banner = Banner::find($id);
        if (is_null($banner)) {
            return response()->json(["error" => 'Video Banner Not Found'], 404);
        }

        $rules = [
            'url' => 'sometimes|string|max:191',
            'primary_tagline' => 'sometimes|string|max:191',
            'secondary_tagline' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
          
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $banner->url = $request['url'];
        $banner->primary_tagline = $request['primary_tagline'];
        $banner->secondary_tagline = $request['secondary_tagline'];
        $banner->status = $request['status'];
        $banner->save();
        return $banner;
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $banner = Banner::find($id);
        if (is_null($banner)) {
            return response()->json(["error" => 'Banner Not Found'], 404);
        }

        $rules = [
            'url' => 'sometimes|string|max:191',
            'primary_tagline' => 'sometimes|string|max:191',
            'secondary_tagline' => 'sometimes|string|max:191',
            'status' => 'sometimes|integer',
            'photo' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $banner->url = $request['url'];
        $banner->primary_tagline = $request['primary_tagline'];
        $banner->secondary_tagline = $request['secondary_tagline'];
        $banner->status = $request['status'];
        $currentBanner = $banner->location;
        if ($request->photo) {
            $name = time() . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('img/banners/'), $name);

            $bannerPhoto = public_path('img/banners/') . $currentBanner;
            if (file_exists($bannerPhoto)) {
                @unlink($bannerPhoto);
            }
            $banner->location = $name;
        }
        $banner->save();
        return $banner;
    }

    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $bannerPhoto = public_path('img/banners/') . $banner->location;
        if (file_exists($bannerPhoto)) {
            @unlink($bannerPhoto);
        }
        $banner->delete();
        return $banner;
    }

    public function destroyvideo($id)
    {
        $banner = Banner::findOrFail($id);
        $bannerVideo = public_path('vid/') . $banner->location;
        if (file_exists($bannerVideo)) {
            @unlink($bannerVideo);
        }
        $banner->delete();
        return $banner;
    }
}
