<?php

namespace App\Http\Controllers\API;

use App\Dealer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DealerController extends Controller
{
    public function index()
    {
        return Dealer::all();
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:191',
            'location' => 'required|string|max:191',
            'logo' => 'required|image|mimes:jpg,jpeg,png|max:3000',
            'phone' => 'required|string|max:15',
            'address' => 'required|string|max:20',
            'website' => 'required|string|max:50',
            'category' => 'required|string|max:10',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $dealer = new Dealer();
        $dealer->name = $request['name'];
        $dealer->location = $request['location'];
        $dealer->phone = $request['phone'];
        $dealer->address = $request['address'];
        $dealer->website = $request['website'];
        $dealer->category = $request['category'];
        if ($request->logo) {
            $name = time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('img/dealers/'), $name);
            $dealer->logo = $name;
        }
        $dealer->save();
        return $dealer;
    }

    public function update(Request $request, $id)
    {
        $dealer = Dealer::find($id);
        if (is_null($dealer)) {
            return response()->json(["error" => 'Dealer Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:191',
            'location' => 'required|string|max:191',
            'logo' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
       
        $dealer->name = $request['name'];
        $dealer->location = $request['location'];
        $dealer->phone = $request['phone'];
        $dealer->address = $request['address'];
        $dealer->website = $request['website'];
        $dealer->category = $request['category'];
        $currentLogo = $dealer->logo;
        if ($request->logo) {
            $name = time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('img/dealers/'), $name);

            $dealerLogo = public_path('img/dealers/') . $currentLogo;
            if (file_exists($dealerLogo)) {
                @unlink($dealerLogo);
            }
            $dealer->logo = $name;
        }
        $dealer->save();
        return $dealer;
    }

    public function destroy($id)
    {
        $dealer = Dealer::findOrFail($id);
        $dealerLogo = public_path('img/dealers/') . $dealer->logo;
        if (file_exists($dealerLogo)) {
            @unlink($dealerLogo);
        }
        $dealer->delete();
        return $dealer;
    }
}
