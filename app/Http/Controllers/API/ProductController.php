<?php

namespace App\Http\Controllers\API;

use App\Brand;
use App\Division;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use App\ProductGender;
use App\ProductImage;
use App\ProductTag;
use App\ProductReview;
use App\Size;
use App\Variant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name','variants', 'variants.photos', 'variants.sizes', 'variants.color'])->latest()->get();
    }

    public function search(Request $request)
    {
        $rules = [
            'query' => 'required|string|min:3',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $query = $request->input('query');
        $products = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->where('name', 'like', "%$query%")
            ->orWhereHas('brand', function ($q) use ($query) {
                $q->where('name', 'like', "%$query%");
            })
            ->orWhereHas('tags', function ($q) use ($query) {
                $q->where('name', 'like', "%$query%");
            })
            ->orWhereHas('productCategory', function ($q) use ($query) {
                $q->where('name', 'like', "%$query%");
            })
            ->orWhereHas('divisions', function ($q) use ($query) {
                $q->where('name', 'like', "%$query%");
            })
            ->paginate(6);

        return $products;
    }

    public function homepage()
    {
        $newarrival = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->orderBy('id', 'DESC')
            ->where('published', 1)
            ->take(4)
            ->get();

        $sale = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->where('sale', '=', '1')
            ->where('published', 1)
            ->take(4)
            ->get();

        $popular = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->orderBy('view', 'DESC')
            ->where('published', 1)
            ->take(4)
            ->get();

        $featured = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->where('featured', '=', '1')
            ->where('published', 1)
            ->take(4)
            ->get();

        $highlighted = Product::with(['photos:photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->where('is_highlighted', '=', '1')
            ->where('published', 1)
            ->take(2)
            ->get();

        return ['newarrival' => $newarrival, 'sale' => $sale, 'popular' => $popular, 'featured' => $featured, 'highlighted' => $highlighted];
    }

    public function indivisualproduct($id)
    {
        return Product::with(['photos:id,photo,product_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])
            ->where('id', '=', $id)
            ->first();
    }

    public function selectedproduct($name)
    {
        try {
            $division = Division::where('name', '=', $name)->firstOrFail();

            $raw = Division::with(['products:name,product_category_id', 'products.productCategory'])
                ->whereHas('products', function ($q) use ($division) {
                    $q->where('published', 1);
                })->where('name', '=', $name)->first();

            $semiRaw = [];
            foreach ($raw['products'] as $product) {
                $prod = json_decode($product);
                array_push($semiRaw, ["name" => $prod->product_category->name]);
            }

            $product_category = ProductCategory::All();
            $products = Product::with(['photos:photo,product_id,color_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
                ->whereHas('divisions', function ($q) use ($name) {
                    $q->where('name', '=', $name);
                })
                ->where('published', 1)
                ->paginate(6);
            return ['brand' => $division, 'product_category' => $semiRaw, 'products' => $products];
        } catch (Exception $e) {
            $brand = Brand::where('name', '=', $name)->firstOrFail();
            $pcat = ProductCategory::with(['products:product_category_id'])
                ->whereHas('products', function ($q) use ($brand) {
                    $q->where('brand_id', $brand->id);
                })
                ->whereHas('products', function ($q) {
                    $q->where('published', 1);
                })
                ->get();

            $products = Product::with(['photos:photo,product_id,color_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
                ->where('brand_id', $brand->id)
                ->where('published', 1)
                ->paginate(6);

            $product_category = [];
            foreach ($pcat as $cat) {
                array_push($product_category, ["name" => $cat['name']]);
            }
            return ['brand' => $brand, 'product_category' => $product_category, 'products' => $products];
        }
    }

    public function selectfilteredproduct($name, $filter)
    {
        try {
            $division = Division::where('name', '=', $name)->firstOrFail();

            $raw = Division::with(['products:name,product_category_id', 'products.productCategory'])
                ->whereHas('products', function ($q) use ($division) {
                    $q->where('published', 1);
                })->where('name', '=', $name)->first();

            $semiRaw = [];
            foreach ($raw['products'] as $product) {
                $prod = json_decode($product);
                array_push($semiRaw, ["name" => $prod->product_category->name]);
            }

            $product_category = ProductCategory::where('name', '=', $filter)->firstOrFail();
            $products = Product::with(['photos:photo,product_id,color_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
                ->whereHas('divisions', function ($q) use ($name) {
                    $q->where('name', '=', $name);
                })
                ->where('published', 1)
                ->where('product_category_id', '=', $product_category->id)
                ->paginate(6);
            return ['brand' => $division, 'product_category' => $semiRaw, 'products' => $products];
        } catch (Exception $e) {
            $brand = Brand::where('name', '=', $name)->firstOrFail();
            $pcat = ProductCategory::with(['products:product_category_id'])
                ->whereHas('products', function ($q) use ($brand) {
                    $q->where('brand_id', $brand->id);
                })
                ->whereHas('products', function ($q) {
                    $q->where('published', 1);
                })
                ->get();
            $product_category = ProductCategory::where('name', '=', $filter)->firstOrFail();
            $products = Product::with(['photos:photo,product_id,color_id', 'tags:name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'productCategory:id,name', 'divisions:name'])
                ->where('brand_id', $brand->id)
                ->where('published', 1)
                ->where('product_category_id', '=', $product_category->id)
                ->paginate(6);

            $product_category = [];
            foreach ($pcat as $cat) {
                array_push($product_category, ["name" => $cat['name']]);
            }
            return ['brand' => $brand, 'product_category' => $product_category, 'products' => $products];
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:100',
            'short_description' => 'required|string|max:1000',
            'features' => 'sometimes|string|max:65535',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale_price' => 'sometimes|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale' => 'sometimes|string',
            'featured' => 'sometimes|string',
            'is_highlighted' => 'sometimes|string',
            'published' => 'sometimes|string',
            'brand_id' => 'required|integer',
            'color' => 'required|integer',
            'product_category_id' => 'required|integer',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'tag.*' => 'sometimes|string|distinct|max:20',
            'size' => 'required|string',
            'gender.*' => 'sometimes|string|distinct|max:20',
            'division.*' => 'sometimes|integer',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        try {
            DB::beginTransaction();
            $product = new Product;
            $product->name = $request['name'];
            $product->slug = Str::slug($request['name'], "-");
            $product->short_description = $request['short_description'];
            $product->features = $request['features'];
            $product->price = $request['price'];
            $product->color_id = $request['color'];
            $product->sale_price = $request['sale_price'];
            $product->sale = $request['sale'] == "true" ? 1 : 0;
            $product->featured = $request['featured'] == "true" ? 1 : 0;
            $product->is_highlighted = $request['is_highlighted'] == "true" ? 1 : 0;
            $product->published = $request['published'] == "true" ? 1 : 0;
            $product->brand_id = $request['brand_id'];
            $product->product_category_id = $request['product_category_id'];

            $product->save();
            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/products/'), $name);

                    ProductImage::create([
                        'photo' => $name,
                        'product_id' => $product->id,
                    ]);
                }
            }

            if ($request->size) {
                $rawSizes = json_decode($request->size);
                foreach ($rawSizes as $key => $s) {
                    $dbsize = Size::find($s->value);
                    $product->sizes()->attach(
                        $dbsize, [
                            'stock' => $s->quantity,
                            'sku'=>$s->sku
                        ]
                    );
                }
            }

            if ($request->gender) {
                foreach ($request->gender as $key => $gender) {
                    ProductGender::create([
                        'name' => $gender,
                        'product_id' => $product->id,
                    ]);
                }
            }

            if ($request->division) {
                $divisions = Division::find($request->division);
                $product->divisions()->attach($divisions);
            }

            if ($request->tag) {
                foreach ($request->tag as $key => $tag) {
                    ProductTag::create([
                        'name' => $tag,
                        'product_id' => $product->id,
                    ]);
                }
            }

            DB::commit();
            return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name','variants', 'variants.photos', 'variants.sizes', 'variants.color'])->where('id', $product->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function remove(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'id' => 'sometimes',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        if ($request->id) {
            $productImage = ProductImage::find($request->id);
            $productImage->delete();
        }

        $product = public_path('img/products/') . $request->name;
        if (file_exists($product)) {
            @unlink($product);
        }

        return response()->json([
            'status' => 'removed',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            return response()->json(["error" => 'Product Not Found'], 200);
        }

        $rules = [
            'name' => 'required|string|max:100',
            'short_description' => 'required|string|max:1000',
            'features' => 'sometimes|string|max:65535',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale_price' => 'sometimes|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale' => 'sometimes|string',
            'featured' => 'sometimes|string',
            'is_highlighted' => 'sometimes|string',
            'published' => 'sometimes|string',
            'brand_id' => 'required|integer',
            'color' => 'required|integer',
            'product_category_id' => 'required|integer',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'tag.*' => 'sometimes|string|distinct|max:20',
            'size' => 'required|string',
            'division.*' => 'sometimes|integer',
            'gender.*' => 'sometimes|string|distinct|max:20',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }
        try {
            DB::beginTransaction();
            $product->name = $request['name'];
            $product->slug = Str::slug($request['name'], "-");
            $product->short_description = $request['short_description'];
            $product->features = $request['features'];
            $product->price = $request['price'];
            $product->sale_price = $request['sale_price'];
            $product->color_id = $request['color'];
            $product->sale = $request['sale'] == "true" ? 1 : 0;
            $product->featured = $request['featured'] == "true" ? 1 : 0;
            $product->published = $request['published'] == "true" ? 1 : 0;
            $product->is_highlighted = $request['is_highlighted'] == "true" ? 1 : 0;
            $product->brand_id = $request['brand_id'];
            $product->product_category_id = $request['product_category_id'];
            $product->save();

            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/products/'), $name);

                    ProductImage::create([
                        'photo' => $name,
                        'product_id' => $product->id,
                    ]);
                }
            }

            
            if ($request->size) {
                $product->sizes()->detach();
                $rawSizes = json_decode($request->size);
                foreach ($rawSizes as $key => $s) {
                    $dbsize = Size::find($s->value);
                    $product->sizes()->attach(
                        $dbsize, [
                            'stock' => $s->quantity,
                            'sku'=>$s->sku
                        ]
                    );
                }
            }

            if ($request->gender) {
                ProductGender::where("product_id", "=", $id)->delete();
                foreach ($request->gender as $key => $gender) {
                    ProductGender::create([
                        'name' => $gender,
                        'product_id' => $product->id,
                    ]);
                }
            }

            if ($request->division) {
                $product->divisions()->detach();
                $divisions = Division::find($request->division);
                $product->divisions()->attach($divisions);
            }

            if ($request->tag) {
                ProductTag::where("product_id", "=", $id)->delete();
                foreach ($request->tag as $key => $tag) {
                    ProductTag::create([
                        'name' => $tag,
                        'product_id' => $product->id,
                    ]);
                }
            }
            DB::commit();
            return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name','variants', 'variants.photos', 'variants.sizes', 'variants.color'])->where('id', $product->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function destroy($id)
    {
        // $this->authorize('isAdmin');
        $product = Product::with(['photos:id,photo,product_id'])->where('id', $id)->first();
        if (is_null($product)) {
            return response()->json(["error" => 'Product Not Found'], 404);
        }
        foreach ($product->photos as $key => $photo) {
            $productPhoto = public_path('img/products/') . $photo->photo;
            if (file_exists($productPhoto)) {
                @unlink($productPhoto);
            }
        }
        $product->delete();
        return $product;
    }

    public function changeParallex(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->is_highlighted = $request['value'] == "true" ? 1 : 0;
        $product->save();
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])->where('id', $product->id)->first();
    }

    public function changePublished(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->published = $request['value'] == "true" ? 1 : 0;
        $product->save();
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])->where('id', $product->id)->first();
    }

    public function changeFeatured(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->featured = $request['value'] == "true" ? 1 : 0;
        $product->save();
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])->where('id', $product->id)->first();
    }

    public function changePrice(Request $request,$id){
        $rules = [
            'value' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->price = $request['value'];
        $product->save();
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name'])->where('id', $product->id)->first();
    }

    public function changeSalePercentage(Request $request,$id){
        $rules = [
            'value' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->sale_price = $request['value'];
        $product->save();
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name','variants', 'variants.photos', 'variants.sizes', 'variants.color'])->where('id', $product->id)->first();
    }

    public function changeSale(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $product = Product::where('id', $id)->first();
        $product->sale = $request['value'] == "true" ? 1 : 0;
        $product->save();
        if($request['value']=="true"){
            $variants=Variant::where('product_id','=',$id)->get();
            foreach($variants as $variant){
                $variant->sale=1;
                $variant->save();
            }
        }else{
            $variants=Variant::where('product_id','=',$id)->get();
            foreach($variants as $variant){
                $variant->sale=0;
                $variant->save();
            }
        }
       
        return Product::with(['photos:id,photo,product_id', 'tags:id,name,product_id', 'sizes:name,group_size_id', 'genders:id,name,product_id', 'color:id,name,hex', 'brand:id,name,banner', 'divisions:name', 'productCategory:id,name','variants', 'variants.photos', 'variants.sizes', 'variants.color'])->where('id', $product->id)->first();
    }

    public function getReviews(Request $request){
        $reviews=ProductReview::with(['product','product.photos'])->get();
        return $reviews;
    }

    public function destroyReview(Request $request,$id){
        $review = ProductReview::where('id', $id)->first();
        $review->delete();
        return $review;
    }
}
