<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function index()
    {
        return Brand::get(['id', 'name', 'banner', 'highlight']);
    }

    public function brands()
    {
        return Brand::get(['id', 'name', 'banner', 'highlight']);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:20|unique:brands',
            'banner' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'highlight' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'info' => 'sometimes|string|max:256',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        $brand = new Brand();
        $brand->name = $request['name'];
        $brand->info = $request['info'];

        if ($request->banner) {
            $name = time() . '.' . $request->banner->getClientOriginalExtension();
            $request->banner->move(public_path('img/banners/'), $name);
            $brand->banner = $name;
        }

        if ($request->highlight) {
            $name = time() . '.' . $request->highlight->getClientOriginalExtension();
            $request->highlight->move(public_path('img/highlights/'), $name);
            $brand->highlight = $name;
        }
        $brand->save();
        return $brand;
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $brand = Brand::find($id);
        if (is_null($brand)) {
            return response()->json(["error" => 'Brand Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:20|unique:brands,name,' . $brand->id,
            'banner' => 'sometimes|image|mimes:jpg,jpeg,png|max:3000',
            'info' => 'sometimes|string|max:256',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $brand->name = $request['name'];
        $brand->info = $request['info'];

        $currentBanner = $brand->banner;
        if ($request->banner) {
            $name = time() . '.' . $request->banner->getClientOriginalExtension();
            $request->banner->move(public_path('img/banners/'), $name);
            $brand->banner = $name;
            $brandBanner = public_path('img/banners/') . $currentBanner;
            if (file_exists($brandBanner)) {
                @unlink($brandBanner);
            }
        }
        $currentHighlight = $brand->highlight;
        if ($request->highlight) {
            $name = time() . '.' . $request->highlight->getClientOriginalExtension();
            $request->highlight->move(public_path('img/highlights/'), $name);
            $brand->highlight = $name;
            $brandHighlight = public_path('img/highlights/') . $currentHighlight;
            if (file_exists($brandHighlight)) {
                @unlink($brandHighlight);
            }
        }

        $brand->save();
        return $brand;
    }

    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $brandBanner = public_path('img/banners/') . $brand->banner;
        if (file_exists($brandBanner)) {
            @unlink($brandBanner);
        }
        $brandHighlight = public_path('img/highlights/') . $brand->highlight;
        if (file_exists($brandHighlight)) {
            @unlink($brandHighlight);
        }
        $brand->delete();
        return $brand;
    }
}
