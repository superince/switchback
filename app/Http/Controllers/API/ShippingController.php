<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Shipping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShippingController extends Controller
{
    public function index()
    {
        return Shipping::get(['id', 'address','amount','status']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'address' => 'required|string|max:20',
            'amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'status' => 'sometimes|string',
        ]);
        return Shipping::create([
            'address' => $request['address'],
            'amount' => $request['amount'],
            'status' => $request['status'] == "true" ? 1 : 0,
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');

        $shipping = Shipping::find($id);
        if (is_null($shipping)) {
            return response()->json(["error" => 'Shipping Not Found'], 200);
        }

        $rules = [
            'address' => 'required|string|max:20',
            'amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'status' => 'sometimes|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }
        
        $shipping->address=$request['address'];
        $shipping->amount=$request['amount'];
        $shipping->status=$request['status']== "true" ? 1 : 0;
        $shipping->save();
        return $shipping;
    }

    public function destroy($id)
    {
        $shipping = Shipping::findOrFail($id);
        $shipping->delete();
        return $shipping;
    }

    public function changeStatus(Request $request,$id)
    {
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $shipping = Shipping::where('id', $id)->first();
        $shipping->status = $request['value'] == "true" ? 1 : 0;
        $shipping->save();
        return $shipping;
    }
}
