<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\CustomerOrder;
use App\OrderProduct;

class OrderController extends Controller
{
    public function index()
    {
        return CustomerOrder::with(['orderProducts'])->get();
    }

    public function store(Request $request)
    {
        $rules = [
            'firstname' => 'required|string|max:50',
            'lastname' => 'required|string|max:50',
            'country' => 'required|string|max:50',
            'state' => 'required|string|max:50',
            'address' => 'required|string|max:50',
            'zip_code' => 'sometimes|string|max:50',
            'email' => 'required|string|email|max:50',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'secondary_contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'payment_method' => 'required|string|max:50',
            'total' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }
        $latestOrder = CustomerOrder::orderBy('created_at', 'DESC')->first();

        $customerOrder = new CustomerOrder();
        $customerOrder->order_no = '#' . str_pad(($latestOrder != null ? $latestOrder->id : 0) + 1, 8, "0", STR_PAD_LEFT);
        $customerOrder->firstname = $request['firstname'];
        $customerOrder->lastname = $request['lastname'];
        $customerOrder->country = $request['country'];
        $customerOrder->state = $request['state'];
        $customerOrder->address = $request['address'];
        $customerOrder->zip_code = $request['zip_code'];
        $customerOrder->email = $request['email'];
        $customerOrder->phone = $request['contact_no'];
        $customerOrder->secondary_phone = $request['secondary_contact_no'];
        $customerOrder->payment_method = $request['payment_method'];
        $customerOrder->total = $request['total'];
        $customerOrder->save();

        foreach ($request->products as $key => $rawproduct) {
            $product = json_decode($rawproduct, true);
            $orderProduct = new OrderProduct();
            $orderProduct->product_id = $product["product_id"];
            $orderProduct->quantity = $product["amount"];
            $orderProduct->price = $product["price"];
            $orderProduct->size = $product["size"];
            $orderProduct->color = $product["color"];
            $orderProduct->name = $product["name"];
            $orderProduct->photo = $product["photo"];
            $orderProduct->customer_order_id = $customerOrder->id;
            $orderProduct->save();
        }
        return response()->json(['success' => "Your Order has been placed."], 200);
    }

    public function changeStatus(Request $request,$id){
        $rules = [
            'status' => 'required|string|max:20',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $order = CustomerOrder::where('id', $id)->first();
        $order->update(['status' => $request->status]);
        $order->update(['email_sent' => 0]);
        return CustomerOrder::with(['orderProducts'])->where('id', $id)->first();
    }

    public function archiveOrder(Request $request,$id){
        $rules = [
            'archived' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $order = CustomerOrder::where('id', $id)->first();
        $order->update(['archived' => $request->archived]);
        return CustomerOrder::with(['orderProducts'])->where('id', $id)->first();
    }

    public function destroy($id)
    {
        $order = CustomerOrder::where('id', $id)->first();
        if (is_null($order)) {
            return response()->json(["error" => 'Order Not Found'], 404);
        }

        $order->delete();
        return $order;
    }
}
