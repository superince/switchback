<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Size;
use App\Variant;
use App\VariantImage;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VariantController extends Controller
{
    public function index($id)
    {
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('product_id', '=', $id)->get();
    }

    public function indivisualvariant($id)
    {
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', '=', $id)->first();
    }

    public function remove(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'id' => 'sometimes',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        if ($request->id) {
            $variantImage = VariantImage::find($request->id);
            $variantImage->delete();
        }

        $variant = public_path('img/products/') . $request->name;
        if (file_exists($variant)) {
            @unlink($variant);
        }

        return response()->json([
            'status' => 'removed',
        ], 200);
    }

    public function store(Request $request, $id)
    {
        $rules = [
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale_price' => 'sometimes|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'is_highlighted' => 'sometimes|string',
            'published' => 'sometimes|string',
            'color' => 'required|integer',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'size' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        try {
            DB::beginTransaction();
            $variant = new Variant();
            $variant->price = $request['price'];
            $variant->color_id = $request['color'];
            $variant->sale_price = $request['sale_price'];
            $variant->is_highlighted = $request['is_highlighted'] == "true" ? 1 : 0;
            $variant->published = $request['published'] == "true" ? 1 : 0;
            $variant->product_id = $id;

            $variant->save();
            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/products/'), $name);

                    VariantImage::create([
                        'photo' => $name,
                        'variant_id' => $variant->id,
                    ]);
                }
            }

            if ($request->size) {
                $rawSizes = json_decode($request->size);
                foreach ($rawSizes as $key => $s) {
                    $dbsize = Size::find($s->value);
                    $variant->sizes()->attach(
                        $dbsize, [
                            'stock' => $s->quantity,
                            'sku' => $s->sku,
                        ]
                    );
                }
            }

            DB::commit();
            return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function update(Request $request, $id)
    {
        $variant = Variant::find($id);

        if (is_null($variant)) {
            return response()->json(["error" => 'Variant Not Found'], 200);
        }

        $rules = [
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'sale_price' => 'sometimes|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'is_highlighted' => 'sometimes|string',
            'published' => 'sometimes|string',
            'color' => 'required|integer',
            'photos.*' => 'sometimes|image|mimes:jpeg,png,jpg|max:3000',
            'size' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }
        try {
            DB::beginTransaction();

            $variant->price = $request['price'];
            $variant->color_id = $request['color'];
            $variant->sale_price = $request['sale_price'];
            $variant->is_highlighted = $request['is_highlighted'] == "true" ? 1 : 0;
            $variant->published = $request['published'] == "true" ? 1 : 0;
            $variant->save();

            if ($request->photos) {
                foreach ($request->photos as $key => $image) {
                    $name = time() . $key . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('img/products/'), $name);

                    VariantImage::create([
                        'photo' => $name,
                        'variant_id' => $variant->id,
                    ]);
                }
            }
    
            if ($request->size) {
                $variant->sizes()->detach();
                $rawSizes = json_decode($request->size);
                foreach ($rawSizes as $key => $s) {
                    $dbsize = Size::find($s->value);
                    if(isset($s->sku)){
                        $variant->sizes()->attach(
                            $dbsize, [
                                'stock' => $s->quantity,
                                'sku' => $s->sku,
                            ]
                        );
                    }else{
                        $variant->sizes()->attach(
                            $dbsize, [
                                'stock' => $s->quantity,
                                'sku' => '',
                            ]
                        );
                    }
                  
                }
            }

            DB::commit();
            return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
        } catch (QueryException $e) {
            DB::rollback();
            return ["msg" => $e];
        }
    }

    public function destroy($id)
    {
        $variant = Variant::with(['photos:id,photo,variant_id'])->where('id', $id)->first();
        if (is_null($variant)) {
            return response()->json(["error" => 'Variant Not Found'], 404);
        }

        foreach ($variant->photos as $key => $photo) {
            $variantPhoto = public_path('img/products/') . $photo->photo;
            if (file_exists($variantPhoto)) {
                @unlink($variantPhoto);
            }
        }
        $variant->delete();
        return $variant;
    }

    public function changeParallex(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $variant = Variant::where('id', $id)->first();
        $variant->is_highlighted = $request['value'] == "true" ? 1 : 0;
        $variant->save();
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
    }

    public function changePrice(Request $request,$id){
        $rules = [
            'value' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $variant = Variant::where('id', $id)->first();
        $variant->price = $request['value'];
        $variant->save();
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
    }

    public function changeSalePercentage(Request $request,$id){
        $rules = [
            'value' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $variant = Variant::where('id', $id)->first();
        $variant->sale_price = $request['value'];
        $variant->save();
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
    }

    public function changeSale(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $variant = Variant::where('id', $id)->first();
        $variant->sale = $request['value'] == "true" ? 1 : 0;
        $variant->save();
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
    }

    public function changePublished(Request $request,$id){
        $rules = [
            'value' => 'required|string|max:6',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $variant = Variant::where('id', $id)->first();
        $variant->published = $request['value'] == "true" ? 1 : 0;
        $variant->save();
        return Variant::with(['photos:id,photo,variant_id', 'sizes:name,group_size_id', 'color:id,name,hex'])->where('id', $variant->id)->first();
    }
}
