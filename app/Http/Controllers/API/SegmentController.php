<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Segment;


class SegmentController extends Controller
{
    public function index()
    {
        return Segment::all();
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:50'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        return Segment::create([
            'name' => $request['name'],
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $segment = Segment::find($id);
        if (is_null($segment)) {
            return response()->json(["error" => 'Segment Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:50'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $segment->update(['name' => $request->name]);
        return $segment;
    }

    public function destroy($id)
    {
        $segment = Segment::findOrFail($id);
        $segment->delete();
        return $segment;
    }
}
