<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        return Message::all();
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:50',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'message' => 'required|string|max:191',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }

        return Message::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'contact_no' => $request['contact_no'],
            'message' => $request['message']
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('isAdmin');
        $message = Message::find($id);
        if (is_null($message)) {
            return response()->json(["error" => 'Message Not Found'], 404);
        }

        $rules = [
            'name' => 'required|string|max:20',
            'email' => 'required|string|max:50',
            'contact_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:5|max:10',
            'message' => 'required|string|max:191',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $message->update(['email' => $request->email,'name'=>$request->name,'contact_no'=>$request->contact_no,'message'=>$request->message]);
        return $message;
    }

    public function destroy($id)
    {
        $message = Message::findOrFail($id);
        $message->delete();
        return $message;
    }
}
