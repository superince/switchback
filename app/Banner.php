<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'location','url','primary_tagline','secondary_tagline','status','type'
    ];
}
