<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogRide extends Model
{
    protected $fillable = [
        'name','email','contact_no','message','blog_id'
    ];
}
