<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name', 'bio', 'segment_id', 'fb_link', 'insta_link', 'photo'
    ];

    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }
}
