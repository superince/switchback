<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupSize extends Model
{
    protected $fillable = [
        'group'
    ];

    public function sizes()
    {
        return $this->hasMany('App\Size');
    }
}
