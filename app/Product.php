<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'size', 'highlights', 'description', 'code',
    ];

    protected $hidden = ['updated_at'];

    public function photos()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function tags()
    {
        return $this->hasMany('App\ProductTag');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Size')->withPivot(['stock','sku']);
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }

    public function divisions()
    {
        return $this->belongsToMany('App\Division');
    }

    public function genders()
    {
        return $this->hasMany('App\ProductGender');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function productCategory()
    {
        return $this->belongsTo('App\ProductCategory');
    }

    public function variants()
    {
        return $this->hasMany('App\Variant');
    }

    public function reviews()
    {
        return $this->hasMany('App\ProductReview');
    }

}
