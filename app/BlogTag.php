<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    protected $fillable = [
        'name','blog_id'
    ];

    public function blog(){
        return $this->belongsTo('App\Blog');
    }
}
