<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
        'id', 'name', 'hex','group'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
