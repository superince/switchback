<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariantImage extends Model
{
    protected $fillable = [
        'photo', 'variant_id',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
