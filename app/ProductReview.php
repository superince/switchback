<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $fillable = [
        'title','name','product_id','email','score','review'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
