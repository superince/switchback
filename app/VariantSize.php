<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariantSize extends Model
{
    protected $table = 'variant_size';
    protected $fillable = [
        'name','variant_id'
    ];

    public function product(){
        return $this->belongsTo('App\Variant');
    }
}
