<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title','description','short_description','date','tags','video'
    ];

    public function photos(){
        return $this->hasMany('App\BlogImage');
    }

    public function tags(){
        return $this->hasMany('App\BlogTag');
    }

    public function riders(){
        return $this->hasMany('App\BlogRide');
    }
}
