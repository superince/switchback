<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = [
        'name', 'group_size_id'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function variants()
    {
        return $this->belongsToMany('App\Variant');
    }

    public function group(){
        return $this->belongsTo('App\GroupSize','group_size_id');
    }
}
