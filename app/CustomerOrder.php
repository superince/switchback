<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
	protected $guarded = [];
	
    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct');
    }
}
