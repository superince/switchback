import VueRouter from "vue-router";
import Vue from "vue";
Vue.use(VueRouter);

const routes = [
    {
        path: "/admin",
        component: require("./components/Admin.vue").default,
        children: [
            {
                path: "",
                name: "Dashboard",
                component: require("./components/Dashboard.vue").default
            },
            {
                path: "notice",
                component: require("./components/Notice.vue").default
            },
            {
                path: "banner",
                component: require("./components/Banner.vue").default
            },
            {
                path: "division",
                component: require("./components/Division.vue").default
            },
            {
                path: "brands",
                component: require("./components/Brand.vue").default
            },
            {
                path: "color",
                component: require("./components/Color.vue").default
            },
            {
                path: "product-category",
                component: require("./components/Product-Category.vue").default
            },
            {
                path: "size",
                component: require("./components/Size.vue").default
            },
            {
                path: "coupon",
                component: require("./components/Coupon.vue").default
            },
            {
                path: "messages",
                component: require("./components/Message.vue").default
            },
            {
                path: "orders",
                component: require("./components/Order.vue").default
            },
            {
                path: "team",
                component: require("./components/Team.vue").default
            },
            {
                path: "users",
                component: require("./components/User.vue").default
            },
            {
                path: "subscribers",
                component: require("./components/Subscriber.vue").default
            },
            {
                path: "blogs",
                component: require("./components/Blog.vue").default
            },
            {
                path: "blogs/add",
                component: require("./components/partials/AddBlog.vue").default
            },
            {
                path: "blogs/:id",
                component: require("./components/partials/EditBlog.vue").default
            },
            {
                path: "products",
                component: require("./components/Product.vue").default
            },
            {
                path: "products/add",
                component: require("./components/partials/AddProduct.vue")
                    .default
            },
            {
                path: "products/:id",
                component: require("./components/partials/EditProduct.vue")
                    .default
            },
            {
                path: "products/:id/variants",
                component: require("./components/Variant.vue").default
            },
            {
                path: "products/:id/variants/add",
                component: require("./components/partials/AddVariant.vue")
                    .default
            },
            {
                path: "products/:id/variants/:vid",
                component: require("./components/partials/EditVariant.vue")
                    .default
            },
            {
                path: "about",
                component: require("./components/About.vue").default
            },
            {
                path: "developer",
                component: require("./components/Developer.vue").default
            },
            {
                path: "reviews",
                component: require("./components/Review.vue").default
            },
            {
                path: "settings",
                component: require("./components/Settings.vue").default
            },
            {
                path: "shipping",
                component: require("./components/Shipping.vue").default
            },
            {
                path: "dealer",
                component: require("./components/Dealer.vue").default
            },
            {
                path: "pages",
                component: require("./components/Page.vue").default
            }
        ]
    }
];

const router = new VueRouter({
    mode: "history",
    routes
});

export default router;
