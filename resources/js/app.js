/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import { Modal, Icon, Upload } from "ant-design-vue";
import Vuetify from "vuetify";

import "vuetify/dist/vuetify.min.css";
import "ant-design-vue/dist/antd.css";
import JsonExcel  from 'vue-json-excel';
import VueHtmlToPaper from 'vue-html-to-paper';

Vue.component(Upload.name, Upload);
Vue.component(Modal.name, Modal);
Vue.component(Icon.name, Icon);
Vue.component("downloadExcel", JsonExcel);
Vue.use(Upload);
Vue.use(Modal);
Vue.use(Icon);
Vue.use(VueHtmlToPaper, {
    name: '_blank',
    specs: [
      'fullscreen=yes',
      'titlebar=yes',
      'scrollbars=yes'
    ],
    styles: [
      'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
      'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
  
});
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component(
    "passport-clients",
    require("./components/passport/Clients.vue").default
);

Vue.component(
    "passport-authorized-clients",
    require("./components/passport/AuthorizedClients.vue").default
);

Vue.component(
    "passport-personal-access-tokens",
    require("./components/passport/PersonalAccessTokens.vue").default
);


Vue.use(Vuetify);
import router from "./router";
import store from "./store/index";

const app = new Vue({
    el: "#app",
    router,
    store,
    vuetify: new Vuetify()
});
