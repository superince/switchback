import Vuex from "vuex";
import Vue from "vue";
import notice from "./notice";
import banner from "./banner";
import division from "./division";
import brand from "./brand";
import color from "./color";
import productcategory from "./productcategory";
import size from "./size";
import coupon from "./coupon";
import order from "./order";
import team from "./team";
import segment from "./segment";
import user from "./user";
import subscriber from "./subscriber";
import message from "./message";
import blog from "./blog";
import product from "./product";
import about from "./about";
import auth from "./auth";
import setting from "./setting";
import dealer from "./dealer";
import page from "./page";
import review from "./review";
import shipping from "./shipping";

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
        notice,
        banner,
        division,
        brand,
        color,
        productcategory,
        size,
        coupon,
        order,
        team,
        segment,
        user,
        subscriber,
        message,
        blog,
        product,
        about,
        auth,
        setting,
        dealer,
        page,
        review,
        shipping
    }
});
