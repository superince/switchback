import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    settings: []
});

// getters
const getters = {
    settings: state => state.settings
};

// mutations
const mutations = {
    FETCH_SETTINGS_SUCCESS(state, settings) {
        state.settings = _.sortBy(settings, ["id"]);
    },

    EDIT_SETTING_SUCCESS(state, updatedSetting) {
        const index = state.settings.findIndex(
            setting => setting.id === updatedSetting.id
        );
        if (index !== -1) {
            state.settings.splice(index, 1, updatedSetting);
        }
    }
};

// actions
const actions = {
    async fetchSettings({ commit }) {
        try {
            const { data } = await axios.get("/api/settings");
            commit("FETCH_SETTINGS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async modifySetting({ commit }, rawData) {
        let formData = new FormData();
        formData.append("key", rawData.key);
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/settings`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
