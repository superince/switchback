import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
  brands: []
});

// getters
const getters = {
  brands: state => state.brands
};

// mutations
const mutations = {
  FETCH_BRANDS_SUCCESS(state, brands) {
    state.brands = _.sortBy(brands, ["id"]);
  },

  ADD_BRAND_SUCCESS(state, brand) {
    state.brands.push(brand);
  },

  EDIT_BRAND_SUCCESS(state, updatedBrand) {
    const index = state.brands.findIndex(brand => brand.id === updatedBrand.id);
    if (index !== -1) {
      state.brands.splice(index, 1, updatedBrand);
    }
  },

  DELETE_BRAND_SUCCESS(state, brand) {
    state.brands = state.brands.filter(t => t.id !== brand.id);
  }
};

// actions
const actions = {
  async fetchBrands({ commit }) {
    try {
      const { data } = await axios.get("/api/brands");
      commit("FETCH_BRANDS_SUCCESS", data);
    } catch (e) {
      throw e;
    }
  },
  async createBrand({ commit }, rawData) {
    let formData = new FormData();
    formData.append("name", rawData.name);
    if (rawData.info != "") {
      formData.append("info", rawData.info);
    }
    if (rawData.banner != "") {
      formData.append("banner", rawData.banner);
    }
    if (rawData.banner != "") {
      formData.append("highlight", rawData.highlight);
    }
    try {
      const { data } = await axios.post("/api/brands", formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  },
  async modifyBrand({ commit }, rawData) {
    let formData = new FormData();
    formData.append("name", rawData.name);
    formData.append("_method", "PUT");
    if (rawData.info != "") {
      formData.append("info", rawData.info);
    }
    if (rawData.banner != "" && rawData.banner != undefined) {
      formData.append("banner", rawData.banner);
    }
    if (rawData.highlight != "" && rawData.highlight != undefined) {
      formData.append("highlight", rawData.highlight);
    }
    try {
      const { data } = await axios.post(`/api/brands/${rawData.id}`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  },

  async deleteBrand({ commit }, brand) {
    try {
      const { data } = await axios.delete(`/api/brands/${brand.id}`);
      commit("DELETE_BRAND_SUCCESS", data);
    } catch (e) {
      throw e;
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
