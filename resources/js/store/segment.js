import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    segments: []
});

// getters
const getters = {
    segments: state => state.segments
};

// mutations
const mutations = {
    FETCH_SEGMENTS_SUCCESS(state, segments) {
        state.segments = _.sortBy(segments, ["id"]);
    },

    ADD_SEGMENT_SUCCESS(state, segment) {
        state.segments.push(segment);
    },

    EDIT_SEGMENT_SUCCESS(state, updatedSegment) {
        const index = state.segments.findIndex(
            segment => segment.id === updatedSegment.id
        );
        if (index !== -1) {
            state.segments.splice(index, 1, updatedSegment);
        }
    },

    DELETE_SEGMENT_SUCCESS(state, segment) {
        state.segments = state.segments.filter(t => t.id !== segment.id);
    }
};

// actions
const actions = {
    async fetchSegments({ commit }) {
        try {
            const { data } = await axios.get("/api/segments");
            commit("FETCH_SEGMENTS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createSegment({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        try {
            const { data } = await axios.post("/api/segments", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifySegment({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("_method", "PUT");
        try {
            const { data } = await axios.post(
                `/api/segments/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },

    async deleteSegment({ commit }, segment) {
        try {
            const { data } = await axios.delete(`/api/segments/${segment.id}`);
            commit("DELETE_SEGMENT_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
