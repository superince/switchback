import axios from "axios";

// state
const state = () => ({
    subscribers: []
});

// getters
const getters = {
    subscribers: state => state.subscribers
};

// mutations
const mutations = {
    FETCH_SUBSCRIBERS_SUCCESS(state, subscribers) {
        state.subscribers = _.sortBy(subscribers, ["id"]);
    },
    ADD_SUBSCRIBER_SUCCESS(state, blog) {
        state.subscribers.push(blog);
    },
    EDIT_SUBSCRIBER_SUCCESS(state, updatedBlog) {
        const index = state.subscribers.findIndex(
            blog => blog.id === updatedBlog.id
        );
        if (index !== -1) {
            state.subscribers.splice(index, 1, updatedBlog);
        }
    },
    DELETE_SUBSCRIBER_SUCCESS(state, subscriber) {
        state.subscribers = state.subscribers.filter(
            t => t.id !== subscriber.id
        );
    }
};

// actions
const actions = {
    //   async createSubscriber({ commit }, rawData) {
    //     try {
    //       const { data } = await axios.post("/subscribers", formData);
    //       return data;
    //     } catch (e) {
    //       throw e;
    //     }
    //   },
    //   async modifySubscriber({ commit }, rawData) {
    //     try {
    //       const { data } = await axios.put(`/subscribers/${rawData.id}`, formData);
    //       return data;
    //     } catch (e) {
    //       throw e;
    //     }
    //   },
    async fetchSubscribers({ commit }) {
        try {
            const { data } = await axios.get("/api/subscribers");
            commit("FETCH_SUBSCRIBERS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteSubscriber({ commit }, blog) {
        try {
            const { data } = await axios.delete(`/api/subscribers/${blog.id}`);
            commit("DELETE_SUBSCRIBER_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
