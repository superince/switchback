const state = () => ({
    dealers: []
});

const getters = {
    dealers: state => state.dealers
};

const actions = {
    async fetchDealers({ commit }) {
        try {
            const response = await axios.get(`/api/dealers`);
            commit("FETCH_DEALERS_SUCCESS", response.data);
        } catch (e) {
            return e;
        }
    },
    async modifyDealer({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        formData.append("name", rawData.name);
        formData.append("location", rawData.location);
        formData.append("phone", rawData.phone);
        formData.append("address", rawData.address);
        formData.append("website", rawData.website);
        formData.append("category", rawData.category);
        if (rawData.photo) {
            formData.append("logo", rawData.photo);
        }

        try {
            const { data } = await axios.post(
                `/api/dealers/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            return 401;
        }
    },
    async createDealer({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("location", rawData.location);
        formData.append("phone", rawData.phone);
        formData.append("address", rawData.address);
        formData.append("website", rawData.website);
        formData.append("category", rawData.category);
        if (rawData.photo) {
            formData.append("logo", rawData.photo);
        }
        try {
            const { data } = await axios.post(`/api/dealers`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            return e;
        }
    },
    async deleteDealer({ commit }, rawData) {
        try {
            const { data } = await axios.delete(`/api/dealers/${rawData.id}`);
            commit("DELETE_DEALER_SUCCESS", data);
        } catch (e) {
            return e;
        }
    }
};

const mutations = {
    FETCH_DEALERS_SUCCESS: (state, dealers) => {
        state.dealers = dealers;
    },
    ADD_DEALER_SUCCESS(state, dealer) {
        state.dealers.push(dealer);
    },
    MODIFY_DEALER_SUCCESS(state, updatedDealer) {
        const index = state.dealers.findIndex(
            notice => notice.id === updatedDealer.id
        );
        if (index !== -1) {
            state.dealers.splice(index, 1, updatedDealer);
        }
    },
    DELETE_DEALER_SUCCESS(state, dealer) {
        state.dealers = state.dealers.filter(t => t.id !== dealer.id);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
