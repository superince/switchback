import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    blogs: [],
    tags: []
});

// getters
const getters = {
    blogs: state => state.blogs,
    tags: state => state.tags
};

// mutations
const mutations = {
    FETCH_BLOGS_SUCCESS(state, blogs) {
        state.blogs = _.sortBy(blogs, ["id"]);
    },
    FETCH_TAGS_SUCCESS(state, tags) {
        state.tags = _.sortBy(tags, ["id"]);
    },
    ADD_BLOG_SUCCESS(state, blog) {
        state.blogs.push(blog);
    },

    EDIT_BLOG_SUCCESS(state, updatedBlog) {
        const index = state.blogs.findIndex(blog => blog.id === updatedBlog.id);
        if (index !== -1) {
            state.blogs.splice(index, 1, updatedBlog);
        }
    },

    DELETE_BLOG_SUCCESS(state, blog) {
        state.blogs = state.blogs.filter(t => t.id !== blog.id);
    }
};

// actions
const actions = {
    async createBlog({ commit }, rawData) {
        let formData = new FormData();
        formData.append("title", rawData.title);
        formData.append("short_description", rawData.short_description);
        formData.append("description", rawData.description);
        formData.append("registration", rawData.registration);
        rawData.chips.map(chip => {
            formData.append("tags[]", chip);
        });
        if (rawData.photos) {
            rawData.photos.map(photo => {
                formData.append("photos[]", photo.originFileObj);
            });
        }
        try {
            const { data } = await axios.post("/api/blogs", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyBlog({ commit }, rawData) {
        let formData = new FormData();
        formData.append("title", rawData.title);
        formData.append("short_description", rawData.short_description);
        formData.append("description", rawData.description);
        formData.append("registration", rawData.registration);
        formData.append("_method", "PUT");
        rawData.chips.map(chip => {
            formData.append("tags[]", chip);
        });
        if (rawData.photos) {
            rawData.photos.map(photo => {
                if (photo.originFileObj) {
                    formData.append("photos[]", photo.originFileObj);
                }
            });
        }
        try {
            const { data } = await axios.post(
                `/api/blogs/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchBlogs({ commit }) {
        try {
            const { data } = await axios.get("/api/blogs");
            commit("FETCH_BLOGS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async fetchInvidisualBlog({ commit }, id) {
        try {
            const { data } = await axios.get(`/api/blogs/${id}`);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeRegistration({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/blogs/registration/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchTags({ commit }) {
        try {
            const { data } = await axios.get("/api/blogs/tags");
            commit("FETCH_TAGS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteBlog({ commit }, blog) {
        try {
            const { data } = await axios.delete(`/api/blogs/${blog.id}`);
            commit("DELETE_BLOG_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
