import axios from "axios";

// state
const state = () => ({
    productCategories: []
});

// getters
const getters = {
    productCategories: state => state.productCategories
};

// mutations
const mutations = {
    FETCH_PRODUCT_CATEGORIES_SUCCESS(state, productCategories) {
        state.productCategories = productCategories;
    },

    ADD_PRODUCT_CATEGORY_SUCCESS(state, productCategory) {
        state.productCategories.push(productCategory);
    },

    EDIT_PRODUCT_CATEGORY_SUCCESS(state, updatedProductCategory) {
        const index = state.productCategories.findIndex(
            category => category.id === updatedProductCategory.id
        );
        if (index !== -1) {
            state.productCategories.splice(index, 1, updatedProductCategory);
        }
    },

    DELETE_PRODUCT_CATEGORY_SUCCESS(state, productCategory) {
        state.productCategories = state.productCategories.filter(
            t => t.id !== productCategory.id
        );
    }
};

// actions
const actions = {
    async fetchProductCategories({ commit }) {
        try {
            const { data } = await axios.get("/api/productcategories");
            commit("FETCH_PRODUCT_CATEGORIES_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createProductCategory({ commit }, rawData) {
        try {
            let formData = new FormData();
            formData.append("name", rawData.name);
            const { data } = await axios.post(
                "/api/productcategories",
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyProductCategory({ commit }, rawData) {
        try {
            let formData = new FormData();
            formData.append("name", rawData.name);
            formData.append("_method", "PUT");
            const { data } = await axios.post(
                `/api/productcategories/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async deleteProductCategory({ commit }, productcategory) {
        try {
            const { data } = await axios.delete(
                `/api/productcategories/${productcategory.id}`
            );
            commit("DELETE_PRODUCT_CATEGORY_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
