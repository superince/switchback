import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    products: []
});

// getters
const getters = {
    products: state => state.products
};

// mutations
const mutations = {
    FETCH_PRODUCTS_SUCCESS(state, products) {
        state.products = products;
  
    },

    ADD_PRODUCT_SUCCESS(state, product) {
        state.products.push(product);
    },

    EDIT_PRODUCT_SUCCESS(state, updatedProduct) {
        const index = state.products.findIndex(
            product => product.id === updatedProduct.id
        );
      
        if (index !== -1) {
            state.products.splice(index, 1, updatedProduct);
        }
    },
    EDIT_VARIANT_SUCCESS(state, updatedVariant) {
        state.products.map(product => {
            const index = product.variants.findIndex(
                variant => variant.id === updatedVariant.id
            );
            if (index !== -1) {
                product.variants.splice(index, 1, updatedProduct);
            }
        });
    },

    DELETE_PRODUCT_SUCCESS(state, product) {
        state.products = state.products.filter(t => t.id !== product.id);
    },

    DELETE_VARIANT_SUCCESS(state, variant) {
        state.products.map(product => {
            if (product.id == variant.product_id) {
                product.variants = product.variants.filter(
                    t => t.id !== variant.id
                );
            }
        });
    }
};

// actions
const actions = {
    async createProduct({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("price", rawData.price);
        if (rawData.sale_price != "" && rawData.sale_price != undefined) {
            formData.append("sale_price", rawData.sale_price);
        }
        if (
            rawData.discount_price != "" &&
            rawData.discount_price != undefined
        ) {
            formData.append("discount_price", rawData.discount_price);
        }
        formData.append("brand_id", rawData.brand_id);
        formData.append("product_category_id", rawData.product_category_id);
        formData.append("short_description", rawData.short_description);
        if (rawData.features != "") {
            formData.append("features", rawData.features);
        }
        formData.append("sale", rawData.sale);
        formData.append("featured", rawData.featured);
        formData.append("published", rawData.published);
        formData.append("is_highlighted", rawData.is_highlighted);
        formData.append("color", rawData.color);
        formData.append("size", JSON.stringify(rawData.sizeQuantity));

        rawData.divisions.map(division => {
            formData.append("division[]", division);
        });

        rawData.genders.map(gender => {
            formData.append("gender[]", gender);
        });
        rawData.tags.map(tag => {
            formData.append("tag[]", tag);
        });

        if (rawData.sizeQuantity.length > 0) {
            rawData.sizeQuantity.map(sq => {
                formData.append(sq.key, sq.quantity);
            });
        }
        if (rawData.photos) {
            rawData.photos.map(photo => {
                formData.append("photos[]", photo.originFileObj);
            });
        }
        try {
            const { data } = await axios.post("/api/products", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async createVariant({ commit }, rawData) {
        let formData = new FormData();
        formData.append("color", rawData.color);
        formData.append("price", rawData.price);
        formData.append(
            "sale_price",
            rawData.sale_price == "" ? 0 : rawData.sale_price
        );
        formData.append("published", rawData.published);
        formData.append("is_highlighted", rawData.is_highlighted);
        //check size
        formData.append("size", JSON.stringify(rawData.sizeQuantity));

        if (rawData.photos) {
            rawData.photos.map(photo => {
                formData.append("photos[]", photo.originFileObj);
            });
        }

        if (rawData.sizeQuantity && rawData.sizeQuantity.length > 0) {
            rawData.sizeQuantity.map(sq => {
                formData.append(sq.key, sq.quantity);
            });
        }
        try {
            const { data } = await axios.post(
                `/api/products/${rawData.product_id}/variants`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return 200;
        } catch (e) {
            return 401;
        }
    },
    async modifyProduct({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("price", rawData.price);
        formData.append("_method", "PUT");
        if (rawData.sale_price != "") {
            formData.append("sale_price", rawData.sale_price);
        }

        formData.append("brand_id", rawData.brand_id);
        formData.append("product_category_id", rawData.product_category_id);
        formData.append("short_description", rawData.short_description);
        if (rawData.features != "") {
            formData.append("features", rawData.features);
        }
        formData.append("sale", rawData.sale);
        formData.append("featured", rawData.featured);
        formData.append("published", rawData.published);
        formData.append("is_highlighted", rawData.is_highlighted);
        formData.append("color", rawData.color);
        formData.append("size", JSON.stringify(rawData.sizeQuantity));
        rawData.divisions.map(division => {
            formData.append("division[]", division);
        });

        rawData.genders.map(gender => {
            formData.append("gender[]", gender);
        });
        rawData.tags.map(tag => {
            formData.append("tag[]", tag);
        });

        if (rawData.photos) {
            rawData.photos.map(photo => {
                if (photo.originFileObj) {
                    formData.append("photos[]", photo.originFileObj);
                }
            });
        }
        try {
            const { data } = await axios.post(
                `/api/products/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyVariant({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        formData.append("color", rawData.color);
        formData.append("price", rawData.price);
        formData.append("sale_price", rawData.sale_price);
        formData.append("published", rawData.published);
        formData.append("is_highlighted", rawData.is_highlighted);
        formData.append("size", JSON.stringify(rawData.sizeQuantity));

        if (rawData.photos) {
            rawData.photos.map(photo => {
                if (photo.originFileObj) {
                    formData.append("photos[]", photo.originFileObj);
                }
            });
        }

        try {
            const { data } = await axios.post(
                `/api/variants/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            return 401;
        }
    },
    async fetchProducts({ commit }) {
        try {
            const { data } = await axios.get("/api/products");
            commit("FETCH_PRODUCTS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async fetchProductVariant({ commit }, id) {
        try {
            const { data } = await axios.get(`/api/products/${id}/variants`);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchIndivisualProduct({ commit }, id) {
        try {
            const { data } = await axios.get(`/api/products/${id}`);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchIndivisualVariant({ commit }, id) {
        try {
            const { data } = await axios.get(`/api/variants/${id}`);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async deleteProduct({ commit }, category) {
        try {
            const { data } = await axios.delete(`/api/products/${category.id}`);
            commit("DELETE_PRODUCT_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteVariant({ commit }, id) {
        try {
            const { data } = await axios.delete(`/api/variants/${id}`);
            commit("DELETE_VARIANT_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async changeParallex({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/parallex/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeVariantParallex({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/variants/parallex/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changePublished({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/published/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeVariantPublished({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/variants/published/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeFeatured({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/featured/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changePrice({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/price/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeVariantPrice({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/variants/price/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeSalePercentage({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/salepercent/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeVariantSalePercentage({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/variants/salepercent/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeSale({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/products/sale/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeVariantSale({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/variants/sale/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
