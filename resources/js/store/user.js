import axios from "axios";
// state
const state = () => ({
    users: []
});

// getters
const getters = {
    users: state => state.users,
    addUserForm: state => state.addUserForm
};

// mutations
const mutations = {
    FETCH_USERS_SUCCESS(state, users) {
        state.users = users;
    },

    ADD_USER_SUCCESS(state, user) {
        state.users.push(user);
    },

    EDIT_USER_SUCCESS(state, updatedUser) {
        const index = state.users.findIndex(user => user.id === updatedUser.id);
        if (index !== -1) {
            state.users.splice(index, 1, updatedUser);
        }
    },

    DELETE_USER_SUCCESS(state, user) {
        state.users = state.users.filter(t => t.id !== user.id);
    }
};

// actions
const actions = {
    async fetchUsers({ commit }) {
        try {
            const { data } = await axios.get("/api/users");
            commit("FETCH_USERS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createUser({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("email", rawData.email);
        formData.append("password", rawData.password);
        formData.append("type", rawData.role);
        try {
            const { data } = await axios.post("/api/users", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyUser({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("email", rawData.email);
        formData.append("password", rawData.password);
        formData.append("type", rawData.role);
        formData.append("_method", "PUT");
        try {
            const { data } = await axios.post(
                `/api/users/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async deleteUser({ commit }, user) {
        try {
            const { data } = await axios.delete(`/api/users/${user.id}`);
            commit("DELETE_USER_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
