import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    orders: []
});

// getters
const getters = {
    orders: state => state.orders
};

// mutations
const mutations = {
    FETCH_ORDERS_SUCCESS(state, orders) {
        state.orders = _.sortBy(orders, ["id"]);
    },
    UPDATE_ORDERS_SUCCESS(state,updatedOrder){
        const index = state.orders.findIndex(order => order.id === updatedOrder.id);
        if (index !== -1) {
            state.orders.splice(index, 1, updatedOrder);
        }
    },
    DELETE_ORDER_SUCCESS(state, order) {
        state.orders = state.orders.filter(t => t.id !== order.id);
    }
};

// actions
const actions = {
    async fetchOrders({ commit }) {
        try {
            const { data } = await axios.get("/api/orders");
            commit("FETCH_ORDERS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },

    async archiveOrder({commit},rawData){
        let formData = new FormData();
        formData.append("archived", rawData.archived);
        try {
          const { data } = await axios.post(
            `/api/orders/archive/${rawData.id}`,
            formData
          );
          commit("UPDATE_ORDERS_SUCCESS",data);
          return 200;
        } catch (e) {
          throw e;   
        }
    },

    async changeStatus({commit},rawData){
        let formData = new FormData();
        formData.append("status", rawData.status);
        try {
          const { data } = await axios.post(
            `/api/orders/${rawData.id}`,
            formData
          );
          commit("UPDATE_ORDERS_SUCCESS",data);
          return 200;
        } catch (e) {
          throw e;   
        }
    },

    async deleteOrder({ commit }, order) {
        try {
            const { data } = await axios.delete(`/api/orders/${order.id}`);
            commit("DELETE_ORDER_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async sendEmail({ commit }, order) {
        try {
            let formData = new FormData();
            formData.append("status", order.status);
            const { data } = await axios.post(`/api/mail/sendmail/${order.id}`,formData);
            commit("UPDATE_ORDERS_SUCCESS",data);
            return 200;
        } catch (e) {
            throw e;
        }
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
