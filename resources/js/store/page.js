import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    pages: []
});

// getters
const getters = {
    pages: state => state.pages
};

// mutations
const mutations = {
    FETCH_PAGES_SUCCESS(state, pages) {
        state.pages = _.sortBy(pages, ["id"]);
    },

    ADD_PAGE_SUCCESS(state, page) {
        state.pages.push(page);
    },

    EDIT_PAGE_SUCCESS(state, updatedPage) {
        const index = state.pages.findIndex(page => page.id === updatedPage.id);
        if (index !== -1) {
            state.pages.splice(index, 1, updatedPage);
        }
    },

    DELETE_PAGE_SUCCESS(state, page) {
        state.pages = state.pages.filter(t => t.id !== page.id);
    }
};

// actions
const actions = {
    async fetchPages({ commit }) {
        try {
            const { data } = await axios.get("/api/pages");
            commit("FETCH_PAGES_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createPage({ commit }, rawData) {
        let formData = new FormData();
        formData.append("title", rawData.title);
        if (rawData.content != "") {
            formData.append("content", rawData.content);
        }
        try {
            const { data } = await axios.post("/api/pages", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyPage({ commit }, rawData) {
        let formData = new FormData();
        formData.append("title", rawData.title);
        formData.append("_method", "PUT");
        if (rawData.content != "" && rawData.content != undefined) {
            formData.append("content", rawData.content);
        }
        try {
            const { data } = await axios.post(
                `/api/pages/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },

    async deletePage({ commit }, page) {
        try {
            const { data } = await axios.delete(
                `/api/pages/${page.id}`
            );
            commit("DELETE_PAGE_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
