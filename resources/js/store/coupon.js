import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    coupons: []
});

// getters
const getters = {
    coupons: state => state.coupons
};

// mutations
const mutations = {
    FETCH_COUPONS_SUCCESS(state, coupons) {
        state.coupons = _.sortBy(coupons, ["id"]);
    },

    ADD_COUPON_SUCCESS(state, coupon) {
        state.coupons.push(coupon);
    },

    EDIT_COUPON_SUCCESS(state, updatedCoupon) {
        const index = state.coupons.findIndex(
            category => category.id === updatedCoupon.id
        );
        if (index !== -1) {
            state.coupons.splice(index, 1, updatedCoupon);
        }
    },

    DELETE_COUPON_SUCCESS(state, coupon) {
        state.coupons = state.coupons.filter(t => t.id !== coupon.id);
    }
};

// actions
const actions = {
    async createCoupons({ commit }, rawData) {
        let formData = new FormData();
        formData.append("code", rawData.code);
        formData.append("amount", rawData.amount);
        formData.append("expiry", rawData.expiry);
        formData.append("cart_limit", rawData.cart_limit);
        formData.append("used", 0);
        if (rawData.type == "Percentage Discount") {
            formData.append("type", 1);
        } else if (rawData.type == "Fixed Cart Discount") {
            formData.append("type", 0);
        }
        if (rawData.description != "") {
            formData.append("description", rawData.description);
        }
        if (rawData.limit != 0) {
            formData.append("limit", rawData.limit);
        }
        try {
            const { data } = await axios.post("/api/coupons", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyCoupon({ commit }, rawData) {
        let formData = new FormData();
        formData.append("code", rawData.code);
        formData.append("_method", "PUT");
        formData.append("amount", rawData.amount);
        formData.append("expiry", rawData.expiry);
        formData.append("cart_limit", rawData.cart_limit);
        if (rawData.type == "Percentage Discount") {
            formData.append("type", 1);
        } else if (rawData.type == "Fixed Cart Discount") {
            formData.append("type", 0);
        }
        if (rawData.description != "") {
            formData.append("description", rawData.description);
        }
        if (rawData.limit != 0) {
            formData.append("limit", rawData.limit);
        }
        try {
            const { data } = await axios.post(
                `/api/coupons/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchCoupons({ commit }) {
        try {
            const { data } = await axios.get("/api/coupons");
            commit("FETCH_COUPONS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteCoupon({ commit }, coupon) {
        try {
            const { data } = await axios.delete(`/api/coupons/${coupon.id}`);
            commit("DELETE_COUPON_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
