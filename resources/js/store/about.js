import axios from "axios";
import _ from "lodash";

// actions
export const actions = {
    async modifyAbout({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        if (rawData.description) {
            formData.append("description", rawData.description);
        }
        if (rawData.photo) {
            formData.append("photo", rawData.photo);
        }
        try {
          
            const { data } = await axios.post(`/api/about/1`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchAbout({ commit }) {
        try {
            const { data } = await axios.get("/api/about");
            return data;
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    actions
};
