const state = () => ({
    notices: []
});

const getters = {
    notices: state => state.notices
};

const actions = {
    async fetchNotices({ commit }) {
        try {
            const response = await axios.get(`/api/notices`);
            commit("FETCH_NOTICES_SUCCESS", response.data);
        } catch (e) {
            return e;
        }
    },
    async modifyNotice({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        formData.append("title", rawData.title);
        if (rawData.link) {
            formData.append("link", rawData.link);
        }
        if (rawData.description) {
            formData.append("description", rawData.description);
        }
        formData.append("status", rawData.status == true ? 1 : 0);
        if (rawData.photo) {
            formData.append("photo", rawData.photo);
        }

        try {
            const { data } = await axios.post(
                `/api/notices/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            return 401;
        }
    },
    async createNotice({ commit }, rawData) {
        let formData = new FormData();
        formData.append("title", rawData.title);
        if (rawData.link) {
          formData.append("link", rawData.link);
      }
      if (rawData.description) {
          formData.append("description", rawData.description);
      }
        formData.append("status", rawData.status == true ? 1 : 0);
        if (rawData.photo) {
            formData.append("photo", rawData.photo);
        }
        try {
            const { data } = await axios.post(`/api/notices`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            return e;
        }
    },
    async deleteNotice({ commit }, rawData) {
        try {
            const { data } = await axios.delete(`/api/notices/${rawData.id}`);
            commit("DELETE_NOTICE_SUCCESS", data);
        } catch (e) {
            return e;
        }
    },
    async changeStatus({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/notices/status/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
};

const mutations = {
    FETCH_NOTICES_SUCCESS: (state, notices) => {
        state.notices = notices;
    },
    ADD_NOTICE_SUCCESS(state, notice) {
        state.notices.push(notice);
    },
    MODIFY_NOTICE_SUCCESS(state, updatedNotice) {
        const index = state.notices.findIndex(
            notice => notice.id === updatedNotice.id
        );
        if (index !== -1) {
            state.notices.splice(index, 1, updatedNotice);
        }
    },
    DELETE_NOTICE_SUCCESS(state, notice) {
        state.notices = state.notices.filter(t => t.id !== notice.id);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
