import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    messages: []
});

// getters
const getters = {
    messages: state => state.messages
};

// mutations
const mutations = {
    FETCH_MESSAGES_SUCCESS(state, messages) {
        state.messages = _.sortBy(messages, ["id"]);
    },

    ADD_CATEGORY_SUCCESS(state, category) {
        state.categories.push(category);
    },

    EDIT_MESSAGE_SUCCESS(state, updatedMessage) {
        const index = state.messages.findIndex(
            message => message.id === updatedMessage.id
        );
        if (index !== -1) {
            state.messages.splice(index, 1, updatedMessage);
        }
    },

    DELETE_MESSAGE_SUCCESS(state, message) {
        state.messages = state.messages.filter(t => t.id !== message.id);
    }
};

// actions
const actions = {
    async fetchMessages({ commit }) {
        try {
            const { data } = await axios.get("/api/messages");
            commit("FETCH_MESSAGES_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createMessage({ commit }, rawData) {
        try {
            const { data } = await axios.post("/api/messages", rawData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyMessage({ commit }, rawData) {
        try {
            const { data } = await axios.put(
                `/api/messages/${rawData.id}`,
                rawData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async sendReply({ commit }, rawData) {
        let formData = new FormData();
        formData.append("subject", rawData.subject);
        formData.append("message", rawData.message);
        try {
            const { data } = await axios.post(
                `/api/messages/send/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },

    async deleteMessage({ commit }, message) {
        try {
            const { data } = await axios.delete(`/api/messages/${message.id}`);
            commit("DELETE_MESSAGE_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
