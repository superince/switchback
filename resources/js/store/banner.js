import axios from "axios";
import _ from "lodash";

const state = () => ({
    banners: []
});

// getters
const getters = {
    banners: state => state.banners
};

// mutations
const mutations = {
    FETCH_BANNERS_SUCCESS(state, banners) {
        state.banners = _.sortBy(banners, ["id"]);
    },

    ADD_BANNER_SUCCESS(state, banner) {
        state.banners.push(banner);
    },

    EDIT_BANNER_SUCCESS(state, updatedBanner) {
        const index = state.banners.findIndex(
            color => color.id === updatedBanner.id
        );
        if (index !== -1) {
            state.banners.splice(index, 1, updatedBanner);
        }
    },

    DELETE_BANNER_SUCCESS(state, banner) {
        state.banners = state.banners.filter(t => t.id !== banner.id);
    }
};

// actions
const actions = {
    async createBanner({ commit }, rawData) {
        let formData = new FormData();
        if (rawData.url) {
            formData.append("url", rawData.url);
        }
        if (rawData.primary_tagline) {
            formData.append("primary_tagline", rawData.primary_tagline);
        }
        if (rawData.secondary_tagline) {
            formData.append("secondary_tagline", rawData.secondary_tagline);
        }
        formData.append("status", rawData.status == true ? 1 : 0);
        if (rawData.photo) {
            formData.append("photo", rawData.photo);
        }
        try {
            const { data } = await axios.post("/api/banners", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async createVideoBanner({ commit }, rawData) {
        let formData = new FormData();
        if (rawData.url) {
            formData.append("url", rawData.url);
        }
        if (rawData.primary_tagline) {
            formData.append("primary_tagline", rawData.primary_tagline);
        }
        if (rawData.secondary_tagline) {
            formData.append("secondary_tagline", rawData.secondary_tagline);
        }
        formData.append("status", rawData.status == true ? 1 : 0);

        if (rawData.video) {
            formData.append("video", rawData.video);
        }
        try {
            const { data } = await axios.post("/api/banners/video", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyBannerVideo({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        if (rawData.url) {
            formData.append("url", rawData.url);
        }
        if (rawData.primary_tagline) {
            formData.append("primary_tagline", rawData.primary_tagline);
        }
        if (rawData.secondary_tagline) {
            formData.append("secondary_tagline", rawData.secondary_tagline);
        }
        formData.append("status", rawData.status == true ? 1 : 0);
        try {
            const { data } = await axios.post(
                `/api/banners/video/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyBanner({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        if (rawData.url) {
            formData.append("url", rawData.url);
        }
        if (rawData.primary_tagline) {
            formData.append("primary_tagline", rawData.primary_tagline);
        }
        if (rawData.secondary_tagline) {
            formData.append("secondary_tagline", rawData.secondary_tagline);
        }
        formData.append("status", rawData.status == true ? 1 : 0);
        if (rawData.photo) {
            formData.append("photo", rawData.photo);
        }
        try {
            const { data } = await axios.post(
                `/api/banners/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async changeStatus({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/banners/status/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchBanners({ commit }) {
        try {
            const { data } = await axios.get("/api/banners");
            commit("FETCH_BANNERS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteBanner({ commit }, banner) {
        try {
            const { data } = await axios.delete(`/api/banners/${banner.id}`);
            commit("DELETE_BANNER_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteVideoBanner({ commit }, banner) {
        try {
            const { data } = await axios.delete(`/api/banners/video/${banner.id}`);
            commit("DELETE_BANNER_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
