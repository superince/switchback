import axios from "axios";

// state
export const state = () => ({
    user: null,
    token: null
});

// getters
export const getters = {
    user: state => state.user
};

// mutations
export const mutations = {
    FETCH_USER_SUCCESS(state, user) {
        state.user = user;
    },

    UPDATE_USER(state, { user }) {
        state.user = user;
    }
};

// actions
export const actions = {
    async fetchUser({ commit }) {
        try {
            const { data } = await axios.get("/api/user");
            commit("FETCH_USER_SUCCESS", data);
        } catch (e) {}
    },

    updateUser({ commit }, payload) {
        commit("UPDATE_USER", payload);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
