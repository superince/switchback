import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    divisions: []
});

// getters
const getters = {
    divisions: state => state.divisions
};

// mutations
const mutations = {
    FETCH_DIVISIONS_SUCCESS(state, divisions) {
        state.divisions = _.sortBy(divisions, ["id"]);
    },

    ADD_DIVISION_SUCCESS(state, division) {
        state.divisions.push(division);
    },

    EDIT_DIVISION_SUCCESS(state, updatedDivision) {
        const index = state.divisions.findIndex(
            category => category.id === updatedDivision.id
        );
        if (index !== -1) {
            state.divisions.splice(index, 1, updatedDivision);
        }
    },

    DELETE_DIVISION_SUCCESS(state, division) {
        state.divisions = state.divisions.filter(t => t.id !== division.id);
    }
};

// actions
const actions = {
    async fetchDivisions({ commit }) {
        try {
            const { data } = await axios.get("/api/divisions");
            commit("FETCH_DIVISIONS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createDivision({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        if (rawData.banner) {
            formData.append("banner", rawData.banner);
        }
        if (rawData.banner) {
            formData.append("highlight", rawData.highlight);
        }
        if (rawData.info != "") {
            formData.append("info", rawData.info);
        }
        try {
            const { data } = await axios.post("/api/divisions", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyDivision({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("_method", "PUT");
        if (rawData.banner) {
            formData.append("banner", rawData.banner);
        }
        if (rawData.highlight) {
            formData.append("highlight", rawData.highlight);
        }
        
        if (rawData.info != "") {
            formData.append("info", rawData.info);
        }
        try {
            const { data } = await axios.post(
                `/api/divisions/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },

    async deleteDivision({ commit }, division) {
        try {
            const { data } = await axios.delete(`/api/divisions/${division.id}`);
            commit("DELETE_DIVISION_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
