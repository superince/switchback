import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    shippings: []
});

// getters
const getters = {
    shippings: state => state.shippings
};

// mutations
const mutations = {
    FETCH_SHIPPINGS_SUCCESS(state, shippings) {
        state.shippings = _.sortBy(shippings, ["id"]);
    },
    ADD_SHIPPING_SUCCESS(state, shipping) {
        state.shippings.push(shipping);
    },
    EDIT_SHIPPING_SUCCESS(state, updatedShipping) {
        const index = state.shippings.findIndex(
            shipping => shipping.id === updatedShipping.id
        );
        if (index !== -1) {
            state.shippings.splice(index, 1, updatedShipping);
        }
    },
    DELETE_SEGMENT_SUCCESS(state, shipping) {
        state.shippings = state.shippings.filter(t => t.id !== shipping.id);
    }
};

// actions
const actions = {
    async fetchShippings({ commit }) {
        try {
            const { data } = await axios.get("/api/shippings");
            commit("FETCH_SHIPPINGS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createShipping({ commit }, rawData) {
        let formData = new FormData();
        formData.append("address", rawData.address);
        formData.append("amount", rawData.amount);
        formData.append("status", rawData.status);
        try {
            const { data } = await axios.post("/api/shippings", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyShipping({ commit }, rawData) {
        let formData = new FormData();
        formData.append("address", rawData.address);
        formData.append("amount", rawData.amount);
        formData.append("status", rawData.status);
        formData.append("_method", "PUT");
        try {
            const { data } = await axios.post(
                `/api/shippings/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async deleteShipping({ commit }, shipping) {
        try {
            const { data } = await axios.delete(
                `/api/shippings/${shipping.id}`
            );
            commit("DELETE_SEGMENT_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async changeStatus({ commit }, rawData) {
        let formData = new FormData();
        formData.append("value", rawData.value);
        try {
            const { data } = await axios.post(
                `/api/shippings/status/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
