import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    teams: []
});

// getters
const getters = {
    teams: state => state.teams
};

// mutations
const mutations = {
    FETCH_TEAMS_SUCCESS(state, teams) {
        state.teams = _.sortBy(teams, ["id"]);
    },

    ADD_TEAM_SUCCESS(state, team) {
        state.teams.push(team);
    },

    EDIT_TEAM_SUCCESS(state, updatedTeam) {
        const index = state.teams.findIndex(team => team.id === updatedTeam.id);
        if (index !== -1) {
            state.teams.splice(index, 1, updatedTeam);
        }
    },

    DELETE_TEAM_SUCCESS(state, team) {
        state.teams = state.teams.filter(t => t.id !== team.id);
    }
};

// actions
const actions = {
    async createTeam({ commit }, rawData) {
        let formData = new FormData();
        if (rawData.insta_link != "") {
            formData.append("insta_link", rawData.insta_link);
        }
        if (rawData.fb_link != "") {
            formData.append("fb_link", rawData.fb_link);
        }
        if (rawData.bio != "") {
            formData.append("bio", rawData.bio);
        }

        formData.append("name", rawData.name);
        formData.append("segment_id", rawData.segment_id);

        if (rawData.photo) {
            formData.append("photo", rawData.photo.originFileObj);
        }
        try {
            const { data } = await axios.post("/api/teams", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyTeam({ commit }, rawData) {
        let formData = new FormData();
        formData.append("_method", "PUT");
        if (rawData.insta_link != "") {
            formData.append("insta_link", rawData.insta_link);
        }
        if (rawData.fb_link != "") {
            formData.append("fb_link", rawData.fb_link);
        }
        if (rawData.bio != "") {
            formData.append("bio", rawData.bio);
        }

        formData.append("name", rawData.name);
        formData.append("segment_id", rawData.segment_id);

        if (rawData.photo) {
            if (rawData.photo.originFileObj) {
                formData.append("photo", rawData.photo.originFileObj);
            }
        }
        try {
            const { data } = await axios.post(
                `/api/teams/${rawData.id}`,
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                }
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchTeams({ commit }) {
        try {
            const { data } = await axios.get("/api/teams");
            commit("FETCH_TEAMS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteTeam({ commit }, team) {
        try {
            const { data } = await axios.delete(`/api/teams/${team.id}`);
            commit("DELETE_TEAM_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
