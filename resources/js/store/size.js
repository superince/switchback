import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    sizes: []
});

// getters
const getters = {
    sizes: state => state.sizes
};

// mutations
const mutations = {
    FETCH_SIZES_SUCCESS(state, sizes) {
        state.sizes = _.sortBy(sizes, ["id"]);
    },

    ADD_SIZE_SUCCESS(state, segment) {
        state.sizes.push(segment);
    },

    EDIT_SIZE_SUCCESS(state, updatedSize) {
        const index = state.sizes.findIndex(size => size.id === updatedSize.id);
        if (index !== -1) {
            state.sizes.splice(index, 1, updatedSize);
        }
    },

    DELETE_SIZE_SUCCESS(state, size) {
        state.sizes = state.sizes.filter(t => t.id !== size.id);
    }
};

// actions
const actions = {
    async fetchSizes({ commit }) {
        try {
            const { data } = await axios.get("/api/sizes");
            commit("FETCH_SIZES_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async createSize({ commit }, rawData) {
        let formData = new FormData();
        formData.append("group", rawData.name);
        if (rawData.sizechart != "" && rawData.sizechart != undefined) {
            formData.append("sizechart", rawData.sizechart);
        }
        for (const size of rawData.sizes) {
            formData.append("name[]", size.size);
        }
        try {
            const { data } = await axios.post("/api/sizes", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifySize({ commit }, rawData) {
        let formData = new FormData();
        formData.append("group", rawData.name);
        if (rawData.sizechart != "" && rawData.sizechart != undefined) {
            formData.append("sizechart", rawData.sizechart);
        }
        formData.append("_method", "PUT");
        for (const size of rawData.sizes) {
            if(size.size!=""){
                formData.append("name[]", JSON.stringify({size:size.size,id:size.id}));
            }
        }
        try {
            const { data } = await axios.post(
                `/api/sizes/${rawData.id}`,
                formData
            );
            return data;
        } catch (e) {
            throw e;
        }
    },
    async deleteSize({ commit }, size) {
        try {
            const { data } = await axios.delete(`/api/sizes/${size.id}`);
            commit("DELETE_SIZE_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
