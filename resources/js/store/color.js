import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    colors: []
});

// getters
const getters = {
    colors: state => state.colors
};

// mutations
const mutations = {
    FETCH_COLORS_SUCCESS(state, colors) {
        state.colors = _.sortBy(colors, ["id"]);
    },

    ADD_COLOR_SUCCESS(state, color) {
        state.colors.push(color);
    },

    EDIT_COLOR_SUCCESS(state, updatedColor) {
        const index = state.colors.findIndex(
            color => color.id === updatedColor.id
        );
        if (index !== -1) {
            state.colors.splice(index, 1, updatedColor);
        }
    },

    DELETE_COLOR_SUCCESS(state, color) {
        state.colors = state.colors.filter(t => t.id !== color.id);
    }
};

// actions
const actions = {
    async createColor({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("hex", rawData.hex);
        formData.append("group", rawData.group);
        try {
            const { data } = await axios.post("/api/colors", formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async modifyColor({ commit }, rawData) {
        let formData = new FormData();
        formData.append("name", rawData.name);
        formData.append("hex", rawData.hex);
        formData.append("group", rawData.group);
        formData.append("_method", "PUT");
        try {
            const { data } = await axios.post(`/api/colors/${rawData.id}`, formData);
            return data;
        } catch (e) {
            throw e;
        }
    },
    async fetchColors({ commit }) {
        try {
            const { data } = await axios.get("/api/colors");
            commit("FETCH_COLORS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },
    async deleteColor({ commit }, color) {
        try {
            const { data } = await axios.delete(`/api/colors/${color.id}`);
            commit("DELETE_COLOR_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
