import axios from "axios";
import _ from "lodash";

// state
const state = () => ({
    reviews: []
});

// getters
const getters = {
    reviews: state => state.reviews
};

// mutations
const mutations = {
    FETCH_REVIEWS_SUCCESS(state, reviews) {
        state.reviews = _.sortBy(reviews, ["id"]);
    },

    DELETE_REVIEW_SUCCESS(state, review) {
        state.reviews = state.reviews.filter(t => t.id !== review.id);
    }
};

// actions
const actions = {
    async fetchReviews({ commit }) {
        try {
            const { data } = await axios.get("/api/reviews");
            commit("FETCH_REVIEWS_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    },

    async deleteReview({ commit }, review) {
        try {
            const { data } = await axios.delete(`/api/reviews/${review.id}`);
            commit("DELETE_REVIEW_SUCCESS", data);
        } catch (e) {
            throw e;
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
