import Swal from "sweetalert2";

export function showToast(type, message) {
  Swal.mixin({
    toast: true,
    icon:type,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000
  }).fire({
    title: message
  });
}

export function showDeleteConfirmation(){
  return Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!"
  })
}

export function showEmailConfirmation(){
  return Swal.fire({
    title: "Are you sure?",
    text: "You are going to send an email!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, send it!"
  })
}