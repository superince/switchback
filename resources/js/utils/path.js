const baseUrl = "https://www.switchbacknepal.com";
export function getImgPath(type, imgName) {
    if (baseUrl != "") {
        if (type == "banners") {
            return `${baseUrl}/img/banners/${imgName}`;
        } else if (type == "highlights") {
            return `${baseUrl}/img/highlights/${imgName}`;
        } else if (type == "products") {
            return `${baseUrl}/img/products/${imgName}`;
        } else if (type == "blog") {
            return `${baseUrl}/img/blog/${imgName}`;
        } else if (type == "teams") {
            return `${baseUrl}/img/teams/${imgName}`;
        } else if (type == "profile") {
            return `${baseUrl}/img/profile/${imgName}`;
        } else if (type == "main") {
            return `${baseUrl}/img/${imgName}`;
        } else if (type == "insta") {
            return `${baseUrl}/img/insta/${imgName}`;
        } else if (type == "about") {
            return `${baseUrl}/img/about/${imgName}`;
        } else if (type == "sizecharts") {
            return `${baseUrl}/img/sizecharts/${imgName}`;
        } else if (type == "notices") {
            return `${baseUrl}/img/notices/${imgName}`;
        } else if (type == "dealers") {
            return `${baseUrl}/img/dealers/${imgName}`;
        } else if (type == "video") {
            return `${baseUrl}/vid/${imgName}`;
        }
    } else {
        if (type == "banners") {
            return `/img/banners/${imgName}`;
        } else if (type == "highlights") {
            return `/img/highlights/${imgName}`;
        } else if (type == "products") {
            return `/img/products/${imgName}`;
        } else if (type == "blog") {
            return `/img/blog/${imgName}`;
        } else if (type == "teams") {
            return `/img/teams/${imgName}`;
        } else if (type == "profile") {
            return `/img/profile/${imgName}`;
        } else if (type == "main") {
            return `/img/${imgName}`;
        } else if (type == "insta") {
            return `/img/insta/${imgName}`;
        } else if (type == "about") {
            return `/img/about/${imgName}`;
        } else if (type == "sizecharts") {
            return `/img/sizecharts/${imgName}`;
        } else if (type == "notices") {
            return `/img/notices/${imgName}`;
        } else if (type == "dealers") {
            return `/img/dealers/${imgName}`;
        } else if (type == "video") {
            return `/vid/${imgName}`;
        }
    }
}
