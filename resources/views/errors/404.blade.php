@extends('frontend.layouts.app')
@section('title', '404 Error -')
@section('content')
<div class="container-fluid mt-5 pt-5 pb-5 mb-5">
  <div class="space mt-5 pt-5"></div>
  <h2 class="mt-5 pt-5">404 - PAGE NOT FOUND!</h2>
  <h3>The page you are looking for might be temporarily unavailable, or it may have been changed or deleted</h3>
  <div class="mt-5 mb-5 pb-5">
    <a href="/" class="btn btn-outline-dark d-inline mr-3">GO BACK HOME</a>
  </div>
</div>
@endsection