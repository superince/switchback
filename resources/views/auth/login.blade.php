@extends('layouts.app')
@section('title', 'Login')
@section('content')
<div class="login-body">
    <div class="form-signin pb-0">
        <div class="text-center">
            <img class="mb-4" src="{{url('frontend/images/switchbak-logo.png')}}" alt="">
        </div>
        <div class="form-container">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group row mb-0">
                    <label for="email" class="sr-only col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <label for="password" class="sr-only col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-0 mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn mx-auto btn-outline-secondary btn-sm btn-primary mt-0 pl-4 pr-4">
                            {{ __('LOG IN') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
