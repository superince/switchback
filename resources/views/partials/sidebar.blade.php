   <!-- Main Sidebar Container -->
   <aside class="main-sidebar sidebar-dark-primary elevation-4" style="position: fixed;height:100vh">
       <!-- Brand Logo -->
       <a class="brand-link">
           <img src="/img/swbn.png" width="100%">
       </a>

       <!-- Sidebar -->
       <div class="sidebar">
           <!-- Sidebar user panel (optional) -->
           <div class="user-panel mt-3 pb-3 mb-3 d-flex">
               <div class="image">
                   <img src="/img/profile.png" class="img-circle elevation-2">
               </div>
               <div class="pt-2">
                    <h5 class="white--text">{{Auth::user()->email}}</h5>
               </div>
           </div>
           <!-- Sidebar Menu -->
           <nav class="mt-2">
               <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                   data-accordion="false">
                   <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                   <li class="nav-item">
                       <router-link to="/admin" class="nav-link">
                           <i class="nav-icon fas fa-tachometer-alt"></i>
                           <p>
                               Dashboard
                           </p>
                       </router-link>
                   </li>
                   <li class="nav-item">
                       <router-link to="/admin/banner" class="nav-link">
                           <i class="nav-icon fas fa-sign"></i>
                           <p>
                               Banner
                           </p>
                       </router-link>
                   </li>
                   <li class="nav-item has-treeview">
                       <a href="#" class="nav-link ">
                           <i class="nav-icon fas fa-tachometer-alt"></i>
                           <p>
                               Products Section
                               <i class="right fas fa-angle-left"></i>
                           </p>
                       </a>
                       <ul class="nav nav-treeview">
                           <li class="nav-item">
                               <router-link to="/admin/division" class="nav-link">
                                   <i class="nav-icon fas fa-layer-group"></i>
                                   <p>
                                       Division
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                               <router-link to="/admin/brands" class="nav-link">
                                   <i class="nav-icon fas fa-tag"></i>
                                   <p>
                                       Brands
                                   </p>
                               </router-link>
                           </li>

                           <li class="nav-item">
                               <router-link to="/admin/product-category" class="nav-link">
                                   <i class="nav-icon fas fa-object-group"></i>
                                   <p>
                                       Product Category
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                               <router-link to="/admin/products" class="nav-link">
                                   <i class="nav-icon fab fa-product-hunt"></i>
                                   <p>
                                       Products
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                               <router-link to="/admin/coupon" class="nav-link">
                                   <i class="nav-icon fas fa-ticket-alt"></i>
                                   <p>
                                       Coupon
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                               <router-link to="/admin/color" class="nav-link">
                                   <i class="nav-icon fas fa-fill-drip"></i>
                                   <p>
                                       Color
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                               <router-link to="/admin/size" class="nav-link">
                                   <i class="nav-icon fas fa-ruler"></i>
                                   <p>
                                       Size
                                   </p>
                               </router-link>
                           </li>
                       </ul>
                   </li>
                   <li class="nav-item">
                       <router-link to="/admin/orders" class="nav-link">
                           <i class="nav-icon fas fa-cart-plus"></i>
                           <p>
                               Orders
                           </p>
                       </router-link>
                   </li>
                   <li class="nav-item">
                       <router-link to="/admin/messages" class="nav-link">
                           <i class="nav-icon fas fa-comments"></i>
                           <p>
                               Messages
                           </p>
                       </router-link>
                   </li>
                   <li class="nav-item">
                       <router-link to="/admin/team" class="nav-link">
                           <i class="nav-icon fas fa-user-friends"></i>
                           <p>
                               Team
                           </p>
                       </router-link>
                   </li>

                   <li class="nav-item">
                       <router-link to="/admin/reviews" class="nav-link">
                           <i class="nav-icon fas fa-star"></i>
                           <p>
                               Reviews
                           </p>
                       </router-link>
                   </li>

                   <li class="nav-item">
                       <router-link to="/admin/blogs" class="nav-link">
                           <i class="nav-icon fab fa-blogger"></i>
                           <p>
                               Blogs
                           </p>
                       </router-link>
                   </li>
                   <li class="nav-item">
                       <router-link to="/admin/about" class="nav-link">
                           <i class="nav-icon fas fa-address-card"></i>
                           <p>
                               About
                           </p>
                       </router-link>
                   </li>

                   <li class="nav-item">
                       <router-link to="/admin/subscribers" class="nav-link">
                           <i class="nav-icon fas fa-user-circle"></i>
                           <p>
                               Subscribers
                           </p>
                       </router-link>
                   </li>

                   <li class="nav-item has-treeview">
                       <a href="#" class="nav-link ">
                           <i class="nav-icon fas fa-puzzle-piece"></i>
                           <p>
                               Extras
                               <i class="right fas fa-angle-left"></i>
                           </p>
                       </a>
                       <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to="/admin/dealer" class="nav-link">
                                    <i class="nav-icon fas fa-people-carry"></i>
                                    <p>
                                        Dealers
                                    </p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/admin/pages" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>
                                        Pages
                                    </p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                               <router-link to="/admin/notice" class="nav-link">
                               <i class="nav-icon fas fa-clipboard"></i>

                                   <p>
                                       Notice
                                   </p>
                               </router-link>
                           </li>
                           <li class="nav-item">
                                <router-link to="/admin/settings" class="nav-link">

                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Highlights
                                    </p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/admin/shipping" class="nav-link">
                                    <i class="nav-icon fas fa-shipping-fast"></i>
                                    <p>
                                        Shipping
                                    </p>
                                </router-link>
                            </li>
                       </ul>
                   </li>
                   <li class="nav-item has-treeview">
                       <a href="#" class="nav-link ">
                           <i class="nav-icon fas fa-tachometer-alt"></i>
                           <p>
                               Admin Section
                               <i class="right fas fa-angle-left"></i>
                           </p>
                       </a>
                       <ul class="nav nav-treeview">


                            <li class="nav-item">
                                <router-link to="/admin/developer" class="nav-link">
                                    <i class="nav-icon fas fa-laptop-code"></i>
                                    <p>
                                        Developer Options
                                    </p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to="/admin/users" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Users
                                    </p>
                                </router-link>
                            </li>
                       </ul>
                   </li>
               </ul>
           </nav>
           <!-- /.sidebar-menu -->
       </div>
       <!-- /.sidebar -->
   </aside>
