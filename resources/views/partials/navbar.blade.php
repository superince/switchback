<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form> -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">

            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-bell" style="font-size:22px"></i>
                <span class="badge badge-warning badge-primary" style="float:right;margin-bottom:-10px;">
                <div style="display: none">
                    {{ $total = 0 }}
                </div>
                @foreach(auth()->user()->notifications as $notification)
                <div style="display: none">
                    {{$total =$total + $notification['total']}}
                </div>
                @endforeach
                {{$total}}
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                @foreach(auth()->user()->notifications as $notification)
                        @if($notification['type']=="App\\Notifications\\NewMessage")
                        <a href="/admin/messages" class="dropdown-item">
                            <i class="fas fa-sticky-note mr-2"></i> {{$notification['total']}} new messages.
                        </a>
                        @endif

                        @if($notification['type']=="App\\Notifications\\NewSubscription")
                        <a href="/admin/users" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> {{$notification['total']}} new subscriptions.
                        </a>
                        @endif
                @endforeach

            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user" style="font-size:22px"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="/" target="_blank" class="dropdown-item">
                    <i class="fas fa-expand-arrows-alt mr-2"></i>View website

                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
