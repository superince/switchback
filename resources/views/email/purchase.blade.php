<!DOCTYPE html>
<html>
<head>
    <title>www.switchbacknepal.com</title>
</head>
<body>
    <h3>Hi {{ $details['name'] }}. Thank you for the purchase. Updates of your order status will be emailed to you.</h3>
    <h4>Thank you.</h4>
    <a href="https://switchbacknepal.com" target="_blank" style="text-decoration:none;padding:8px;background-color:red;color:white;">Visit our store</a>
    
    <hr style="margin-top:8px">

    <p>If you have any questions, you can email us at switchbacknepal@gmail.com or call us at +977-9840248539/+977-9840248539</p>
</body>
</html>