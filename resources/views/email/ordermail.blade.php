<!DOCTYPE html>
<html>
<head>
    <title>www.switchbacknepal.com</title>
</head>
<body>
    <p>Hi there! Your order no. {{ $details['order_no'] }} {{ $details['status'] }}</p>
 
    <h1>Order No: {{ $details['order_no'] }}</h1>
    <p>Date: {{ $details['date'] }}</p>
    
    <hr>
    <table>
      <tr>
        <th>Name</th>
        <th>Color</th>
        <th>Qty</th>
        <th>Size</th>
        <th>Price</th>
      </tr>
      @foreach($details['products'] as $prod)
      <tr>
        <td>
          {{$prod->name}}
        </td>
        <td>
          {{$prod->color->name}}
        </td>
        <td>
          {{$prod->qty}}
        </td>
        <td>
          {{$prod->size}}
        </td>
        <td>
          {{$prod->price}}
        </td>
      </tr>
      @endforeach
    </table>
  <hr>
  <h6>Sub Total : {{$details['subtotal']}}</h6>
  <h6>Promo Code discount : {{$details['promo']}}</h6>
  <h6>Total : {{$details['total']}}</h6>


  <h4>Thank you</h4>
</body>
</html>