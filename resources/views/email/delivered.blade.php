<!DOCTYPE html>
<html>
<head>
    <title>www.switchbacknepal.com</title>
</head>
<body>
    <h2>Your order has been delivered!!</h2>
    <h3>ORDER NO : {{ $details['order_no'] }}</h3>
    <p>Thank you for shopping with Switchback.</p>

    <a href="https://switchbacknepal.com" target="_blank" style="text-decoration:none;padding:8px;background-color:red;color:white;">Visit our store</a>
    
    <hr style="margin-top:8px">

    <p>If you have any questions, you can email us at switchbacknepal@gmail.com or call us at +977-9840248539/+977-9840248539</p>
</body>
</html>