<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>SwitchBackNepal - Backend</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/custom.css') }}">
    <style>
      .v-application ul{
        padding-left:0 !important
      }
      ::-webkit-scrollbar {
        width: 6px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
        background: #f1f1f1;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: #888;
      }

      /* Handle on hover */
      ::-webkit-scrollbar-thumb:hover {
        background: #555;
      }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div id="app" class="wrapper">
        <v-app>
            @include('partials.navbar')
            @include('partials.sidebar')
            <v-main>
                <div class="content-wrapper">
                    <div class="content">
                        <div class="container-fluid pt-2">
                            <router-view></router-view>
                        </div>
                    </div>
                </div>
            </v-main>
        </v-app>
    </div>
    <!-- REQUIRED SCRIPTS -->
    <script src="/js/app.js"></script>
    <style>
      .v-dialog{
        box-shadow: none;
      }
    </style>
</body>

</html>
