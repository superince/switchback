<div class="container-fluid mb-0">
  <div class="row row-cols-1 row-cols-md-3">
    <div class="col mb-4 pb-3">
      <div class="card pl-5 pr-5 pt-4 pb-2">
        <h4 class="pb-1 mb-0">Switchback Nepal</h4>
        <address>
          Phone: +9779840248539<br>
          E-mail: info@switchbacknepal.com<br>
          Lalitpur 44600, Nepal<br> 
          <br>
        </address>
      </div>
    </div> <!-- /col mb-4 pb-3/ repeat this for multiple address box -->
  </div>
</div>
<div class="container-fluid mb-5">
      <div class="cart-col-inner pt-4 pb-4">
        <h4 class="pl-5 pb-3">Contact Us</h4>
        <div class="outer-container pb-4">
          <form action="{{url('postcontact')}}" method="post" class="needs-validation" novalidate id="contactform">
             {{csrf_field()}}
            <div class="row">
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="firstName" placeholder="First Name" name="fname" required maxlength="50" minlength="3">
              </div>
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lname" required maxlength="50" minlength="3">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <input type="email" class="form-control" id="email" placeholder="your@email" name="email" required>
              </div>

              <div class="col-md-6 mb-3">
                <input type="number" class="form-control" id="address" placeholder="Phone number" required name="contact_no" maxlength="10" minlength="5">
              </div>
            </div>

            <div class="row">
              <div class="col mb-3 mt-2">
                <textarea  placeholder="Message" class="form-control" id="message" rows="5" name="message" required maxlength="191"></textarea>
              </div>
            </div>

            <button class="btn btn-primary btn-outline-secondary btn-lg mt-3" type="submit">SUBMIT</button>
          </form>
        </div>
      </div>
    </div> <!-- /.container-fluid -->

