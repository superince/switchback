@if(Session::has('notify'))
    <div class="alert alert-success alert-md">
        <button type="button" class="close" data-dismiss="alert"><span class="icon-x"></span><span class="sr-only">Close</span></button>
        <h5 class="modal-title"><span class="text-semibold">{{ Session::get('notify') }}</span></h5>
    </div>
@endif
