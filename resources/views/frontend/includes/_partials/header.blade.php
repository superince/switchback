<!-- <div class="alert alert-success alert-sm">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span class="icon-x"></span>
  </button>
  <h5 class="mb-0">THANK YOU FOR SUBSCRIBING.</h5>
</div> -->

<header>
  <div class="search-overlay" id="search-overlay">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span class="icon-x"></span>
    </button>
    <div class="container">
      <form action="" class="form-inline my-2 my-lg-0">
        <div class="search-input transparent-search">
          <input class="form-control" id="search-products" type="text" placeholder="Search.." aria-label="Search">
          <a href="shop-page.html" class="btn btn-secondary my-2 my-sm-0" type="submit"><span class="icon-search"></span></a>
        </div>
      </form>
    </div>
    <div class="container" style="padding-top: 30px;">
      <div class="row" id="search-result">
      </div>
    </div>
  </div>
  <nav id="navbar" class="navbar fixed-top navbar-expand-lg navbar-dark transparent-bg enable-bg {{ Request::is('/') ? 'home-nav' : '' }}">
    <div class="container-fluid nav-container-fluid">
      <div class="top-menu-wrap">
        <div class="topmenu">
          <ul>
            <li><a href="/about">ABOUT US</a></li>
            <li><a href="/contact">CONTACT US</a></li>
          </ul>
        </div>
      </div>
      <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{url('frontend/images/logo_white_horizontal.png')}}" alt="" class="logo-lg" style="margin-top: 26px;">
        <img src="{{url('frontend/images/logo_white_horizontal.png')}}" alt="" class="logo-sm">
      </a>
      <div class="sm-quick-menu">
        <div class="quick-item">
          <div class="pr-0 search-input transparent-search" id="search-trigger-sm">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit"><span class="icon-search"></span></button>
          </div>
        </div>
        <div class="quick-item">
          <div class="cart-wrapper">
            <span class="ml-4 ml-lg-3 cart-icon cart-toggle"><span class="icon-cart"></span> <span class="badge badge-light">{{ count($cart_products) }}</span></span>
          </div> <!-- /.cart-wrapper -->
        </div>
      </div>
      <div class="cart-overlay cart-overlay-sm">
        @include('frontend.includes._partials.cart')
      </div> <!-- /.cart-overlay -->

      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-list"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        @include('frontend.includes._partials.messages')
        <ul class="navbar-nav ml-auto" id="navabarul">
          @foreach ($divisions as $divdata)
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="{{url(strtolower($divdata->name))}}">{{strtoupper($divdata->name)}}</a>
            <span class="sm-dropdown-toggle d-lg-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon-chevron-down"></span>
            </span>

            <div class="dropdown-menu full-width-dropdown clearfix" aria-labelledby="dropdown01">
              @if($divdata->highlight)
              <div class="catimg" style="background-image: url({{url('/img/highlights',$divdata->highlight)}});">
                <img src="{{url('highlight',$divdata->highlight)}}" alt="" class="img-fluid">
              </div>
              @else
              <div class="catimg" style="background-image: url({{url('frontend/images/menu-cat-img.jpg')}});">
                <img src="{{url('frontend/images/menu-cat-img.jpg')}}" alt="" class="img-fluid">
              </div>
              @endif
              <ul>
              @foreach ($divdata->categories as $cat)
              <li><a class="dropdown-item" href="{{url(strtolower($divdata->name),strtolower($cat))}}">{{strtoupper($cat)}}</a></li>
              @endforeach
              </ul>
            </div>
          </li>
          @endforeach
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BRANDS</a>
            <span class="sm-dropdown-toggle d-lg-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon-chevron-down"></span>
            </span>
            
            <div class="dropdown-menu full-width-dropdown clearfix" aria-labelledby="dropdown01" id="brand-dropdown">
              <div class="catimg default-cat-img" style="background-image: url({{url('frontend/images/menu-cat-img.jpg')}});">
                <img src="{{url('frontend/images/menu-cat-img.jpg')}}"class="img-fluid">
              </div>

              <div class="catimg catimg1" style="background-image: url({{url('frontend/images/switchbak-logo.png')}});">
                <img src="{{url('frontend/images/menu-cat-img.jpg')}}"class="img-fluid">
              </div>
              <div class="catimg catimg2" style="background-image: url({{url('frontend/images/single-img.jpg')}});">
                <img src="{{url('frontend/images/menu-cat-img.jpg')}}"class="img-fluid">
              </div>

              <ul>
                @foreach($brands as $brand)
                <li><a class="dropdown-item" href="{{ route('brand-shop', strtolower($brand->name))}}">{{strtoupper($brand->name)}}</a></li>
                @endforeach
              </ul>
            </div>

          </li>
          <li class="nav-item">
            <a class="nav-link" href="/blog">BLOGS</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0 d-none d-lg-inline">
          <div class="search-input transparent-search" id="search-trigger">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit"><span class="icon-search"></span></button>
          </div>
        </form>
        <div class="cart-wrapper d-none d-lg-inline">
          <span class="ml-4 ml-lg-3 cart-icon cart-toggle"><span class="icon-cart"></span> <span class="badge badge-light cart-product-count">{{ count($cart_products) }}</span></span>
          <div class="cart-overlay header-cart-wrapper">
            @include('frontend.includes._partials.cart')
          </div> <!-- /.cart-overlay -->
        </div> <!-- /.cart-wrapper -->

      </div>
    </div>
  </nav>
</header>