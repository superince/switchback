<div class="cart-col-inner">
  <h4>
    MY CART 
    <button type="button" class="close cart-close" data-dismiss="modal" aria-label="Close" onclick="cartclose();">
      <span class="icon-x"></span>
    </button>
  </h4>
  <div class="clearfix"></div>
  
  @foreach($cart_products as $key => $cart_product)
  <div class="product-row clearfix cart-product-row">
    <div class="product-image">
      <img src="{{ asset('img/products/'.$cart_product->photos[0]->photo)}}">
    </div>
    <div class="product-name">
      <div class="product-cat">{{ $cart_product->brand->name }}</div> 
      <div class="product-name-text">{{ $cart_product->name }}</div>
      <div class="product-color d-inline"><span class="d-inline">Color: </span> <span class="p-color red d-inline-block" style="background: {{ $cart_product->cart_color->hex }}"></span></div>
      <div class="product-size d-inline"><span class="">Size: </span>{{ $cart_product->cart_size }}</div>
      <div class="product-quantity d-inline"><span class="">Quantity: </span>{{ $cart_product->cart_quantity }}</div>
    </div>
    <div class="product-price text-left">
      @if($cart_product->sale==1)
        <div class="product-price-txt">Rs. {{ ($cart_product->price - $cart_product->sale_price/100*$cart_product->price) * $cart_product->cart_quantity }}</div>
      @else
        <div class="product-price-txt">Rs. {{ $cart_product->price * $cart_product->cart_quantity }}</div>
      @endif
    
      <button class="btn p-0" onclick="removeFromCart({{$key}})">REMOVE</button>
    </div>
  </div>
  @endforeach

  <div class="product-row clearfix sub-total-row">
    <div class="sub-total-txt">SUBTOTAL</div>
    <div class="sub-total">Rs. {{ $cart_subtotal }}</div>
  </div>
  <div class="checkout-row">
    <a href="{{ route('cart.empty-cart') }}" style="padding-top: 1.5em;padding-right: 1.2em;">Empty Cart</a>
    <a href="/cart" class="btn pl-4 pr-4 btn-sm btn-primary btn-outline-secondary checkout-btn" type="button" id="button-addon2">PROCEED TO CHECKOUT</a>
  </div>
</div>