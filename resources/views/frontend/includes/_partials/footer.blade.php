<div class="callout-outer bg-blue">
  <div class="callout diagonal-bg-outer container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-6 callout-col">
        <div class="row align-items-center">
          <div class="col-12 mb-3">
            <h5>
            @foreach($static_content as $content)
              @if($content->key=='book_my_ride')
                {{$content->value}} <br>
              @endif
            @endforeach
            </h5>
          </div>
          <div class="col-12 text-center">
            <a class="btn btn-primary hover-animate" href="/about/#blogs-rides" role="button">BOOK MY RIDE <span class="icon-dot-right"></span></a>
          </div>
        </div>
      </div>
      <div class="col-lg-6 callout-col bg-blue">
        <div class="row align-items-center">
          <div class="col-12 mb-3">
            <h5 class="pl-lg-4">
            @foreach($static_content as $content)
              @if($content->key=='survey_text')
                {{$content->value}} <br>
              @endif
            @endforeach
            </h5>
          </div>
          <div class="col-12 text-center">
          @foreach($static_content as $content)
            @if($content->key=='survey_link')
              <a class="btn btn-primary hover-animate" href="{{$content->value}}" target="_blank" role="button">PARTICIPATE <span class="icon-dot-right"></span></a>
            @endif
          @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-2 col-lg-3 col-md-4">
          <h3><a href="/contact">CONTACT US</a></h3>
          <p>
          @foreach($static_content as $content)
            @if($content->key=='contactno')
              {{$content->value}} <br>
            @endif
          @endforeach

          @foreach($static_content as $content)
            @if($content->key=='secondarycontactno')
              {{$content->value}} <br>
            @endif
          @endforeach

          @foreach($static_content as $content)
            @if($content->key=='email')
              {{$content->value}}
            @endif
          @endforeach
          <br>
          <a href="https://www.google.com/maps/place/Switchback+Apparels/@27.6829151,85.3042639,17z/data=!3m1!4b1!4m5!3m4!1s0x39eb1849c86d2277:0xb2fc4e8dabfef94!8m2!3d27.6829104!4d85.3064526" target="_blank">Get Directions</a>
          </p>
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4">
          <h3><a href="/about">ABOUT US</a></h3>
          <p>
            <a href="/about/#story">Our Story</a> <br>
            <a href="/about/#teams">Our Team</a> <br>
            <a href="/about/#blogs">Our Blogs</a> <br>
            
          </p>
        </div>
        <div class="col-xl-1 col-lg-3 col-md-4">
          <h3><a href="/service">SERVICE</a></h3>
          <p>
          @foreach ($pages->take(3) as $page)
            <a href="/service/#{{strtolower(str_replace(' ','', $page->title))}}">{{$page->title}}</a><br>
          @endforeach

        </div>
        <div class="col-xl-2 col-lg-3 col-md-4">
          <h3>&nbsp;</h3>
          <p>
          @foreach ($pages->skip(3) as $page)
            <a href='/service/#{{strtolower(str_replace(' ','', $page->title))}}'>{{$page->title}}</a><br>
          @endforeach
        </div>
        <div class="col-xl-2 col-lg-3 col-md-4">
          <p class="social mt-md-5"><a href="https://www.facebook.com/switchbacknp/" target="_blank"><span class="icon-facebook"></span></a>
            <a href="https://www.instagram.com/switchback.nepal" target="_blank"><span class="icon-instagram"></span></a>
            <a href="https://www.youtube.com/channel/UCIhOO24_7Q4PLtVqsS8yW2w" target="_blank"><span class="icon-youtube"></span></a>
          </p>
        </div>
        <div class="col-xl-3 pt-3 col-lg-3 col-md-4">
          <p class="mb-2">Subscribe to our Newsletter</p>
        <form action="{{url('newsubscribe')}}" method="post">
           {{csrf_field()}}
          <div class="input-group mb-3 subscribe-form">
            <input type="text" class="form-control" placeholder="Email" aria-label="email" name="email" aria-describedby="button-addon2">
            <div class="input-group-append">
              <button class="btn btn-primary hover-animate" type="submit" id="button-addon2">SUBSCRIBE</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </footer>
