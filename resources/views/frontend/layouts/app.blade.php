<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    @yield('meta')
    <title>@yield('title') SwitchBackNepal </title>

    <!-- <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/"> -->

    <!-- Bootstrap core CSS -->
    <style>
    #loadlogo{
      opacity: 0;
      display: none;
      position: fixed;
      top: -50px;
      left: 0%;
      width: 100%;
      height: 100vh;
      z-index: 9999999;
      background-image: url({{url('frontend/images/logo_white_horizontal.png')}});
      background-repeat: no-repeat;
      background-position: center;
    }
    </style>
  <link  href="{{url('frontend/assets/icomoon-v1.0/style.css')}}" rel="stylesheet">
  <link href="{{url('frontend/js/bootstrap-slider/dist/css/bootstrap-slider.min.css')}}" rel="stylesheet">
	<link href="{{url('frontend/dist/css/styles.css')}}" rel="stylesheet" >
  <link href="{{ asset('frontend/dist/css/custom.css') }}" rel="stylesheet" type="text/css" >
  <link href="{{ asset('frontend/css-hack.css') }}" rel="stylesheet" type="text/css" >
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    @yield('after-styles')
  </head>
  <body>
    <div id="loadlogo"></div>
    <div id="body-load">
    </div>
    <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="318400388493203"
  theme_color="#0A7CFF">
      </div>
    @include('frontend.includes._partials.header')
      @include('frontend.includes._partials.alert')
      @yield('content')
    @include('frontend.includes._partials.footer')
@yield('after-scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ENTGWXRNH5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ENTGWXRNH5');
</script>
<script>
  $(document).ready(function(){
    $('.owl-carousel-product').owlCarousel({
      dots: false,
      margin: 38,
      responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        },
        1400:{
            items:5,
            nav:true,
            loop:false
        }
      }
    });
    $('.owl-carousel-blog').owlCarousel({
      dots: false,
      margin: 38,
      responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        },
        1400:{
            items:3,
            nav:true,
            loop:false
        }
      }
    });
  });
</script>
<script type="text/javascript">
$('body').on('keyup','#search-products',function(){
  const searchQuery=$(this).val();
  if(searchQuery!=''){
    $.ajax({
      method:'POST',
      url:'{{route("search-products")}}',
      dataType:'json',
      data:{
        '_token':'{{csrf_token()}}',
        searchQuery:searchQuery
      },
      success:function(res){
        $("#search-result").empty();
        res.forEach(function(product,index){
          let carouselItems=[];
          let colorItems=[];
          product.variants.forEach(function(variant,i){
             let carousel=$('<div class="carousel-item " style="background-image:url({{{'/img/products/'}}}'+variant.photos[0].photo+');"><img src="{{url("frontend/images/square-img-temp.jpg")}}" class="d-block w-100" alt="..."></div>')
             carouselItems.push(carousel);
             let color=$('<span class="color red" id="new-arrival-product" data-target="#searchproduct-'+index+'" data-slide-to="'+(i+1)+'" style="background:'+variant.color.hex+';"></span>');
             colorItems.push(color);
          })

          let item=$('<div class="col-6 col-md-4 col-lg-3 pb-4" ><div class="card search-card"><div id="searchproduct-'+index+'" class="productpreview carousel slide" data-ride="carousel" data-interval="false"><a href="/products/'+product.slug+'")}}"><div class="carousel-inner-'+index+'"> <div class="carousel-item active " style="background-image:url({{{'/img/products/'}}}'+product.photos[0].photo+');"><img src="{{url("frontend/images/square-img-temp.jpg")}}" class="d-block w-100" alt="..."></div></div></a></div><div class="card-body"><a href="{{url("products",'+product.slug+')}}"><h5 class="card-title">'+product.brand.name+'</h5><p class="card-text">'+product.name+'</p><div class="price">Rs. '+product.price+'</div></a> <div class="color-option carousel-indicators mt-1 indi-'+index+'"><span class="color red active" data-target="#searchproduct-'+index+'" data-slide-to="0" id="new-arrival-product" style="background:'+product.color.hex+'"></span></div></div></div></div>');

          $("#search-result").append(item);
          $(".carousel-inner-"+index).append(carouselItems);
          $(".indi-"+index).append(colorItems);
          $('.color-option.carousel-indicators span').on('mouseover',function(){
            $(this).trigger('click');
            $('.color-option.carousel-indicators span').removeClass('active');
            $(this).addClass('active');
          });
        })
      }
    })

  }else{
    $("#search-result").empty();
  }
})
</script>
</body>
</html>
