@extends('frontend.layouts.app')
@section('content')
    <!-- @include('frontend.includes._partials.cart')     -->
  <div class="banner-inner-page" style="background-image:url({{url('frontend/images/jump-power-motorbike.jpg')}});">
      <img src="{{url('frontend/images/jump-power-motorbike.jpg')}}" alt="Welcome to Switchback">
    </div>
    <div class="container-fluid breadcrumb-container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">{{strtoupper($getcat->name)}}</a></li>
        </ol>
      </nav>
    </div> <!-- /.container-fluid breadcrumb-container-->
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 sidebar mb-5">
          <h2>Category</h2>
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            
            @foreach ($categorydata as $category)
            <a class="nav-link {{$loop->first?'active':'' }}" href="#">{{$category->name}}</a>
            @endforeach
          </div>
          <h3>SIZE</h3>
          <div class="size-option">

            @foreach ($sizedata as $size)
                
            <div class="form-check form-check-inline">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="size1">
                <label class="custom-control-label" for="size1">{{$size->name}}</label>
              </div>
            </div>
            @endforeach



          </div>
          <h3>COLOR</h3>
          <div class="color-option">
          
            @foreach ($colordata as $color)
            <div class="form-check form-check-inline">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="red">
                <label class="custom-control-label {{strtolower($color->name)}}-color" for="red"></label>
              </div>
            </div>
              
            @endforeach
            
          </div>
          <h3>PRICE</h3>
          <div class="price-option mb-2">
            <input id="price-slider" type="text" class="span2" value="" data-slider-min="0" data-slider-max="100000" data-slider-step="5" data-slider-value="[100,30000]"/> 
          </div>
          <h3>BRANDS</h3>

          <div class="brand-option">

          @foreach ($branddata as $brand)
            <div class="form-check form-check-inline">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="{{$brand->name}}">
                <label class="custom-control-label" for="switchback">{{$brand->name}}</label>
              </div>
            </div>
          @endforeach
          

          </div>

          <!-- <button type="button" class="btn btn-sm btn-outline-secondary">CLEAR FILTER</button>
          <button type="button" class="btn btn-sm btn-primary btn-outline-secondary">APPLY FILTERS</button> -->
        </div><!-- /.sidebar -->
        <div class="col-md-10 product-list-col">
          <div class="container">
            <div class="row">
            
            @foreach ($getproduct as $product)
                
              <div class="col-md-3">
                <div class="card tagnew">
                  <a href="{{url('product',$product->name)}}">
                   @if(!empty($product->photos[0]))
                      <img src="{{url('img/products/'.$product->photos[0]->photo)}}" class="card-img" alt="{{$product->name}}" >
                    @else
                      <img src="{{url('frontend/images/ski-goggles.png')}}" alt="{{$product->name}}" class="img-fluid">
                    @endif
                    <div class="card-body">
                      <h5 class="card-title">{{strtoupper($product->brand->name)}}</h5>
                      <p class="card-text">{{strtoupper($product->name)}}</p>
                      <div class="price">Rs. {{$product->price}}</div>
                      <div class="color-option">
                        <span class="color red active"></span>
                        <span class="color gray"></span>
                        <span class="color green"></span>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            @endforeach


            </div>
            <div class="row">
              <div class="col-12">
                <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end">
                                    {{$getproduct->render()}}
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid card-owl-carousel related-products-container">
              
      <h3>RELATED PRODUCTS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
          @foreach ($relatedproductdata as $product)
        <div> 
          <div class="card tagnew">
              <span class="card-img-top-wrap">
                <a href="{{url('product',$product->name)}}">
              @if(!empty($product->photos[0]))
                <img src="{{url('img/products/'.$product->photos[0]->photo)}}" class="card-img" alt="{{$product->name}}">
              @else
                <img src="{{url('frontend/images/ski-goggles.png')}}" alt="{{$product->name}}" class="img-fluid">
              @endif
                </a>
              </span>
              <div class="card-body">
                <a href="{{url('product',$product->name)}}">
                <h5 class="card-title">{{strtoupper($product->brand->name)}}</h5>
                <p class="card-text">{{strtoupper($product->name)}}</p>
                <div class="price">Rs. {{$product->price}}</div>
                </a>
                <div class="color-option">
                  <span class="color red active"></span>
                  <span class="color gray"></span>
                  <span class="color green"></span>
                </div>
              </div>
          </div> <!-- /.card -->
        </div>
        @endforeach
      </div>
    </div> <!-- /.new-arrival-container -->
@endsection

@section('after-scripts')                    
<script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>
<script type="text/javascript">
    // var feed = new Instafeed({
    //   accessToken: '666059216856850'
    // });
    // feed.run();
    var slider = new Slider('#price-slider', {});
</script>

@endsection
