@extends('frontend.layouts.app')
@section('title', 'Contact -')
@section('content')
  <div class="map-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3286.4301587503323!2d85.3043800775434!3d27.68299615294323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1849c86d2277%3A0xb2fc4e8dabfef94!2sSwitchback%20Nepal!5e0!3m2!1sen!2snp!4v1609963150783!5m2!1sen!2snp"frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
  <div data-aos="fade-up" data-aos-delay="200" data-aos-once="true" class="mt-5">
    @include('frontend.includes._partials.contactform')
  </div>
    @endsection

    @section('after-scripts')
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        <script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
        <script src="{{url('frontend/js/jquery-parallax.js/parallax.min.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
        <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('frontend/dist/js/all.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" type="text/javascript"></script>
        <script>
          AOS.init({duration: 800});
          $("#contactform").validate();
        </script>
    @endsection
