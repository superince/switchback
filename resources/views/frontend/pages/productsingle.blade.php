@extends('frontend.layouts.app')
@section('title', 'Product -')
@section('content')
<style>
iframe{
  width:100%;
  height:350px;
}
</style>
    <div class="product-single-wrapper pb-5">
      <div class="container-fluid breadcrumb-container" data-aos="fade-up" data-aos-delay="0" data-aos-once="true" data-aos-duration="500">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/{{strtolower($productdata->divisions[0]->name)}}">{{$productdata->divisions[0]->name}}</a></li>
            <li class="breadcrumb-item"><a href="/{{strtolower($productdata->divisions[0]->name)}}/{{strtolower($productdata->productcategory->name)}}">{{$productdata->productcategory->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$productdata->brand->name}}</li>
          </ol>
        </nav>
      </div> <!-- /.container-fluid breadcrumb-container-->
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 pt-3">
            <div class="single-product-carousel-container sticky-top">
              <div id="single-product-carousel" class="cur-color-default carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  @foreach ($productdata->photos as $photo)
                  <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image:url({{url('img/products/'.$photo->photo)}});">
                    <a href="#" class="singleimg" type="button" data-toggle="modal" data-target="#productzoom">
                      <img src="{{url('frontend/images/single-carousel-top-img-size.jpg')}}" class="d-block w-100" alt="...">
                    </a>
                  </div>
                  @endforeach
                  <a class="carousel-control-prev" href="#single-product-carousel" role="button" data-slide="prev">
                    <span class="icon-dot-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#single-product-carousel" role="button" data-slide="next">
                    <span class="icon-dot-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

                <div class="thumb-container">
                  <div class="row carousel-indicators justify-content-start pt-2">
                  @foreach ($productdata->photos as $photo)
                    <div class="p-2 col-4 thumb {{ $loop->first ? 'active' : '' }}">
                      <div class="thumb-bg" style="background-image: url('{{url('img/products/'.$photo->photo)}}');" data-target="#single-product-carousel" data-slide-to="{{$loop->index}}">
                        <img src="{{url('frontend/images/square-img-temp.jpg')}}" alt="" class="img-fluid">
                      </div>
                    </div>
                  @endforeach
                  </div>
                </div>
              </div>

                @foreach($productdata->variants as $variant)
                  <div id="single-product-carousel{{$loop->index+1}}" class="cur-color-{{str_replace(' ', '', strtolower($variant->color->name))}} carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                    @foreach ($variant->photos as $photo)
                      <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image:url({{url('img/products/'.$photo->photo)}});">
                        <a href="#" class="singleimg" type="button" data-toggle="modal" data-target="#productzoom">
                          <img src="{{url('frontend/images/single-carousel-top-img-size.jpg')}}" class="d-block w-100" alt="...">
                        </a>
                      </div>
                    @endforeach
                      <a class="carousel-control-prev" href="#single-product-carousel" role="button" data-slide="prev">
                        <span class="icon-dot-left"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#single-product-carousel" role="button" data-slide="next">
                        <span class="icon-dot-right"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                    <div class="thumb-container">
                      <div class="row carousel-indicators justify-content-start">
                        @foreach ($variant->photos as $photo)
                        <div class="p-2 col-4 thumb {{ $loop->first ? 'active' : '' }}">
                          <div class="thumb-bg" style="background-image: url('{{url('img/products/'.$photo->photo)}}');" data-target="#single-product-carousel" data-slide-to="{{$loop->index}}">
                            <img src="{{url('img/products/'.$photo->photo)}}" alt="" class="img-fluid">
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                @endforeach
            </div>
            <!-- Modal -->
            <div class="modal fade" id="productzoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-x"></span>
                  </button>
                  <div class="modal-body">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                      @foreach ($productdata->photos as $photo)
                          <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="{{ $loop->first ? 'active' : '' }}" style="background-image:url({{url('img/products/'.$photo->photo)}})"></li>
                        @endforeach
                      </ol>
                      <div class="carousel-inner">
                      @foreach ($productdata->photos as $photo)
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="background-image:url({{url('img/products/'.$photo->photo)}})">
                          <img src="{{url('img/products/'.$photo->photo)}}" class="d-block h-100" alt="...">
                        </div>
                      @endforeach
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="icon-dot-left"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="icon-dot-right"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- /.features -->
          <div class="col-md-6 pt-2 product-detail-col" data-aos="fade-left" data-aos-delay="1000" data-aos-once="true">
            <h4>{{$productdata->brand->name}}</h4>
            <h3 id="productname">{{strtoupper($productdata->name)}}</h3>
            @if($productdata->sale==1)
              <div class="price"><span style="text-decoration:line-through;font-style:italic">Rs.{{$productdata->price}}</span>  Rs.{{$productdata->price - $productdata->sale_price/100 * $productdata->price }}</div>
            @else
              <div class="price">Rs. {{$productdata->price}} </div>
            @endif
            <div class="prduct-dis pb-4 mb-0">
             {{ucfirst($productdata->short_description)}}
            </div>

            {{ Form::open(['route' => 'cart.add-to-cart-show', 'id' => 'add-to-cart-show']) }}
            <div class="color-option mt-1 mb-3" id="colorOption">
              <h5 class="d-inline align-top">COLOR</h5>
              <br>
              <div class="custom-control custom-radio custom-control-inline color-img-radio">
                <div class="color-image-thumb" style="background-image: url({{url('img/products/'.$photo->photo)}});">
                </div>

                <input type="radio" id="default" name="color" class="custom-control-input" value="{{ $productdata->color->id }}" checked required>

                <label class="custom-control-label {{str_replace(' ', '', strtolower($productdata->color->name))}}-color rm-color" for="default">
                  <span style="background-color: {{strtolower($productdata->color->hex)}}; margin-left: -6px;"></span>
                </label>
              </div>

              @foreach ($productdata->variants as $variant)
              <div class="custom-control custom-radio custom-control-inline color-img-radio">
                @if(count($variant->photos))
                <div class="color-image-thumb" style="background-image: url({{url('img/products/'.$variant->photos[0]->photo)}});">
                </div>
                @endif
                <input type="radio" id="{{str_replace(' ', '', strtolower($variant->color->name))}}" name="color" class="custom-control-input" value="{{ $variant->color->id }}">
                <label class="custom-control-label {{str_replace(' ', '', strtolower($variant->color->name))}}-color rm-color" for="{{str_replace(' ', '', strtolower($variant->color->name))}}">
                  <span style="background-color: {{strtolower($variant->color->hex)}}; margin-left: -6px;"></span>
                </label>
              </div>
              @endforeach
            </div> <!-- /.color-option -->

            <h5 class="pt-3">PLEASE SELECT SIZE  </h5>

            <!-- size-option-default will be shown other will be hidden -->
            <div class="size-option size-option-default mt-1">
              @foreach($productdata->sizes as $size)
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="size-{{$size->name}}" name="size" class="custom-control-input" stock="{{$size->pivot->stock}}" value="{{ $size->name }}" required
                  @if($loop->first)
                    checked
                  @endif
                >
                <label class="custom-control-label" for="size-{{$size->name}}">{{$size->name}}</label>
              </div>
              @endforeach
            </div> <!-- /.size-option -->
            
            @foreach($productdata->variants as $variant)
              <div class="size-option size-option-{{strtolower($variant->color->name)}} mt-1">
                @foreach($variant->sizes as $size)
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="{{$size->name}}" name="size" class="custom-control-input" stock="{{$size->pivot->stock}}" value="{{ $size->name }}" required
                  @if($loop->first)
                    checked
                  @endif
                  >
                  <label class="custom-control-label" for="{{$size->name}}">{{$size->name}}</label>
                </div>
                @endforeach
              </div>
            @endforeach

            <p id="showStock"></p>

            {{ Form::hidden('stock', $size->pivot->stock, ['id'=> 'hiddenStock']) }}

            <div class="quantity mb-2">
              QUANTITY <br>
              <div class="quantity-input-wrap ml-0 mt-2">
                <div class="quantity-input input-group inline-group">
                  <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary btn-minus" type="button">
                      <span class="icon-minus"></span>
                    </button>
                  </div>
                  <input class="form-control quantity form-quantity" min="1" name="quantity" value="1" type="number">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary btn-plus" type="button">
                      <span class="icon-plus"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            @if($productdata->sizes && $productdata->sizes[0] && $productdata->sizes[0]->group->sizechart)
            <div>
              <div style="margin-top: 20px;" class="btn-group">
              <button class="btn-md btn-outline-secondary btn mr-3" type="button" data-toggle="modal" data-target="#exampleModal">
                SIZE CHART
              </button>
              </div>
            </div>
            @endif
            <div style="margin-top: 20px;" class="btn-group">
              <button type="submit" class="btn btn-primary btn-outline-secondary hover-animate add-to-cart-btn">ADD TO CART <span class="icon-dot-right"></span></button>
              <span id="loading" style="display: none;">Loading...</span>
            </div>

            {{ Form::hidden('product_id', $productdata->id) }}
            {{ Form::close() }}

            <div class="features mt-5 pt-3">
              <h4>FEATURES</h4>
              <ul class="pl-0 pt-3">
                {!!$productdata->features!!}
              </ul>
            </div>

          </div>
        </div>
      </div>
    </div> <!-- /.product-single-wrapper -->
    <div class="container-fluid review-box pb-0">
      <div class="cart-col-inner pt-4 mb-0">
        <a class="review-btn d-inline-block w-auto border-dark btn btn-primary btn-md pt-2 pb-2 pl-4 pr-4 float-right mr-5" data-toggle="collapse" href="#review-container" role="button" aria-expanded="false" aria-controls="review-container">WRITE A REVIEW</a>
        @if(count($errors))
        <div class="outer-container form-style2 pb-4 pl-5 pr-5" id="review-container">
        <script>
          window.onload = function()
          {
            document.getElementById('review-container').scrollIntoView();
          };
        </script>
        @else
        <div class="outer-container form-style2 pb-4 pl-5 pr-5 collapse" id="review-container">
        @endif

          <h4 class="pl-0 pb-3 mb-0">
            WRITE A REVIEW
          </h4>
          <form action="{{url('review',[$productdata->id])}}" method="post" class="needs-validation" novalidate>
            @csrf
            <div class="form-row">
              <div class="col-12 mb-3">
                <div><span class="text-danger">*</span> Score</div>
                <input type="text" class="form-control score" value="0" required name="score" hidden>
                <div class="star-box">
                  <span class="star star1" id="1"></span>
                  <span class="star star2" id="2"></span>
                  <span class="star star3" id="3"></span>
                  <span class="star star4" id="4"></span>
                  <span class="star star5" id="5"></span>
                </div><!-- /.star-box -->
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-row">
                  <div class="col-12 mb-3">
                    <label for="validationDefault01"><span class="text-danger">*</span> Title</label>
                    <input type="text" class="form-control" id="validationDefault01" value="" required name="title">
                    <span style="color:red">{{$errors->first('title')}}</span>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-12 mb-3">
                    <label for="validationDefault01"><span class="text-danger">*</span> Name</label>
                    <input type="text" class="form-control" id="validationDefault01" value="" required name="name">
                    <span style="color:red">{{$errors->first('name')}}</span>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-12 mb-3">
                    <label for="validationDefault01"><span class="text-danger">*</span> Email</label>
                    <input type="text" class="form-control" id="validationDefault01" value="" required name="email">
                    <span style="color:red">{{$errors->first('email')}}</span>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-row">
                  <div class="col-12 mb-7">
                    <label for="validationDefault01"><span class="text-danger">*</span> Review</label>
                    <textarea  placeholder="Message" class="form-control" id="message" rows="8" name="review" style="height: 13.1em;" required ></textarea>
                    <span style="color:red">{{$errors->first('review')}}</span>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-12 mb-7 text-right">
                    <button class="btn btn-primary btn-outline-secondary btn-lg mt-3" type="submit">POST</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="review-row pt-3">
            <div class="text border-top"></div>
          </div>
        </div>
      </div>
    </div> <!-- /.container-fluid -->

    <div class="container-fluid review-box mb-5">
      <div id="review-list">
      </div>
    </div>

    <div class="container-fluid card-owl-carousel related-products-container mb-5 pb-5">
      <h3>RELATED PRODUCTS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
       @foreach ($relatedproduct as $related)
       <div data-aos="fade-up" data-aos-delay="200" data-aos-offset="100" class="h-100">
          <div class="h-100 card @if(Carbon\Carbon::parse($related->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($related->sale==1) tagsale @endif @endif">
            <div id="related-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
              <a href="{{url('products',$related->slug)}}">
                <div class="carousel-inner">
                    <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$related->photos[0]->photo}}})">
                      <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                    </div>
                  @foreach($related->variants as $variant)
                    @if(!empty($variant->photos[0]))
                    <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                      <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                    </div>
                    @endif
                  @endforeach
                </div>
              </a>
            </div>
            <div class="card-body">
              <h5 class="card-title">{{$related->brand->name}}</h5>
              <p class="card-text">{{$related->name}}</p>
              @if($related->sale==1)
              <div>
                <span class="price old-price"><del>Rs.{{$related->price}}</del></span> <span class="price new-price">Rs.{{$related->price - $related->sale_price/100 * $related->price }}</span>
              </div>
              @else
              <div class="price">Rs. {{$related->price}} </div>
              @endif

              <div class="color-option carousel-indicators mt-1" >
                <span class="color red active" data-target="#related-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$related->color->hex}}"></span>
                @foreach($related->variants as $variant)
                <span class="color red" id="new-arrival-product" data-target="#related-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
                @endforeach
              </div>
            </div>
          </div> <!-- /.card -->
        </div>
       @endforeach

      </div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          @if($productdata->sizes && $productdata->sizes[0] && $productdata->sizes[0]->group)
          <h4 class="modal-title text-center w-100" id="exampleModalLabel"> {{$productdata->sizes[0]->group->group}} </h4>
          @endif
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-x"></span>
          </button>
        </div>
        <div class="modal-body">
           @if($productdata->sizes && $productdata->sizes[0] && $productdata->sizes[0]->group)
           <img src="{{url('img/sizecharts/'.$productdata->sizes[0]->group->sizechart)}}" class="card-img" alt="{{$productdata->name}}" >
           @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Modal -->


</div> <!-- /.new-arrival-container -->
@endsection


@section('after-scripts')
<script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>
<script>
  AOS.init({duration: 800});
</script>

<script type="text/javascript">
  $('#add-to-cart-show').on('submit', function(e) {
    e.preventDefault();
    //loading
    $('.add-to-cart-btn').hide();
    $('#loading').show();

    $.ajax({
      method: 'post',
      url: $(this).attr('action'),
      data: $(this).serialize(),
      success: function(res) {
        $('.header-cart-wrapper').html(res);

        $('.form-quantity').val(1);

        var countCartItem = $('.cart-product-row').length;
        $('.cart-product-count').html(countCartItem);

        if (!$(".cart-toggle").hasClass("close-trigger")) {
          $(".cart-toggle").addClass("close-trigger");
          gsap.to('.cart-overlay', { duration: 0, display: "block", zIndex: "999", opacity: 0, y: -10 });
          gsap.to('.cart-overlay', { duration: 0.5, opacity: 1, y: 0, ease: "power4.outout" });
        }

        //remove loading
        $('.add-to-cart-btn').show();
        $('#loading').hide();
      }
    });
  });
</script>

<script type="text/javascript">
  $('.btn-plus, .btn-minus').on('click', function(e) {
    const isNegative = $(e.target).closest('.btn-minus').is('.btn-minus');
    const input = $(e.target).closest('.input-group').find('input');

    if (input.is('input')) {
      input[0][isNegative ? 'stepDown' : 'stepUp']()
    }
  });

  $(document).ready(function() {
    $('.carousel').carousel({'pause': true, 'interval': 90000});
  });

  $(document).ready(function () {
     // Attach Button click event listener
    $("#myBtn").click(function(){
      // show Modal
      $('#sizemodal').modal('show');
    });
  });

  $(document).ready(function(){
    $('.owl-carousel-product').owlCarousel({
      dots: false,
      margin: 38,
      responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        },
        1400:{
            items:5,
            nav:true,
            loop:false
        }
      }});
  });

  $('.color-option.carousel-indicators span').on('mouseover',function(){
    $(this).trigger('click');
    $('.color-option.carousel-indicators span').removeClass('active');
    $(this).addClass('active');
  });
</script>
<script>
  // function for carousel change according to selected color
  function currentCarouselColor(selColor){
    $('.single-product-carousel-container .carousel').addClass('d-none');
    $('.cur-color-'+selColor).removeClass('d-none');

    $('.size-option').addClass('d-none');
    $('.size-option-'+selColor).removeClass('d-none');
  }

  $(document).ready(function(){
    // default color goes here
    currentCarouselColor("default");

    // if color radio button is changed
    $('#colorOption').change(function(){
      var selected_value = $("input[name='color']:checked").attr('id');
      currentCarouselColor(selected_value);
      $("#showStock").empty();
      // $("input[name='size']").prop('checked',false);
    });

    $('.size-option').change(function(){
      getStock();
    })
  });

  getStock();
  function getStock() {
    var selected_value = $("input[name='size']:checked").attr('id');
    var stock = $("input[name='size']:checked").attr('stock');

    $("#showStock").empty();
    $("#showStock").append(stock + " stock remaining");
    $(".form-quantity").attr('max', stock);
    $("#hiddenStock").val(stock);
  }
</script>

<script>
  // this is for star rating
  $(document).ready(function() {
    $(document).on('click','.pagination a',function(event){
      event.preventDefault();
      const url=$(this).attr('href');
      getMoreReviews(url);
    });
    getMoreReviews();
    function getMoreReviews(url){
      if(url){
        $.ajax({
          type:"GET",
          url:url,
          success:function(data){
            $('#review-list').html(data);
          }
        })
      }else{
        $.ajax({
          type:"GET",
          url:'/{{ \Str::slug($productdata->name) }}/products-review/',
          success:function(data){
            $('#review-list').html(data);
          }
        })
      }
    }
    $('.star-box .star').click(function() {

      if ($(this).parent('.star-box').hasClass('star1')){
        $(this).parent('.star-box').removeClass('star1');
        $("#score").val("0");
      }else{
        var $this = $(this);
        $(".score").val($this.attr('id'));
        addStar($this);
      }
    });
    $('.star-box.star.star1 .star1').click(function() {
      $(this).parent('.star-box').removeClass('star1 star');
    });

    function addStar($this){
      var cStar = $this.attr('class');
      $this.parent('.star-box').removeClass().addClass("star-box " + cStar);
    }

  });
</script>

@endsection
