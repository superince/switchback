<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
    {{$blogs->appends(request()->except('page'))->links()}} 
  </ul>
</nav>

<div class="tab-content" id="myTabContent">
  @foreach($blogs as $blog)
  <div class="tab-pane fade show @if(!$loop->index) active @endif" id="{{$blog['name']}}" role="tabpanel" aria-labelledby="{{$blog['name']}}-tab">
    <div class="blog-container">
        <div class="row row-cols-1 row-cols-lg-3">
          @foreach($blogs as $bd)
          <div class="col card-col">
            <div class="card img-in-side h-100">
              <div class="row no-gutters">
              @if(count($bd->photos)>0)
              <div class="col-sm-12 col-md-12 col-lg-12 card-img-as-bg" style="background-image:url({{url('/img/blog/'.$bd->photos[0]->photo)}})">
              @else
                <div class="col-sm-6 col-md-6 col-lg-12 card-img-as-bg" style="background-image:url({{url('/frontend/images/blog-card-img-size.jpg')}}); ">
              @endif
                <a href="{{url('blog',$bd['slug'])}}"><img src="{{url('/frontend/images/blog-card-img-sizel.png')}}" class="card-img" alt="..."></a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-12 pb-5">
                  <div class="card-body">
                    <h3 class="card-title"><a href="{{url('blog',$bd['slug'])}}">{{ucfirst(trans($bd['title']))}}</a></h3>
                    <p class="date">{{\Carbon\Carbon::parse($bd['created_at'])->diffforHumans()}}</p>
                    
                    <div class="card-text"><a href="{{url('blog',$bd['slug'])}}">{{ substr(strip_tags(htmlspecialchars_decode($bd['short_description'])), 0, 150) }} ...</a></div>
                    <div class="pb-3">
                      <a href="{{url('blog',$bd['slug'])}}" class="btn btn-outline-dark d-inline-block mr-2">READ MORE</a>
                      <div class="sharebtn d-inline-block">
                        <a href="#" class="btn btn-outline-dark d-inline-block">SHARE</a>
                        <div class="share">
                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://switchbacknepal.com/"><span class="icon-facebook1"></span></a>
                          <a href="https://twitter.com/intent/tweet?url=https://switchbacknepal.com/&text="><span class="icon-twitter"></span></a>
                          <a href="https://pinterest.com/pin/create/button/?url=https://switchbacknepal.com/&media=&description="><span class="icon-instagram2"></span></a>
                          <a href="mailto:info@example.com?&subject=&body=https://switchbacknepal.com/ "><span class="icon-envelope"></span></a>
                        </div>
                      </div>
                      @if($bd->registration===1)
                      <div class="d-inline-block mt-3">
                        <a href="#blogRegister" target="_blank" class="d-inline-block w-auto border-0 btn btn-primary btn-md pl-4 pr-4 pt-2 pb-2 hover-animate open-registerride" data-toggle="modal" data-id="{{$blog->id}}">REGISTER FOR RIDE</a>
                      </div>
                      @endif
                    </div>
                  </div> <!-- /.card-body -->
                </div> <!-- /.col-md-6 -->
              </div>
            </div> <!-- /.card .img-in-side  -->
          </div> <!-- /.col-md-6 -->
          @endforeach
        </div> <!-- /.row -->
    </div><!-- /.blog-container -->
  </div>
  @endforeach
</div>

<div class="modal fade" id="blogRegister" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content cart-col-inner bg-white">
      <div class="modal-header pt-3 pb-0">
        <h4>REGISTER FOR RIDE </h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-x"></span>
          </button>
      </div>
      <div class="modal-body">
        <div class="outer-container pl-0 pr-0 pb-4">
          <form action="" method="post" class="needs-validation register-form" novalidate>
              {{csrf_field()}}
            <div class="row">
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="firstName" placeholder="First Name" name="fname" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lname" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <input type="email" class="form-control" id="email" placeholder="your@email" name="email" required>
                <div class="invalid-feedback">
                  Please enter a valid email address
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <input type="number" class="form-control" id="address" placeholder="Phone number" required name="contact_no" max="10" min="5">
                <div class="invalid-feedback">
                  Please enter your phone no.
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col mb-3 mt-2">
                <textarea  placeholder="Message" class="form-control" id="message" rows="5" name="message" required></textarea>
              </div>
            </div>

            <button class="btn btn-primary btn-outline-secondary btn-lg mt-3" type="submit">SUBMIT</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>