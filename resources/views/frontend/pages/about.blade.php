@extends('frontend.layouts.app')
@section('title', 'About -')
@section('content')
<style>
iframe{
  width:100%;
  height:550px;
}
</style>
    @if(!empty($about[0]->photo))
    <div class="banner-inner-page" style="background-image:url({{url('img/about/'.$about[0]->photo)}});">
      <img src="{{url('frontend/images/strory-img.jpg')}}" alt="">
    </div>
    @else
    <div class="banner-inner-page" style="background-image:url({{url('frontend/images/strory-img.jpg')}});">
      <img src="{{url('frontend/images/strory-img.jpg')}}" alt="">
    </div>
    @endif
    <div class="container-sm story-container" id="story">
      <h3>Our Story</h3>
      <div >
      @if(!empty($about[0]))
        {!! $about[0]->description !!}
      @else
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Convallis senectus phasellus enim libero. Tincidunt sit ac ut eros, nullam facilisi. Convallis sagittis, cras imperdiet id ultrices aenean amet lacus. Vitae massa eget pellentesque enim semper cursus purus, mauris ac. Eget egestas volutpat iaculis eleifend laoreet maecenas ut. Egestas tellus, congue eget gravida. Tincidunt molestie ipsum ultricies sapien non enim laoreet enim. Etiam amet cursus nullam cursus. Ipsum lorem aliquam aliquam neque. Vitae proin non arcu ac feugiat mauris nisl eget. Egestas tortor at eros feugiat justo congue semper tempus elit.</p>
      <p>Quis eu in nibh et quis molestie. Sed ultricies eu ligula platea porta etiam. Facilisi tellus vestibulum felis id morbi mattis sit. Fermentum etiam odio faucibus lacus, risus pellentesque sed laoreet. Urna mauris sed id parturient quam dolor, porttitor id arcu. Tristique orci tristique dictum eleifend diam egestas risus egestas. Orci sit risus faucibus turpis sodales pulvinar. Tincidunt urna vel nullam gravida sagittis ac enim est. Vel sodales posuere feugiat ullamcorper. Magna sodales nunc urna, maecenas hendrerit. Turpis facilisis placerat eget consectetur dolor fames arcu aliquam. Sollicitudin turpis risus, pharetra enim ac fermentum, egestas non.
      </p>
      @endif
      </div>
    </div> <!-- /.blog-single-contianer -->

    <div class="container-fluid" id="teams">
      <h3>OUR TEAM</h3>
      <div class="row">
        <!-- <div class="col-md-4" data-aos="fade-up" data-aos-delay="100" data-aos-once="true"> -->
        @foreach ($team as $teamdata)
        <div class="col-md-3 mb-5">
          <div class="card team h-100 mb-0">
            <div class="imgthumb"><img src="{{url('img/teams',$teamdata->photo)}}" alt=""></div>
            <div class="card-body">
              <h4 class="card-text name">{{$teamdata->name}}</h4>
              <h5 class="card-title">{{$teamdata->segment->name}}</h5>
              <p class="card-text">{{$teamdata->bio}}</p>
              <p class="card-text links">
                @if($teamdata->insta_link)
                <a href="{{$teamdata->insta_link}}" target="_blank"><span class="icon-instagram"></span></a>
                @endif
                @if($teamdata->fb_link)
                <a href="{{$teamdata->fb_link}}" target="_blank"><span class="icon-facebook"></span></a>
                @endif
              </p>
            </div>
          </div><!-- /.card team -->
        </div>
        @endforeach
      </div>
    </div>

    <div class="container-fluid card-owl-carousel">
      <h3>DEALERS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
        @foreach ($dealers as $dealer)

        <div class="h-100" data-aos-delay="100" data-aos-offset="0">
          <div class="card team h-100">
            <img src="{{url('img/dealers',$dealer->logo)}}" alt="" >
            <div class="card-body">
              <h5 class="card-title">{{$dealer->name}}</h5>
              <p class="card-text name mt-0 pb-1">{{$dealer->address}}</p>
              <h4 class="card-text m-0 pb-3">{{$dealer->category}}</h4 >
              <p class="card-text m-0 p-0 txt-with-icon"><a href="tel:+{{$dealer->phone}}"><span class="icon-phone"></span> {{$dealer->phone}}</a></p>
              @if($dealer->location)
                <p class="card-text m-0 p-0 txt-with-icon"><a target="_blank"><span class="icon-location"></span> {{$dealer->location}}</a></p>
              @endif

              @if($dealer->website)
                <p class="card-text m-0 p-0 txt-with-icon"><a href="{{$dealer->website}}" target="_blank"><span class="icon-global"></span> {{$dealer->name}}</a></p>
              @endif

            </div>
          </div><!-- /.card team -->
        </div>
        @endforeach
      </div>
    </div> <!-- /.FEATURED PRODUCTS new-arrival-container -->

    <div class="container-fluid">
      <h3>WHAT WE’VE BEEN UPTO</h3>
    </div>
    <div class="container-fluid blog-container pb-1" id="blogs">
      <div class="tab-nav-with-paination" id="blogs-rides">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          @foreach($tags as $tag)
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="{{$tag['name']}}-tab" data-toggle="tab" href="#{{$tag['name']}}" role="tab" aria-controls="{{$tag['name']}}" aria-selected="false" value="{{$tag['name']}}">{{strtoupper($tag['name'])}}</a>
          </li>
          @endforeach
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="rides-tab" data-toggle="tab" href="#rides" role="tab" aria-controls="rides" aria-selected="false" value="rides">RIDES</a>
          </li>
        </ul>
      </div>
      <div id="blog-filter">
      </div>
    </div>

<!-- new  -->
@endsection

@section('after-scripts')
<script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>
<script>
$(document).ready(function(){
  if(location.hash=='#blogs-rides'){
    getBlogsByTag("rides")
    $(".nav-item #rides-tab").addClass('active');
  }else{
    $(".nav-tabs .nav-link").first().addClass('active');
    const activeNav=$(".nav-item .nav-link.active").attr("value");
    getBlogsByTag(activeNav)
  }
})

$(document).on('click','.pagination a',function(event){
  event.preventDefault();
  const url=$(this).attr('href');
  getBlogsByTag(activeNav,url);
});



$(document).on('click','.nav-tabs a',function(event){
  event.preventDefault();
  const value=$(this).attr('value');
  getBlogsByTag(value);
});


function getBlogsByTag(name,url){
  if(url){
    $.ajax({
      type:"GET",
      url:url,
      success:function(data){
        $('#blog-filter').html(data);
      }
    })
  }else{
    $.ajax({
      type:"GET",
      url:`/about/blogs/${name}`,
      success:function(data){
        $('#blog-filter').html(data);
      }
    })
  }

}
$('.owl-carousel-product').owlCarousel({
      dots: false,
      margin: 38,
      responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
      }
    });
</script>

@endsection
