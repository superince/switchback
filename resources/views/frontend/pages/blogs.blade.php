@extends('frontend.layouts.app')
@section('title', 'BLogs -')
@section('content')
<div class="container-fluid" style="margin-top:80px;">
      <h3>WHAT WE’VE BEEN UPTO</h3>
    </div>
    <div class="container-fluid blog-container pb-1" id="blogs">
      <div class="tab-nav-with-paination" id="blogs-rides">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          @foreach($tags as $tag)
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="{{$tag['name']}}-tab" data-toggle="tab" href="#{{$tag['name']}}" role="tab" aria-controls="{{$tag['name']}}" aria-selected="false" value="{{$tag['name']}}">{{strtoupper($tag['name'])}}</a>
          </li>
          @endforeach
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="rides-tab" data-toggle="tab" href="#rides" role="tab" aria-controls="rides" aria-selected="false" value="rides">RIDES</a>
          </li>
        </ul>
      </div>
      <div id="blog-filter">
      </div>
    </div>
    <!-- new  -->
@endsection

@section('after-scripts')
<script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>
<script>
$(document).ready(function(){
  if(location.hash=='#blogs-rides'){
    getBlogsByTag("rides")
    $(".nav-item #rides-tab").addClass('active');
  }else{
    $(".nav-tabs .nav-link").first().addClass('active');
    const activeNav=$(".nav-item .nav-link.active").attr("value");
    getBlogsByTag(activeNav)
  }
})

$(document).on('click','.pagination a',function(event){
  event.preventDefault();
  const url=$(this).attr('href');
  getBlogsByTag(activeNav,url);
});



$(document).on('click','.nav-tabs a',function(event){
  event.preventDefault();
  const value=$(this).attr('value');
  getBlogsByTag(value);
});


function getBlogsByTag(name,url){
  if(url){
    $.ajax({
      type:"GET",
      url:url,
      success:function(data){
        $('#blog-filter').html(data);
      }
    })
  }else{
    $.ajax({
      type:"GET",
      url:`/about/blogs/${name}`,
      success:function(data){
        $('#blog-filter').html(data);
      }
    })
  }

}
$('.owl-carousel-product').owlCarousel({
      dots: false,
      margin: 38,
      responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:true
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
      }
    });
</script>

@endsection