@extends('frontend.layouts.app')
@section('title', 'Service -')
@section('content')
    @endsection
    <div class="container-fluid no-banner-page sm-container">
      @foreach($services as $service)
      <div data-aos="fade-up" data-aos-delay="200" data-aos-once="true" class="mt-5" id="{{strtolower(str_replace(' ','', $service->title))}}">
        <h3>{{$service->title}}</h3>
        <p>{!!$service->content!!}</p>
      </div>
      <hr>
      @endforeach
    </div>
    @section('after-scripts')
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        <script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
        <script src="{{url('frontend/js/jquery-parallax.js/parallax.min.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
        <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('frontend/dist/js/all.js')}}"></script>
        <script>
          AOS.init({duration: 800});
        </script>
        <script>
        $(document).ready(function(){
            $('.owl-carousel-product').owlCarousel({
            dots: false,
            margin: 38,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:4,
                    nav:true,
                    loop:false
                },
                1400:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }});
        });
        </script>
        <script>
            $.get('https://www.instagram.com/switchback.nepal/?__a=1',  // url
              function (data, textStatus, jqXHR) {  // success callback
                data.graphql.user.edge_owner_to_timeline_media.edges.forEach(function(node,index){
                    if(index<10){
                      var item=$('<a data-aos="fade-up" data-aos-delay="'+(100+index*100)+'" data-aos-offset="0" style="background-image:url('+node.node.display_url+');"><img src="'+ "{{url('frontend/images/square-img-temp.jpg')}}" +'" alt=""></a>');
                      $("#instafeed").append(item);
                    }
                })
            });
        </script>
    @endsection
