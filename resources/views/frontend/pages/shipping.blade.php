<div class="row" id="shippingrate">
  <div class="col-8">Shipping</div>
  <div class="col-4 shipping">Rs. {{session('cart_shipping')}}</div>
</div>
<hr>
<div class="row total-row">
  <div class="col-8">TOTAL</div>
  
  <div class="col-4">RS. {{ session('cart_subtotal')- session('cart_discount') + session('cart_shipping') }}</div>
</div>
<hr>