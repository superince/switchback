<div class="container container-products">
  <div class="row">
  @foreach ($productdata as $product)
    <div class="col-md-3 product-card-col mb-4 pb-3" data-aos="fade-up" data-aos-delay="0" data-aos-once="true">
      <div class="card mb-0 h-100 @if(Carbon\Carbon::parse($product->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($product->sale==1) tagsale @endif @endif">
        <div id="data-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
          <a href="{{url('products',$product->slug)}}">
            <div class="carousel-inner">
                @if(!empty($product->photos[0]))
                  <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$product->photos[0]->photo}}})">
                    <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                  </div>
                @endif

              @foreach($product->variants as $variant)
                @if(!empty($variant->photos[0]))
                <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                  <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                </div>
                @endif
              @endforeach
            </div>
          </a>
        </div>
          <div class="card-body">
            <h5 class="card-title"><a href="{{url('products',$product->slug)}}">{{strtoupper($product->brand->name)}}</a></h5>
            <p class="card-text"><a href="{{url('products',$product->slug)}}">{{strtoupper($product->name)}}</a></p>
            @if($product->sale==1)
            <div>
              <span class="price old-price"><del>Rs.{{$product->price}}</del></span> <span class="price new-price">Rs.{{$product->price - $product->sale_price/100 * $product->price }}</span>
            </div>
            @else
              <div class="price">Rs. {{$product->price}} </div>
            @endif
            <div class="color-option carousel-indicators mt-1" >
              <span class="color red active" data-target="#data-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$product->color->hex}}"></span>
              @foreach($product->variants as $variant)
              <span class="color red" id="new-arrival-product" data-target="#data-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
              @endforeach
            </div>
          </div>
      </div>
    </div>
  @endforeach
  </div>
  <div class="row">
    <div class="col-12">
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          {{$productdata->appends(request()->except('page'))->links()}}
        </ul>
      </nav>
    </div>
  </div>
</div>
