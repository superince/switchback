<div class="cart-col-inner pt-2 pb-4">
  <h4 class="pl-5 pb-3">
    REVIEWS <span class="text-muted ml-3">{{$total}} reviews</span>
  </h4>
  @foreach($reviews as $review)
    <div class="review-row pl-5 pr-5 pb-4">
      <div class="mb-3">
        <span class="date text-muted pr-4">{{$review->created_at->format('j F, Y')}}</span> <span class="name">{{ucwords($review->name)}}</span>
      </div>
      <div class="star-box">
        @if($review->score==0)
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        @elseif($review->score==1)
        <span class="star checked"></span>
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        @elseif($review->score==2)
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star"></span>
        <span class="star"></span>
        <span class="star"></span>
        @elseif($review->score==3)
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star"></span>
        <span class="star"></span>
        @elseif($review->score==4)
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star"></span>
        @elseif($review->score==5)
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        @endif
      </div><!-- /.star-box -->
      <div class="mt-2 title font-weight-bold">{{$review->title}}</div>
      <div class="text pb-4 border-bottom">{{$review->review}}</div>
    </div>
  @endforeach
  <!-- review pagination -->
<div class="review-pagination review-row pl-5 pr-5 pb-2">
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-end mb-0">
      {{$reviews->appends(request()->except('page'))->links()}}
      </ul>
    </nav>
  </div>
</div>

