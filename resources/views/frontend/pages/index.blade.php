@extends('frontend.layouts.app')
@section('content')

  @if($bannertype=='video')
  <div id="myCarousel" class="carousel slide rellax" data-rellax-speed="-7" data-ride="carousel">
    <div class="carousel-inner">
      @foreach($banner as $ban)
      <div class="carousel-item @if($loop->index==0) active @endif">
        <div class="img-as-bg video-item">
          <video class="video-fluid" autoplay loop muted>
            <source src="{{url('vid/',$ban->location)}}" type="video/mp4" />
          </video>
        </div>
        <div class="container">
          <div class="carousel-caption rellax" data-rellax-speed="3" data-rellax-percentage="0.3">
            <h2><span class="hide-text">{{$ban->primary_tagline}}  <br> <span class="secondary-line">{{$ban->secondary_tagline}}</span></span></h2>

            <p class="carousel-btn-link"><span class="hide-text"><a class="btn btn-primary hover-animate" href="{{url('')}}/{!!str_replace('_', ' ', $ban->url)!!}" role="button">EXPLORE<span class="icon-dot-right"></span></a></span></p>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div> <!-- /#myCarousel -->
  @endif
  @if($bannertype=='photo')
  <div id="myCarousel" class="carousel slide rellax" data-rellax-speed="-7" data-ride="carousel">
    <div class="carousel-inner">
    @foreach($banner as $ban)
      <div class="carousel-item @if($loop->index==0) active @endif">
        <div class="img-as-bg " style="background-image: url('{{url('/img/banners',$ban->location)}}')">
        </div>
        <div class="container">
          <div class="carousel-caption rellax" data-rellax-speed="3" data-rellax-percentage="0.3">
            <h2><span class="hide-text">{{$ban->primary_tagline}}  <br><span class="secondary-line">{{$ban->secondary_tagline}}</span></span></h2>
            <p class="carousel-btn-link"><span class="hide-text"><a class="btn btn-primary hover-animate" href="{{url('')}}/{!!str_replace('_', ' ', $ban->url)!!}" role="button">EXPLORE<span class="icon-dot-right"></span></a></span></p>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div> <!-- /#myCarousel -->
  @endif
    <div class="product-promo-cat-wrap skewed-style">
      <div class="product-promo-bg">
      </div> <!-- /.product-promo-bg -->
      <div class="product-promo">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="product-promo-txt rellax" data-rellax-speed="1" data-rellax-percentage="1">
                <div class="product-promo-gsap" data-aos="fade-right">
                  @foreach($static_content as $content)
                    @if($content->key=='title')
                      <h2>{{$content->value}}</h2>
                    @endif
                    @if($content->key=='info')
                      <h3>{{$content->value}}</h3>
                    @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div> <!-- /.row -->
          @foreach ($parallaxproductdata->take(1) as $paraproduct)
          <div class="row skewed-bg-row align-items-center">
            <div class="col-md-6" data-aos="fade-right" data-aos-delay="100">
              <div class="info-container rellax" data-rellax-speed="1" data-rellax-percentage="0.5">
                <h2 class="h6">{{strtoupper($paraproduct->brand->name)}}</h2>
                <h3>{{strtoupper($paraproduct->name)}}</h3>
                <h4>{{strtoupper($paraproduct->productcategory->name)}}</h4>
                <p>{{$paraproduct->short_description}} </p>
                <p class="mt-4 pt-2"><a class="btn btn-primary hover-animate" href="{{url('products',$paraproduct->slug)}}" role="button">SHOP NOW <span class="icon-dot-right"></span></a></p>
              </div>
            </div>
            <div class="col-md-6 text-right" data-aos="fade-left" data-aos-delay="400">
              <div class="rellax" data-rellax-speed="5" data-rellax-percentage="0.6">
                <div class="img-bg">
                @if(!empty($paraproduct->photos[0]))
                  <img src="{{url('/img/products/'.$paraproduct->photos[0]->photo)}}" class="card-img" alt="...">
                @else
                <img src="{{url('frontend/images/ski-goggles2.png')}}" alt="">
                @endif
                </div>
              </div>
            </div>
          </div>
          @endforeach
          @foreach ($parallaxproductdata->skip(1)->take(1) as $paraproduct)
          <div class="row skewed-bg-row align-items-center">
            <div class="col-md-6 text-left" data-aos="fade-right">
              <div class="rellax" data-rellax-speed="3" data-rellax-percentage="0.5">
                <div class="img-bg full-body text-center">
                @if(!empty($paraproduct->photos[0]))
                  <img src="{{url('/img/products/'.$paraproduct->photos[0]->photo)}}" class="card-img" alt="...">
                @else
                <img src="{{url('frontend/images/img-full-body1.png')}}" alt="" class="img-out-box">
                @endif
                </div>
              </div>
            </div>
            <div class="col-md-5 offset-md-1" data-aos="fade-left" data-aos-delay="400">
              <div class="info-container">
                <h2 class="h6">{{strtoupper($paraproduct->brand->name)}}</h2>
                <h3>{{strtoupper($paraproduct->name)}}</h3>
                <h4>{{strtoupper($paraproduct->productcategory->name)}}</h4>
                <p>{{$paraproduct->short_description}} </p>
                <p class="mt-4 pt-2"><a class="btn btn-primary hover-animate"  href="{{url('products',$paraproduct->slug)}}" role="button">SHOP NOW <span class="icon-dot-right"></span></a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div> <!-- /.product-promo -->
      <div class="product-promo">
        <div class="container-fluid">
          @foreach ($parallaxproductdata->skip(2)->take(1) as $paraproduct)
          <div class="row skewed-bg-row align-items-center">
            <div class="col-md-6" data-aos="fade-right" data-aos-delay="100">
              <div class="info-container rellax" data-rellax-speed="0.5" data-rellax-percentage="0.5">
                <h2 class="h6">{{strtoupper($paraproduct->brand->name)}}</h2>
                <h3>{{strtoupper($paraproduct->name)}}</h3>
                <h4>{{strtoupper($paraproduct->productcategory->name)}}</h4>
                <p>{{$paraproduct->short_description}} </p>
                <p class="mt-4 pt-2"><a class="btn btn-primary hover-animate" href="{{url('products',$paraproduct->slug)}}" role="button">SHOP NOW <span class="icon-dot-right"></span></a></p>
              </div>
            </div>
            <div class="col-md-6 text-right" data-aos="fade-left" data-aos-delay="400">
              <div class="rellax" data-rellax-speed="5" data-rellax-percentage="0.6">
                <div class="img-bg">
                @if(!empty($paraproduct->photos[0]))
                  <img src="{{url('/img/products/'.$paraproduct->photos[0]->photo)}}" class="card-img" alt="...">
                @else
                <img src="{{url('frontend/images/ski-goggles2.png')}}" alt="">
                @endif
                </div>
              </div>
            </div>
          </div>
          @endforeach
          @foreach ($parallaxproductdata->skip(3)->take(1) as $paraproduct)
          <div class="row skewed-bg-row align-items-center">
            <div class="col-md-6 text-left" data-aos="fade-right">
              <div class="rellax" data-rellax-speed="3" data-rellax-percentage="0.5">
                <div class="img-bg full-body text-center">
                @if(!empty($paraproduct->photos[0]))
                  <img src="{{url('/img/products/'.$paraproduct->photos[0]->photo)}}" class="card-img" alt="...">
                @else
                <img src="{{url('frontend/images/img-full-body1.png')}}" alt="" class="img-out-box">
                @endif
                </div>
              </div>
            </div>
            <div class="col-md-5 offset-md-1" data-aos="fade-left" data-aos-delay="400">
              <div class="info-container">
                <h2 class="h6">{{strtoupper($paraproduct->brand->name)}}</h2>
                <h3>{{strtoupper($paraproduct->name)}}</h3>
                <h4>{{strtoupper($paraproduct->productcategory->name)}}</h4>
                <p>{{$paraproduct->short_description}} </p>
                <p class="mt-4 pt-2"><a class="btn btn-primary hover-animate"  href="{{url('products',$paraproduct->slug)}}" role="button">SHOP NOW <span class="icon-dot-right"></span></a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div> <!-- /.product-promo -->
      <div class="categories-box-wrapper">
        <div class="row no-gutters">
          @foreach ($highlights as $highlight)
            <div class="col-4" data-aos="fade-up" data-aos-delay="100" data-aos-offset="0">
                @if(!empty($highlight->highlight))
                <a href="{{strtolower($highlight->name)}}" style="background-image: url({{url('/img/highlights/'.$highlight->highlight)}});">
                  <span class="text-uppercase">{{$highlight->name}}</span>
                </a>
                @else
                <a href="{{strtolower($highlight->name)}}" style="background-image: url({{url('frontend/images/moto-cat.jpg')}});">
                  <span class="text-uppercase">{{$highlight->name}}</span>
                </a>
                @endif
            </div>
          @endforeach
        </div>
      </div> <!-- /.categories-box-wrapper -->
    </div>

    <div class="categories-box-wrapper d-none">
      <div class="row no-gutters">
         @foreach ($divisions->take(3) as $division)
        <div class="col-md-4">
          <a href="{{url($division->name)}}" style="background-image: url({{url('/img/highlights/'. $division->highlight)}});">
            <img src="{{url('img/highlights/'.$division->highlight)}}" alt="">
            <span>{{strtoupper($division->name)}}</span>
          </a>
        </div>
      @endforeach
      </div>
    </div> <!-- /.categories-box-wrapper -->

    <div class="container-fluid card-owl-carousel new-arrival-container diagonal-bg-outer">
      <div class="diagonal-bg">
      </div> <!-- /.product-promo-bg -->

      <h3>NEW ARRIVALS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
        @foreach ($newproducts as $newproduct)
          <div class="h-100" data-aos="fade-up" data-aos-delay="200" data-aos-offset="100">
            <div class="h-100 card @if(Carbon\Carbon::parse($newproduct->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($newproduct->sale==1) tagsale @endif @endif">
              <div id="newproduct-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
                <a href="{{url('products',$newproduct->slug)}}">
                  <div class="carousel-inner">
                      <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$newproduct->photos[0]->photo}}})">
                        <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                      </div>
                    @foreach($newproduct->variants as $variant)
                      @if(!empty($variant->photos[0]))
                      <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                        <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                      </div>
                      @endif
                    @endforeach
                  </div>
                </a>
              </div>
              <div class="card-body">
                <a href="{{url('products',$newproduct->slug)}}">
                  <h5 class="card-title">{{$newproduct->brand->name}}</h5>
                  <p class="card-text">{{$newproduct->name}}</p>

                  @if($newproduct->sale==1)
                  <span class="price old-price"><del>Rs.{{$newproduct->price}}</del></span> <span class="price new-price">Rs.{{$newproduct->price - $newproduct->sale_price/100 * $newproduct->price }}</span>
                  @else
                    <div class="price">Rs. {{$newproduct->price}} </div>
                  @endif
                </a>

                <div class="color-option carousel-indicators mt-1" >
                  <span class="color red active" data-target="#newproduct-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$newproduct->color->hex}}"></span>
                  @foreach($newproduct->variants as $variant)
                  <span class="color red" id="new-arrival-product" data-target="#newproduct-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
                  @endforeach
                </div>
              </div>
            </div> <!-- /.card -->
          </div>
        @endforeach
      </div>
    </div> <!-- /.new-arrival-container -->
    <div class="container-fluid blog-container blog-bg-dark">
      <h3>WHAT WE’VE BEEN UPTO</h3>
      <div class="owl-carousel-blog owl-carousel owl-theme">
        @foreach ($blogs as $blog)
          <div class="h-100" data-aos="fade-up" data-aos-delay="100" data-aos-offset="100">
            <div class="card img-in-side h-100">
              <div class="row no-gutters">
                @if(count($blog->photos)>0)
                <div class="col-sm-12 col-md-12 col-lg-12 card-img-as-bg" style="background-image:url({{url('/img/blog/'.$blog->photos[0]->photo)}})">
                  <a href="{{url('blog',$blog->slug)}}"><img src="{{url('/frontend/images/blog-card-img-sizel.png')}}" class="card-img" alt="..."></a>
                  @if($blog->registration)
                  <div class="btn-over-img">
                    <a href="#blogRegister" target="_blank" class="d-inline-block w-auto border-0 btn btn-primary btn-md pl-4 pr-4 hover-animate open-registerride" data-toggle="modal" data-id="{{$blog->id}}">REGISTER FOR RIDE</a>
                  </div>
                  @endif
                </div>
                @else
                <div class="col-sm-12 col-md-12 col-lg-12 card-img-as-bg" style="background-image:url({{url('/frontend/images/blog-card-img-size.png')}})">
                  <img src="{{url('/frontend/images/blog-card-img-sizel.png')}}" class="card-img" alt="...">
                </div>
                @endif

                <div class="col-sm-12 col-md-12 col-lg-12 pb-5">
                <div class="card-body">
                  <h3 class="card-title"><a href="{{url('blog',$blog->slug)}}">{{ucfirst(trans($blog->title))}}</a></h3>
                  <p class="date">{{$blog->created_at->diffforHumans()}}</p>
                  <div></div>
                  <div class="card-text"><a href="#">{{ substr(strip_tags(htmlspecialchars_decode($blog->short_description)), 0, 150) }} ...</a></div>

                  <div class="pb-3">
                    <a href="{{url('blog',$blog->slug)}}" class="btn btn-outline-dark d-inline-block mr-2">READ MORE</a>
                    <div class="sharebtn d-inline-block">
                      <a href="#" class="btn btn-outline-dark d-inline-block">SHARE</a>
                      <div class="share">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=http://switchbacknepal.cf/"><span class="icon-facebook1"></span></a>
                        <a href="https://twitter.com/intent/tweet?url=http://switchbacknepal.cf/&text="><span class="icon-twitter"></span></a>
                        <a href="https://pinterest.com/pin/create/button/?url=http://switchbacknepal.cf/&media=&description="><span class="icon-instagram2"></span></a>
                        <a href="mailto:info@example.com?&subject=&body=http://switchbacknepal.cf/ "><span class="icon-envelope"></span></a>
                      </div>
                    </div>
                    @if($blog->registration)
                    <div class="d-inline-block mt-3">
                      <a href="#blogRegister" target="_blank" class="d-inline-block w-auto border-0 btn btn-primary btn-md pl-4 pr-4 pt-2 pb-2 hover-animate open-registerride" data-toggle="modal" data-id="{{$blog->id}}">REGISTER FOR RIDE</a>
                    </div>
                    @endif
                  </div>
                </div> <!-- /.card-body -->
              </div> <!-- /.col-md-6 -->
              </div>
            </div>
          </div>
        @endforeach
      </div> <!-- /.row -->
      <div class="text-center">
        <a href="/about/#blogs" target="_blank" class="border-dark btn btn-primary btn-md pl-5 pr-5 hover-animate">EXPLORE</a>
      </div>
    </div> <!-- /.blog-container -->

    @if($featuredproductdatas->count() > 0)
    <div class="container-fluid card-owl-carousel">
      <h3>FEATURED PRODUCTS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
        @foreach ($featuredproductdatas as $featuredproduct)
        <div class="h-100" data-aos="fade-up" data-aos-delay="100" data-aos-offset="0">
          <div class="h-100 card @if(Carbon\Carbon::parse($featuredproduct->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($featuredproduct->sale==1) tagsale @endif @endif">

              <div id="featuredproduct-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
              <a href="{{url('products',$featuredproduct->slug)}}">
                <div class="carousel-inner">
                  @if (count($featuredproduct->photos)>0)
                  <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$featuredproduct->photos[0]->photo}}})">
                    @else
                    <div class="carousel-item active " style="background-image:url({{'/frontend/images/logo_white_horizontal.png'}})">
                  @endif
                      <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                    </div>
                  @foreach($featuredproduct->variants as $variant)
                    @if(!empty($variant->photos[0]))
                    <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                      <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                    </div>
                    @endif
                  @endforeach
                </div>
                </a>
              </div>

              <div class="card-body">
                <a href="{{url('products',$featuredproduct->slug)}}">
                  <h5 class="card-title">{{$featuredproduct->brand->name}}</h5>
                  <p class="card-text">{{$featuredproduct->name}}</p>
                  @if($featuredproduct->sale==1)
                    <span class="price old-price"><del>Rs.{{$featuredproduct->price}}</del></span> <span class="price new-price">Rs.{{$featuredproduct->price - $featuredproduct->sale_price/100 * $featuredproduct->price }}</span>
                  @else
                    <div class="price">Rs. {{$featuredproduct->price}} </div>
                  @endif
                </a>
                <div class="color-option carousel-indicators mt-1">
                  <span class="color red active" data-target="#featuredproduct-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$featuredproduct->color->hex}}"></span>
                  @foreach($featuredproduct->variants as $variant)
                  <span class="color red" id="new-arrival-product" data-target="#featuredproduct-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
                  @endforeach
                </div>
              </div>
            </a>
          </div> <!-- /.card -->
        </div>
        @endforeach
      </div>
    </div> <!-- /.FEATURED PRODUCTS new-arrival-container -->
    @endif

    <div class="container-fluid card-owl-carousel">
      <h3>POPULAR PRODUCTS</h3>
      <div class="owl-carousel-product owl-carousel owl-theme">
        @foreach ($popularproductdatas as $popularproduct)
          <div class="h-100" data-aos="fade-up" data-aos-delay="100" data-aos-offset="100">
            <div class="h-100 card @if(Carbon\Carbon::parse($popularproduct->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($popularproduct->sale==1) tagsale @endif @endif">
              <div id="popularproduct-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
                <a href="{{url('products',$popularproduct->slug)}}">
                  <div class="carousel-inner">
                    @if (count($popularproduct->photos)>0)
                      <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$popularproduct->photos[0]->photo}}})">
                      @else
                      <div class="carousel-item active " style="background-image:url({{'/frontend/images/logo_white_horizontal.png'}})">
                    @endif
                        <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                      </div>
                    @foreach($popularproduct->variants as $variant)
                      @if(!empty($variant->photos[0]))
                      <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                        <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                      </div>
                      @endif
                    @endforeach
                  </div>
               </a>
              </div>
                <div class="card-body">
                  <a href="{{url('products',$popularproduct->slug)}}">
                    <h5 class="card-title"><a href="{{url('products',$popularproduct->slug)}}">{{$popularproduct->brand->name}}</a></h5>
                    <p class="card-text"><a href="{{url('products',$popularproduct->slug)}}">{{$popularproduct->name}}</a></p>
                    @if($popularproduct->sale==1)
                    <div>
                      <span class="price old-price"><del>Rs.{{$popularproduct->price}}</del></span> <span class="price new-price">Rs.{{$popularproduct->price - $popularproduct->sale_price/100 * $popularproduct->price }}</span>
                    </div>
                    @else
                    <div class="price">Rs. {{$popularproduct->price}} </div>
                    @endif
                  </a>
                  <div class="color-option carousel-indicators mt-1">
                    <span class="color red active" data-target="#popularproduct-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$popularproduct->color->hex}}"></span>
                    @foreach($popularproduct->variants as $variant)
                    <span class="color red" id="new-arrival-product" data-target="#popularproduct-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
                    @endforeach
                  </div>
                </div>
            </div> <!-- /.card -->
          </div>
        @endforeach
      </div>
    </div> <!-- /POPULAR PRODUCTS .new-arrival-container -->


    @include('frontend.includes._partials.instafollow')

    <div class="modal fade" id="blogRegister" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content cart-col-inner bg-white">
          <div class="modal-header pt-3 pb-0">
            <h4>REGISTER FOR RIDE </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="icon-x"></span>
              </button>
          </div>
          <div class="modal-body">
            <div class="outer-container pl-0 pr-0 pb-4">
              <form action="" method="post" class="needs-validation register-form" novalidate>
                 {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <input type="text" class="form-control" id="firstName" placeholder="First Name" name="fname" required>
                    <div class="invalid-feedback">
                      Valid first name is required.
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lname" required>
                    <div class="invalid-feedback">
                      Valid last name is required.
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <input type="email" class="form-control" id="email" placeholder="your@email" name="email" required>
                    <div class="invalid-feedback">
                      Please enter a valid email address
                    </div>
                  </div>

                  <div class="col-md-6 mb-3">
                    <input type="number" class="form-control" id="address" placeholder="Phone number" required name="contact_no" max="10" min="5">
                    <div class="invalid-feedback">
                      Please enter your phone no.
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col mb-3 mt-2">
                    <textarea  placeholder="Message" class="form-control" id="message" rows="5" name="message" required></textarea>
                  </div>
                </div>

                <button class="btn btn-primary btn-outline-secondary btn-lg mt-3" type="submit">SUBMIT</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    @foreach($notice as $not)
    <div class="modal fade" id="subscribe-box-{{$loop->index}}" tabindex="-1" aria-labelledby="subscribe-boxModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header pb-0">
            <h4 class="text-center modal-title w-100" id="exampleModalLabel">{{$not->title}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span class="icon-x"></span>
            </button>
          </div>
          <div class="modal-body pb-4">
            <div class="text-center">
              <a href="{{$not->link}}"><img src="{{url('img/notices',$not->photo)}}" alt="" class="img-fluid"></a>
            </div>
            <div class="mt-3 notice-txt">
              <a href="{{$not->link}}">
                {{$not->description}}
              </a>
              <div>
                <a href="{{$not->link}}" class="btn btn-sm btn-outline-dark d-inline-block mt-3 mr-2">READ MORE</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script>
    
    $(document).ready(function() {
      setTimeout(function(){ $('#subscribe-box-{{$loop->index}}').modal('show'); }, 3000);
    });
    </script>
    @endforeach
    @endsection

    @section('after-scripts')
       <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
        <script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
        <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
        <script src="{{url('frontend/dist/js/all.js')}}"></script>
        <script type="text/javascript">
          $('body').on('keyup','#search-products',function(){
            const searchQuery=$(this).val();
            $.ajax({
              method:'POST',
              url:'{{route("search-products")}}',
              dataType:'json',
              data:{
                '_token':'{{csrf_token()}}',
                searchQuery:searchQuery
              },
              success:function(res){
                console.log(res)
              }
            })
          })
        </script>
        <script type="text/javascript">
          AOS.init({duration: 800});
        // entry animation
      function loadingAnimate(){
        $(document).ready(function(){
            gsap.from("#loadlogo", {duration: 0.5, display: 'block', opacity: '1', y: "50px",  delay: 2, ease: "power4.outout"});
            gsap.from("#body-load", {duration: 2, display: 'block', scale: 3, y: '0%', x: '0%', skewY: '0deg', delay: 2, ease: "power4.outout"});
            gsap.from("#myCarousel", {duration: 2,  scale: 1.1, delay: 1, transformOrigin: 'center center', ease: "power4.outout"});
            gsap.from(".hide-text", {duration: 0.5, y: "120%", delay: 1.8, stagger: 0.5, ease: "power4.outout"});
        });
      }

      // check cookie if true run loading 
      function getCookie(name) {
          var dc = document.cookie;
          var prefix = name + "=";
          var begin = dc.indexOf("; " + prefix);
          if (begin == -1) {
              begin = dc.indexOf(prefix);
              if (begin != 0) return null;
          }
          else
          {
              begin += 2;
              var end = document.cookie.indexOf(";", begin);
              if (end == -1) {
              end = dc.length;
              }
          }
          // because unescape has been deprecated, replaced with decodeURI
          //return unescape(dc.substring(begin + prefix.length, end));
          return decodeURI(dc.substring(begin + prefix.length, end));
      } 

      function showLoading() {
          var myCookie = getCookie("MyCookie");

          if (myCookie == null) {
              // do cookie doesn't exist stuff;
              console.log("no cookie");
              document.cookie = "MyCookie=true";
              loadingAnimate();
          }
          else {
              // do cookie exists stuff
              console.log("yes cookie");
          }
      }
      showLoading();
      
        // parallax rellax
        var rellax = new Rellax('.rellax');
        </script>
        <script>
          $(document).on("click", ".open-registerride", function () {
              var blogId = $(this).data('id');
              console.log(blogId)
              $(".register-form").attr("action",'/bookride/'+blogId);
              // As pointed out in comments,
              // it is unnecessary to have to manually call the modal.
              // $('#addBookDialog').modal('show');
          });
        </script>
        <script>
          $.get('/instagram',  // url
            function (data, textStatus, jqXHR) {  // success callback
              data.graphql.user.edge_owner_to_timeline_media.edges.forEach(function(node,index){
                  if(index<10){
                    var item=$('<a data-aos="fade-up" data-aos-delay="'+(100+index*100)+'" data-aos-offset="0" style="background-image:url('+node.node.display_url+');"><img src="'+ "{{url('frontend/images/square-img-temp.jpg')}}" +'" alt=""></a>');
                    $("#instafeed").append(item);
                  }
              })
          });
        </script>
<script>
  $('.color-option.carousel-indicators span').on('mouseover',function(){
    $(this).trigger('click');
    $('.color-option.carousel-indicators span').removeClass('active');
    $(this).addClass('active');
  });
 /* $("#new-arrival-product").hover(function () {

      $.ajax({
          'method': 'get',
          'url': '<?php // echo url("getproductimg" , ['id' => $newproduct->id]);?>',
          success: function (response) {

            console.log(response);
              $('#img-to-replace').text(response);
              $('#img-to-replace').css("height:50000px");

          }
      });
  }); */

</script>
@endsection