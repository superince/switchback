@extends('frontend.layouts.app')
@section('title', 'Shop -')
@section('content')
<style>
input[type=checkbox]:checked + .custom-color-label {
    border: 2px solid black !important;
}
</style>
    @if($brand->banner)
    <div class="banner-inner-page" style="background-image:url({{url('/img/banners/'.$brand->banner)}});">
      <img src="{{url('img/banners/'.$brand->banner)}}" alt="Welcome to Switchback">
      <div class="banner-info-wrapper">
        <div class="container-fluid">
          <div class="banner-info-txt">
              <div class="title">{{$brand->name}}</div>
              <div class="info d-none">{{$brand->info}}</div>
            </div>
        </div>
      </div>
    </div>
    @else
    <div class="banner-inner-page" style="background-image:url({{url('frontend/images/jump-power-motorbike.jpg')}});">
      <img src="{{url('frontend/images/jump-power-motorbike.jpg')}}" alt="Welcome to Switchback">
    </div>
    @endif

    <div class="container-fluid breadcrumb-container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{$brand->name}}</li>
        </ol>
      </nav>
    </div> <!-- /.container-fluid breadcrumb-container-->
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 sidebar mb-5">
          <h2>{{strtoupper($brand->name)}}</h2>
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

            @foreach ($categorydata as $category)
            @if(request()->segment(1) == 'brand')
              <a class="nav-link text-uppercase {{(request()->segment(3) == strtolower($category['name']))?'active':''}}" href="{{url('brand/'. strtolower($brand->name),strtolower($category['name']))}}">{{$category['name']}}</a>
            @else
              <a class="nav-link text-uppercase {{(request()->segment(2) == strtolower($category['name']))?'active':''}}" href="{{url(strtolower($brand->name),strtolower($category['name']))}}">{{$category['name']}}</a>
            @endif
            @endforeach
          </div>

        <button type="button" class="btn btn-primary w-100 mb-3" data-toggle="collapse" data-target="#filter"> <i class="fas fa-filter"></i>&nbsp; Filter</button>
        <div id="filter" class="collapse">
          
          <h3>BRANDS</h3>

            <div class="brand-option">
              @foreach ($branddata as $brand_side)
              <div class="custom-control custom-radio custom-control-inline">
                  <input type="checkbox" id="{{$brand_side->name}}" name="brand" class="custom-control-input" @if($brand->name==$brand_side->name) checked @endif>
                  <label class="custom-control-label" for="{{$brand_side->name}}">{{$brand_side->name}}</label>
                </div>
              @endforeach
            </div>

            <h3>SIZE</h3>
            @foreach($sizedata as $group)
              <h6>{{$group->group}}</h6>
              <div class="size-option border-bottom border-dark">
                @foreach ($group->sizes as $size)
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="checkbox" id="{{$group->id}}-{{$size->name}}" name="size" class="custom-control-input" @if(Request::get("size")==$size->name) checked @endif>
                    <label class="custom-control-label" for="{{$group->id}}-{{$size->name}}">{{$size->name}}</label>
                  </div>
                @endforeach
              </div>
            @endforeach

            <h3>COLOR</h3>
            <div class="color-option">
              @foreach ($colordata as $color)
                <input type="checkbox" id="color-{{strtolower($color->name)}}" name="color" class="color-input-radio" value="{{ $color->id }}" @if(Request::get("color")==strtolower($color->name)) checked @endif >
                <label for="color-{{strtolower($color->name)}}" class="custom-color-label"
                  style="background: {{ $color->hex }}; border: 2px solid {{ $color->hex }}"
                ></label>
              @endforeach
            </div>

            <h3>PRICE</h3>
            <div class="price-option mb-2">
              <input id="price-slider" type="text" class="span2" value="" data-slider-min="0" data-slider-max="100000" data-slider-step="5" data-slider-value="[100,100000]"/>
              <div id="pirceview" class="price text-center pr-3 h6 pb-3"></div>
            </div>
        </div>

          <!-- <button type="button" class="btn btn-sm btn-outline-secondary mb-3">CLEAR FILTER</button>
          <button type="submit" class="btn btn-sm btn-primary btn-outline-secondary mb-3">APPLY FILTERS</button> -->
        </div><!-- /.sidebar -->
        <div class="col-md-10 product-list-col">
          <div id="products-list">
          </div>
        </div>
      </div>
    </div>
 <!-- /.new-arrival-container -->

<div class="container-fluid card-owl-carousel">
  <h3>POPULAR PRODUCTS</h3>
  <div class="owl-carousel-product owl-carousel owl-theme">
    @foreach ($relatedproductdata as $related)
    <div class="h-100" data-aos="fade-up" data-aos-delay="200" data-aos-offset="100">
      <div class="h-100 card @if(Carbon\Carbon::parse($related->created_at)->diffInDays(now(), false) <= 7) tagnew @else @if($related->sale==1) tagsale @endif @endif">
        <div id="related-{{$loop->index}}" class="productpreview carousel slide" data-ride="carousel" data-interval="false">
          <a href="{{url('products',$related->slug)}}">
            <div class="carousel-inner">
                  @if (count($related->photos)>0)
                    <div class="carousel-item active " style="background-image:url({{{'/img/products/'.$related->photos[0]->photo}}})">
                  @else
                  <div class="carousel-item active " style="background-image:url({{'/frontend/images/logo_white_horizontal.png'}})">
                  @endif
                  <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                </div>
              @foreach($related->variants as $variant)
                @if(!empty($variant->photos[0]))
                <div class="carousel-item " style="background-image:url({{{'/img/products/'.$variant->photos[0]->photo}}})">
                  <img src="{{url('frontend/images/square-img-temp.jpg')}}" class="d-block w-100" alt="...">
                </div>
                @endif
              @endforeach
            </div>
          </a>
        </div>
        <div class="card-body">
          <h5 class="card-title">{{$related->brand->name}}</h5>
          <p class="card-text">{{$related->name}}</p>
          @if($related->sale==1)
          <div>
            <span class="price old-price"><del>Rs.{{$related->price}}</del></span> <span class="price new-price">Rs.{{$related->price - $related->sale_price/100 * $related->price }}</span>
          </div>
          @else
            <div class="price">Rs. {{$related->price}} </div>
          @endif

          <div class="color-option carousel-indicators mt-1" >
            <span class="color red active" data-target="#related-{{$loop->index}}" data-slide-to="0" id="new-arrival-product" style="background:{{$related->color->hex}}"></span>
            @foreach($related->variants as $variant)
            <span class="color red" id="new-arrival-product" data-target="#related-{{$loop->parent->index}}" data-slide-to="{{$loop->index +1}}" style="background:{{$variant->color->hex}}"></span>
            @endforeach
          </div>
        </div>
      </div> <!-- /.card -->
    </div>
    @endforeach
  </div>
</div>

@endsection

@section('after-scripts')
<script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap-slider/dist/bootstrap-slider.js')}}"></script>
<script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>
<script>
  AOS.init({duration: 800});
  $('body').on('mouseover','.color-option.carousel-indicators span',function(){
    $(this).trigger('click');
    $('.color-option.carousel-indicators span').removeClass('active');
    $(this).addClass('active');
  });
</script>

<script>
  var slider = new Slider('#price-slider', {});

  let selected_size = [];
  let selected_color = [];
  let selected_brand = [];
  let startPrice = 0;
  let endPrice = 100000;
  let category = '{{ $cat }}';
  let type = "{{ request()->segment(1) == 'brand' ? 'brand' : 'division' }}";

  if(type == 'brand') {
    $("input[name=brand][id='{{ ucfirst(request()->segment(2)) }}']").prop("checked",true);
    selected_brand.push('{{ ucfirst(request()->segment(2)) }}');
  }

  filterProducts();

  function filterProducts() {
    $.ajax({
      type: 'get',
      url: '/{{ \Str::slug($brand->name) }}/products-filter/',
      data: {
        size: JSON.stringify(selected_size),
        color: JSON.stringify(selected_color),
        brand: JSON.stringify(selected_brand),
        startPrice: startPrice,
        endPrice: endPrice,
        category: category,
        type: type
      },
      success: function (res) {
        $('#products-list').html(res);
      }
    });
  }

  $('.size-option, .color-option, .brand-option').change(function(){
    var temp_brand = [], temp_color = [], temp_size = [];

    $.each($("input[name='size']:checked"), function(){
      temp_size.push($(this).attr('id'))
    });
    selected_size = [...temp_size];
        
    $.each($("input[name='color']:checked"), function(){
      temp_color.push($(this).val())
    });
    selected_color = [...temp_color];

    $.each($("input[name='brand']:checked"), function(){
      temp_brand.push($(this).attr('id'))
    });
    selected_brand = [...temp_brand];
    filterProducts();
  });

  slider.on('slideStop', function(ev){
    startPrice = slider.getValue()[0];
    endPrice = slider.getValue()[1];

    filterProducts();
  });

  $('body').on('click', '.container-products .pagination a', function(e) {
      e.preventDefault();
      var url = $(this).attr('href');

      $.ajax({
        url: url,
        success: function(res) {
          $('#products-list').html(res);
        }
      });
  });


  // price view
  $(document).ready(function () {
    showprice();
    $('#price-slider').change(function () {
      showprice();
    });
  });

  function showprice() {
    var curPrice = $('.slider .tooltip .tooltip-inner').html();
    var newString = curPrice.replace(":", "-");
    $('#pirceview').html(newString);
  }
</script>
@endsection
