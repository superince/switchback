@extends('frontend.layouts.app')
@section('content')
     <div class="cart-page-wrapper">
      <div class="container-fluid">
        {{ Form::open(['route' => 'cart.checkout', 'id' => 'checkoutform']) }}
        <div class="row">
          <div class="col-xl-8 cart-col mb-5" data-aos="fade-right" data-aos-delay="100" data-aos-once="true">
            <div class="cart-col-inner">
              <h4>CHECK OUT</h4>
              <div class="outer-container pb-4">
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control" id="firstName" name="firstname" placeholder="First Name" value="" required>
                    </div>
                    <div class="col-md-6 mb-3">
                      <input type="text" class="form-control" id="lastName" name="lastname" placeholder="Last Name" value="" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <input type="email" class="form-control" id="email" name="email" placeholder="your@email" required>
                    </div>

                    <div class="col-md-6 mb-3">
                      <input type="tel" class="form-control" id="phone" name="phone" placeholder="Phone number" required>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <input type="tel" class="form-control" id="secondary_phone" name="secondary_phone" placeholder="Secondary Phone number" required>
                    </div>
                  </div>

                  <div class="row mt-3">
                    <div class="col-md-6 mb-3">
                      <select class="custom-select d-block w-100" id="country" name="country" required>
                        <option value="Nepal">Nepal</option>
                        <option value="India">India</option>
                      </select>
                    </div>
                    <div class="col-md-6 mb-3">
                      <select class="custom-select d-block w-100" id="state" name="state" required>
                        @foreach($shippings as $shipping)
                          <option value="{{$shipping->address}}">{{$shipping->address}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="mb-3">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" required>
                  </div>

                  <h4 class="mb-3 ml-0 pl-0">PAYMENT</h4>

                  <div class="d-block my-3">
                    <div class="custom-control custom-radio" id="csdOption">
                      <input id="cash-delivery" name="payment_method" type="radio" class="custom-control-input" value="cashondelivery" checked required>
                      <label class="custom-control-label" for="cash-delivery">Cash on Delivery</label>
                    </div>
                    <!-- <div class="custom-control custom-radio">
                      <input id="esewa" name="payment_method" type="radio" class="custom-control-input" value="esewa" required>
                      <label class="custom-control-label" for="esewa">Esewa</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input id="fonepay" name="payment_method" type="radio" class="custom-control-input" value="fonepay" required>
                      <label class="custom-control-label" for="fonepay">Fonepay</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input id="Visa" name="payment_method" type="radio" class="custom-control-input" value="visa" required>
                      <label class="custom-control-label" for="Visa">Visa</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input id="paypal" name="payment_method" type="radio" class="custom-control-input" value="paypal" required>
                      <label class="custom-control-label" for="paypal">PayPal</label>
                    </div> -->
                  </div>

                  <hr class="mt-5">
                  <div class="pt-0 pb-2">*Shipping Charges will be applied</div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 order-summary-col" data-aos="fade-left" data-aos-delay="400" data-aos-once="true">
            <div class="container-fluid order-summary-col-inner">
              <h4>ORDER SUMMARY</h4>
              <div class="summary-contianer">
                <div class="row">
                  <div class="col-8">Subtotal </div>
                  <div class="col-4">Rs.{{ $cart_subtotal }}</div>
                </div>
                <div class="row">
                  <div class="col-8">Discount</div>
                  <div class="col-4">Rs.{{ $cart_discount }}</div>
                </div>
                <div id="shippingrate">

                </div>

                {{-- <div class="row">
                  <div class="col-8">Taxes</div>
                  <div class="col-4">Rs.0</div>
                </div>
                <div class="row">
                  <div class="col-8">Shipping</div>
                  <div class="col-4">Rs.0</div>
                </div> --}}



                <div class="row">
                  <div class="col-12 text-right mt-5 pt-5">
                    @if(count($cart_products))
                    <button class="btn pl-4 pr-4 btn-sm btn-primary btn-outline-secondary checkout-btn" type="submit" id="button-addon2">CHECKOUT</button>
                    {{-- <!-- <a href="{{ route('make.payment') }}" class="btn pl-4 pr-4 btn-sm btn-primary btn-outline-secondary checkout-btn">Pay $224 via Paypal</a>
                    <form action="https://uat.esewa.com.np/epay/main" method="POST">
                      <input value="100" name="tAmt" type="hidden">
                      <input value="90" name="amt" type="hidden">
                      <input value="5" name="txAmt" type="hidden">
                      <input value="2" name="psc" type="hidden">
                      <input value="3" name="pdc" type="hidden">
                      <input value="EPAYTEST" name="scd" type="hidden">
                      <input value="ee2c3ca1-696b-4cc5-a6be-2c40d929d453" name="pid" type="hidden">
                      <input value="http://merchant.com.np/page/esewa_payment_success?q=su" type="hidden" name="su">
                      <input value="http://merchant.com.np/page/esewa_payment_failed?q=fu" type="hidden" name="fu">
                      <input value="Submit" type="submit">
                    </form> --> --}}
                    @else
                    <p>No products in cart to checkout</p>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>

@endsection

@section('after-scripts')
  <script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
  <script src="{{url('frontend/js/jquery-parallax.js/parallax.min.js')}}"></script>
  <script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
  <script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
  <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
  <script src="{{url('frontend/dist/js/all.js')}}"></script>
  <!-- <script type="text/javascript" src="./js/instafeed.min.js"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js" type="text/javascript"></script>
  <script>
    AOS.init({duration: 800});
    $("#checkoutform").validate();
  </script>
  <script>
     $(document).ready(function () {
        const selValue=$("#state").find(':selected').val();
        $.ajax({
          type:"GET",
          url:`/shippingrate/${selValue}`,
          success:function(data){
            $('#shippingrate').html(data);
          }
        })

      $(function(){
        $("#state").change(function(){
          const selValue=$(this).find(':selected').val();
          $.ajax({
          type:"GET",
          url:`/shippingrate/${selValue}`,
          success:function(data){
            $('#shippingrate').html(data);
          }
        })
        if($(this).val().split('.')[1] !="3")
        {
          $('#csdOption').addClass("d-none");
        }
        else
        {
          $('#csdOption').removeClass("d-none");
        }
        });
        });
    });
  </script>
@endsection
