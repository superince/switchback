@extends('frontend.layouts.app')
@section('title', 'Blog -')
@section('content')

<style>
iframe{
  width:100%;
  height:450px;
}
</style>

@if(count($blogdata->photos)>0)
<div class="banner-inner-page" style="background-image:url({{url('/img/blog/'.$blogdata->photos[0]->photo)}}); background-position: 123px;">
  <img src="{{url('/img/blog/'.$blogdata->photos[0]->photo)}}" alt="">
</div>

<!-- <div class="banner-inner-page" style="background-image:url({{url('img/blog',$blogdata->photos[0]->photo)}});">
  <img src="{{url('frontend/images/strory-img.jpg')}}" alt="">
</div> -->
@else
<div class="banner-inner-page" style="background-image:url({{url('frontend/images/strory-img.jpg')}});">
  <img src="{{url('frontend/images/strory-img.jpg')}}" alt="">
</div>
@endif
<div class="container-sm blog-single-contianer">
  <h3 class="large">{{$blogdata->title}}</h3>
  <div class="date">{{$blogdata->created_at->diffForHumans()}}</div>
  @if($blogdata->registration===1)
  <div class="d-inline-block mt-3">
    <a href="#blogRegister" target="_blank" class="d-inline-block w-auto border-0 btn btn-primary btn-md pl-4 pr-4 pt-2 pb-2 hover-animate open-registerride" data-toggle="modal" data-id="{{$blogdata->id}}">REGISTER FOR RIDE</a>
  </div>
  @endif
  <div class="share-btn-wrap d-inline-block">
    <div class="sharebtn d-inline-block">
      <a href="#" class="btn btn-md btn-outline-dark" style="padding: 7px 15px;"><span class="icon-share mr-2"></span> SHARE</a>
      <!-- <a href="#" class="btn btn-outline-dark d-inline-block">SHARE</a> -->
      <div class="share">
        <a href="https://www.facebook.com/sharer/sharer.php?u=http://switchbacknepal.cf/" style="font-size: 1.1em" target="_blank"><span class="icon-facebook1"></span></a>
        <a href="https://twitter.com/intent/tweet?url=http://switchbacknepal.cf/&text=" style="font-size: 1.1em" target="_blank"><span class="icon-twitter"></span></a>
        <a href="https://pinterest.com/pin/create/button/?url=http://switchbacknepal.cf/&media=&description=" style="font-size: 1.1em" target="_blank"><span class="icon-instagram2"></span></a>
        <a href="mailto:info@example.com?&subject=&body=http://switchbacknepal.cf/ " style="font-size: 1.1em" target="_blank"><span class="icon-mail2"></span></a>
      </div>
    </div>

  </div>

  <!-- <div class="img-caption">{!!substr($blogdata->title,0,40) !!}</div> -->
  <div class="blog-detail">{!!$blogdata->description!!}</div>
</div> <!-- /.blog-single-contianer -->


<div class="container-fluid blog-container">
  <h3>YOU MAY ALSO LIKE</h3>
  <div class="row row-cols-1 row-cols-lg-3">
    @foreach ($relatedblogdata as $related)
    <div class="col card-col">
      <div class="card img-in-side h-100">
        <div class="row no-gutters">
          @if(count($related->photos))
            <div class="col-sm-6 col-md-6 col-lg-12 card-img-as-bg" style="background-image:url({{url('img/blog',$related->photos[0]->photo)}}); ">
              <img src="{{url('frontend/images/blog-card-img-sizel.png')}}" class="card-img" alt="...">
            </div>
          @else
            <div class="col-sm-6 col-md-6 col-lg-12 card-img-as-bg" style="background-image:url({{url('frontend/images/blog-card-img3.jpg')}}); ">
              <img src="{{url('frontend/images/blog-card-img-sizel.png')}}" class="card-img" alt="...">
            </div>
          @endif
          <div class="col-sm-6 col-md-6 col-lg-12 pb-5">
            <div class="card-body">
              <h3 class="card-title">{{$related->title}}</h3>
              <p class="date">{{$related->created_at->diffForHumans()}}</p>
              <p class="card-text">{!!substr($related->short_description,0,100) !!} </p>
              <div class="pb-3">
                <a href="{{url('blog',$related->slug)}}" class="btn btn-outline-dark d-inline-block mr-2">READ MORE</a>
                <div class="sharebtn d-inline-block">
                  <a href="#" class="btn btn-outline-dark d-inline-block">SHARE</a>
                  <div class="share">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://switchbacknepal.cf/"><span class="icon-facebook1"></span></a>
                    <a href="https://twitter.com/intent/tweet?url=http://switchbacknepal.cf/&text="><span class="icon-twitter"></span></a>
                    <a href="https://pinterest.com/pin/create/button/?url=http://switchbacknepal.cf/&media=&description="><span class="icon-instagram2"></span></a>
                    <a href="mailto:info@example.com?&subject=&body=http://switchbacknepal.cf/ "><span class="icon-envelope"></span></a>
                  </div>
                </div>
              </div>
              @if($related->registration===1)
              <div>
                <a href="#" target="_blank" class="d-inline-block w-auto border-dark btn btn-primary btn-md pl-4 pr-4 hover-animate">REGISTER FOR RIDE</a>
              </div>
              @endif
            </div> <!-- /.card-body -->
          </div> <!-- /.col-md-6 -->
        </div>
      </div> <!-- /.card .img-in-side  -->  
    </div> <!-- /.col-md-6 -->
    @endforeach
  </div> <!-- /.row -->
  <div class="text-center">
    <a href="/about/#blogs" target="_blank" class="border-dark btn btn-primary btn-md pl-5 pr-5 hover-animate">EXPLORE</a>
  </div>
</div> <!-- /.blog-container -->
@endsection

@section('after-scripts')                    
<script src="{{url('frontend/js/jquery/dist/jquery.slim.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
<script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
<script src="{{url('frontend/dist/js/all.js')}}"></script>

@endsection

