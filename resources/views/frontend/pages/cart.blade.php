@extends('frontend.layouts.app')
@section('title', 'Cart -')
@section('content')
    <div class="cart-page-wrapper">
      <div class="container-fluid" style="min-height: 700px;">
        <div class="row">
          <div class="col-xl-8 cart-col mb-5" data-aos="fade-right" data-aos-delay="100" data-aos-once="true">
            <div class="cart-col-inner">
              <h4>MY CART</h4>
              <div class="column-labels clearfix d-none d-md-block">
                <label class="product-image">Product</label>
                <label class="product-name">Name</label>
                <label class="product-size">Size</label>
                <label class="product-color">Color</label>
                <label class="product-quantity">Quantity</label>
                <label class="product-price">Price</label>
                <label class="product-action">Action</label>
              </div>
              @foreach($cart_products as $key => $cart_product)
           
              <div class="product-row clearfix">
                <div class="p-col product-image">
                  <img src="{{ asset('img/products/'.$cart_product->photos[0]->photo)}}" class="img-fluid" width="100">
                </div>
                <div class="p-col product-name">
                  <div class="p-col product-cat">{{ $cart_product->brand->name }}</div> 
                  <div class="p-col product-name-text">{{ $cart_product->name }}</div>
                </div>
                <div class="p-col product-size"><span class="d-inline d-md-none">Size: </span>{{ $cart_product->cart_size }}</div>
                <div class="p-col product-color"><span class="d-inline d-md-none">Color: </span> <span class="p-color red d-inline-block" style="background: {{ $cart_product->cart_color->hex }}"></span></div>
                <div class="clearfix d-block d-md-none"></div>
                <div class="p-col product-quantity">
                  <div class="quantity-input-wrap">
                    {{ Form::open(['route' => ['cart.update-quantity', $key]]) }}
                   
                    <div class="quantity-input input-group inline-group">
                      <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary btn-minus" type="button">
                          <span class="icon-minus"></span>
                        </button>
                      </div>
                    
                      <input class="form-control quantity" min="0" max="{{$cart_product->cart_stock}}" name="quantity" value="{{ $cart_product->cart_quantity }}" type="number" data-cart-key="{{ $key }}">
                      <div class="input-group-append">
                        <button class="btn btn-outline-secondary btn-plus" type="button">
                          <span class="icon-plus"></span>
                        </button>
                      </div>
                      <button type="submit" class="btn btn-sm border-left-0 border border-secondary btn-primary">Update</button>
                    </div>
                    {{ Form::close() }}
                  </div>
               
                </div>
                @if($cart_product->sale==1)
                  <div class="p-col product-price">Rs. {{ ($cart_product->price - $cart_product->sale_price/100*$cart_product->price) * $cart_product->cart_quantity }}</div>
                @else
                  <div class="p-col product-price">Rs. {{ $cart_product->price * $cart_product->cart_quantity }}</div>
                @endif
                
                <div class="p-col product-action">
                  <a class="btn btn-sm btn-outline-secondary" href="{{ route('cart.remove-from-cart', $key) }}"><span class="icon-x"></span></a>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          <div class="col-xl-4 order-summary-col" data-aos="fade-left" data-aos-delay="400" data-aos-once="true">
            <div class="container-fluid order-summary-col-inner">              
              <h4>ORDER SUMMARY</h4>
              <div class="summary-contianer">
                <div class="row">
                  <div class="col-8">Subtotal </div> 
                  <div class="col-4">Rs.{{ $cart_subtotal }}</div>
                </div>
                <div class="row">
                  <div class="col-8">Discount</div> 
                  <div class="col-4">Rs.{{ $cart_discount }}</div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="promo-code">
                      {{ Form::open(['route' => 'cart.promocode']) }}
                      Enter promo Codes for discount
                      <div class="input-group mb-3 promo-code-form">
                        <input type="text" name="code" class="form-control" placeholder="Promo Code" aria-label="email" aria-describedby="button-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary gray-bordered" type="submit" id="button-addon2">APPLY</button>
                        </div>
                      </div>
                      {{ Form::close() }}
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row total-row">
                  <div class="col-8">TOTAL</div> 
                  <div class="col-4">Rs.{{ $cart_subtotal - $cart_discount }}</div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-12 text-center"><a href="/checkout" class="btn pl-4 pr-4 btn-sm btn-primary btn-outline-secondary checkout-btn" type="button" id="button-addon2">CHECKOUT</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection

  @section('after-scripts')
      <script src="{{url('frontend/js/jquery/dist/jquery.min.js')}}"></script>
      <script src="{{url('frontend/js/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{url('frontend/js/aos/dist/aos.js')}}"></script>
      <script src="{{url('frontend/js/gsap/dist/gsap.min.js')}}"></script>
      <script src="{{url('frontend/js/gsap/dist/CSSRulePlugin.min.js')}}"></script>
      <script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
      <script src="{{url('frontend/js/rellax/rellax.min.js')}}"></script>
      <script src="{{url('frontend/dist/js/all.js')}}"></script>
      <script>
        AOS.init({duration: 800});
      </script>
    
      <script type="text/javascript">
        $('.btn-plus, .btn-minus').on('click', function(e) {
            const isNegative = $(e.target).closest('.btn-minus').is('.btn-minus');
            const input = $(e.target).closest('.input-group').find('input');
            
            if (input.is('input')) {
              input[0][isNegative ? 'stepDown' : 'stepUp']()
            }
        });
      </script>

  @endsection
